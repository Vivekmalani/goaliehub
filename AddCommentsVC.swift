//
//  AddCommentsVC.swift
//  GoalieHub
//
//  Created by Vivek on 15/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit

class AddCommentsVC: UIViewController {
    @IBOutlet var BarBTN: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func viewWillAppear(animated: Bool) {
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }

}
