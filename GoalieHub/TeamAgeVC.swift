//
//  TeamAgeVC.swift
//  GoalieHub
//
//  Created by Vivek on 09/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit

class TeamAgeVC: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {
    @IBOutlet var BarBTN: UIBarButtonItem!
    
    @IBOutlet var ToBTN: UIButton!
    @IBOutlet var FromBTN: UIButton!
    var PickerValue=NSMutableArray()
    var pickerView:UIPickerView!
    var pickerToolbar:UIToolbar!
    var detectPicker:Int=0
    var MinValue:Int=18,MaXValue:Int=90
    var MinValueStr:Int!,MaxValueStr:Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        pickerView=UIPickerView(frame: CGRectMake(0, self.view.frame.size.height-265, self.view.frame.size.width, 150))
        pickerView.dataSource=self
        pickerView.delegate=self
        pickerView.backgroundColor=UIColor.whiteColor()
        self.view.addSubview(pickerView)
        pickerToolbar=UIToolbar(frame: CGRectMake(0, pickerView.frame.origin.y-35, self.view.frame.size.width, 35))
        pickerToolbar.barStyle = UIBarStyle.Default
        pickerToolbar.translucent = true
        pickerToolbar.tintColor = UIColor.blackColor()
        pickerToolbar.barTintColor=UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
        //pickerToolbar.sizeToFit()
        
        var doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: "donePicker")
        var CButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        pickerToolbar.setItems([CButton,doneButton], animated: false)
        pickerToolbar.userInteractionEnabled = true
        self.view.addSubview(pickerToolbar)
        
        pickerToolbar.hidden=true
        pickerView.hidden=true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func toBTNClick(sender: UIButton) {
        detectPicker=2
        PickerValue.removeAllObjects()
        PickerValue=NSMutableArray(objects: "18","20","25","30","35","40","45","50","60","70","90")
        OpenPicker()
    }
    @IBAction func FromBTNClick(sender: UIButton) {
        detectPicker=1
        PickerValue.removeAllObjects()
        PickerValue=NSMutableArray(objects: "18","20","25","30","35","40","45","50","60","70","90")
        OpenPicker()
    }
    
    @IBAction func BTN1Click(sender: UIButton) {
        FromBTN.setTitle("18", forState: UIControlState.Normal)
        ToBTN.setTitle("24", forState: UIControlState.Normal)
        MinValue=18
        MaXValue=24
        MinValueStr=18
        MaxValueStr=24
    }
    @IBAction func BTN2Click(sender: UIButton) {
        FromBTN.setTitle("25", forState: UIControlState.Normal)
        ToBTN.setTitle("34", forState: UIControlState.Normal)
        MinValue=25
        MaXValue=34
        MinValueStr=25
        MaxValueStr=34
    }

    @IBAction func BTN3Click(sender: UIButton) {
        FromBTN.setTitle("35", forState: UIControlState.Normal)
        ToBTN.setTitle("59", forState: UIControlState.Normal)
        MinValue=35
        MaXValue=59
        MinValueStr=35
        MaxValueStr=59
    }
    @IBAction func BTN4Click(sender: UIButton) {
        FromBTN.setTitle("60", forState: UIControlState.Normal)
        ToBTN.setTitle("90", forState: UIControlState.Normal)
        MinValue=60
        MaXValue=90
        MinValueStr=60
        MaxValueStr=90
    }
    
    @IBAction func SetAgeBTNClick(sender: UIButton) {
        if MinValueStr != nil && MaxValueStr != nil
        {
            NSUserDefaults.standardUserDefaults().setInteger(MinValueStr, forKey: "MinAge")
            NSUserDefaults.standardUserDefaults().setInteger(MaxValueStr, forKey: "MaxAge")
        }
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    func OpenPicker()
    {
        if detectPicker==1
        {
            FromBTN.setTitle(PickerValue.objectAtIndex(0) as? String, forState: UIControlState.Normal)
            MinValue=PickerValue.objectAtIndex(0).integerValue
            MinValueStr=PickerValue.objectAtIndex(0).integerValue
        }
        else if detectPicker==2
        {
            ToBTN.setTitle(PickerValue.objectAtIndex(0) as? String, forState: UIControlState.Normal)
            MaXValue=PickerValue.objectAtIndex(0).integerValue
            MaxValueStr=PickerValue.objectAtIndex(0).integerValue
        }
        pickerView.hidden=false
        pickerToolbar.hidden=false
        
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: false)
    }
    
    func donePicker()
    {
        pickerView.hidden=true
        pickerToolbar.hidden=true
    }
    
    /******************************************************
    MARK:PickerView Delegate Method
    ******************************************************/
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return PickerValue.count
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if detectPicker==1
        {
            if PickerValue.objectAtIndex(row).integerValue <= MaXValue
            {
                FromBTN.setTitle(PickerValue.objectAtIndex(row) as? String, forState: UIControlState.Normal)
                MinValue=PickerValue.objectAtIndex(row).integerValue
                MinValueStr=PickerValue.objectAtIndex(row).integerValue
            }
            //MinValue=PickerValue.objectAtIndex(row).integerValue
        }
        else if detectPicker==2
        {
            if PickerValue.objectAtIndex(row).integerValue >= MinValue
            {
                ToBTN.setTitle(PickerValue.objectAtIndex(row) as? String, forState: UIControlState.Normal)
                MaXValue=PickerValue.objectAtIndex(row).integerValue
                MaxValueStr=PickerValue.objectAtIndex(row).integerValue
            }
            //MaXValue=PickerValue.objectAtIndex(row).integerValue
        }
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return PickerValue.objectAtIndex(row) as! String
    }
    
    override func viewWillAppear(animated: Bool) {
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }
    
}
