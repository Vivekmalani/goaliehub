//
//  EventDetailsVC.swift
//  GoalieHub
//
//  Created by Vivek on 13/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire
class EventDetailsVC: UIViewController,MBProgressHUDDelegate {
    @IBOutlet var BarBTN: UIBarButtonItem!
    @IBOutlet var CarbonView: UIView!
    @IBOutlet var IDLBL: UILabel!
    
    @IBOutlet var CancelBTN: UIButton!
    @IBOutlet var PopupView: UIView!
    @IBOutlet var BackView: UIView!
    @IBOutlet var DetailView: UIView!
    @IBOutlet var CommentsLBL: UILabel!
    @IBOutlet var UserName: UILabel!
    @IBOutlet var UserIMG: UIImageView!
    @IBOutlet var DateLBL: UILabel!
    @IBOutlet var CreateDateLBL: UILabel!
    @IBOutlet var EventNameLBL: UILabel!
    var carbonTabSwipeNavigation:CarbonTabSwipeNavigation!
    var EventID:Int!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!
    var itemAry=NSArray()
    var sendDict=NSMutableDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        CancelBTN.clipsToBounds=true
        CancelBTN.layer.cornerRadius=5.0
        CancelBTN.layer.borderWidth=1.0
        CancelBTN.layer.borderColor=UIColor.whiteColor().CGColor
        
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        itemAry=NSArray(array: ["EVENT DETAILS","ATTENDANCE","COMMENTS","HISTORY"])
        carbonTabSwipeNavigation=CarbonTabSwipeNavigation(items: itemAry as [AnyObject], delegate: self)
        carbonTabSwipeNavigation.insertIntoRootViewController(self, andTargetView: CarbonView)
        Style()
        EventID = NSUserDefaults.standardUserDefaults().valueForKey("EventID") as! Int
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        let IdentiDict=NSDictionary(objectsAndKeys: EventID,"eventId")
        let ParamsDict=NSDictionary(objectsAndKeys:IdentiDict,"Identifier")
        let RequestDict=NSDictionary(objectsAndKeys: "getEventDetails","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Event",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
        }
        
        let gesture = UITapGestureRecognizer(target: self, action: "PopupAction:")
        BackView.addGestureRecognizer(gesture)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func PopupAction(sender:UITapGestureRecognizer){
        BackView.hidden=true
        PopupView.hidden=true
    }
    
    @IBAction func EventBTNClick(sender: UIButton) {
        BackView.hidden=false
        PopupView.hidden=false
    }
    @IBAction func CancelBTNClick(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("CancelEventVC") as! CancelEventVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
        initialViewController.CancelDict=sendDict
    }
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
            HUD.hide(true)
        }
        else
        {
            DetailView.hidden=false
            sendDict=Dict as! NSMutableDictionary
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            EventNameLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("eventName") as? String
            IDLBL.text=NSString(format: "Event : %@", Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("eventId") as! NSNumber) as String
            
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("eventTs") as! String)
            DateFormatter.dateFormat = "EE,MMM dd,yyyy"
            DateLBL.text = DateFormatter.stringFromDate(DateObj!)
            
            let CreateDate=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("eventCreateTs") as! String
            let CreateDateAry=CreateDate.componentsSeparatedByString(" ")
            CreateDateLBL.text=CreateDateAry[0]
            
            let ImageData = NSData(base64EncodedString: Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("vUserSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            UserIMG.image=UIImage(data: ImageData!)
            UserIMG.clipsToBounds=true
            UserIMG.layer.cornerRadius=UserIMG.frame.size.width/2
            
            UserName.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
            let Width=UserName.intrinsicContentSize().width
            UserName.frame=CGRectMake(self.view.frame.size.width-Width-5, UserName.frame.origin.y, Width, UserName.frame.size.height)
            UserIMG.frame=CGRectMake(UserName.frame.origin.x-UserIMG.frame.size.width-5, UserIMG.frame.origin.y, UserIMG.frame.size.width, UserIMG.frame.size.height)
            CommentsLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("eventLocationnote") as? String
        }
        
    }

    
    func Style()
    {
        let color=UIColor.whiteColor()
        self.navigationController?.navigationBar.translucent=false
        carbonTabSwipeNavigation.setTabBarHeight(40)
        carbonTabSwipeNavigation.toolbar.translucent=false
        carbonTabSwipeNavigation.setIndicatorColor(color)
        carbonTabSwipeNavigation.setSelectedColor(UIColor.whiteColor())
        carbonTabSwipeNavigation.setNormalColor(UIColor.whiteColor())
        carbonTabSwipeNavigation.toolbar.barTintColor=UIColor.blackColor()
        //carbonTabSwipeNavigation.view.backgroundColor
        carbonTabSwipeNavigation.setTabExtraWidth(30)
        
        //        carbonTabSwipeNavigation.toolbar.layer.borderWidth=1.0
        //        carbonTabSwipeNavigation.toolbar.layer.borderColor=UIColor.whiteColor().CGColor
        //        carbonTabSwipeNavigation.toolbar.clipsToBounds=true
        
        carbonTabSwipeNavigation.carbonSegmentedControl.setWidth(self.view.frame.size.width/2, forSegmentAtIndex: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl.setWidth(self.view.frame.size.width/2, forSegmentAtIndex: 1)
        carbonTabSwipeNavigation.carbonSegmentedControl.setWidth(self.view.frame.size.width/2, forSegmentAtIndex: 2)
        carbonTabSwipeNavigation.carbonSegmentedControl.setWidth(self.view.frame.size.width/2, forSegmentAtIndex: 3)
    }
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        switch index
        {
        case 0:
            return self.storyboard!.instantiateViewControllerWithIdentifier("DetailsVC") as! DetailsVC
        case 1:
            return self.storyboard!.instantiateViewControllerWithIdentifier("AttandanceVC") as! AttandanceVC
        case 2:
            return self.storyboard!.instantiateViewControllerWithIdentifier("CommentsVC") as! CommentsVC
        default:
            return self.storyboard!.instantiateViewControllerWithIdentifier("HistoryVC") as! HistoryVC
        }
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAtIndex index: UInt) {
        if index == 0
        {
            NSNotificationCenter.defaultCenter().postNotificationName("EventDetails", object: nil)
        }
        else if index==1
        {
            NSNotificationCenter.defaultCenter().postNotificationName("Attandance", object: nil)
        }
        else if index==2
        {
            NSNotificationCenter.defaultCenter().postNotificationName("Comments", object: nil)
        }
        else if index==3
        {
            NSNotificationCenter.defaultCenter().postNotificationName("History", object: nil)
        }
        
    }
    
    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func viewWillAppear(animated: Bool) {
        
       
        if carbonTabSwipeNavigation.carbonSegmentedControl.selectedSegmentIndex == 2
        {
            NSNotificationCenter.defaultCenter().postNotificationName("Comments", object: nil)
        }
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }

}
