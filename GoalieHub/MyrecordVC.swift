//
//  MyrecordVC.swift
//  GoalieHub
//
//  Created by Vivek on 11/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class MyrecordVC: UIViewController,MBProgressHUDDelegate {
    @IBOutlet var FirstView: UIView!
    @IBOutlet var ThirdView: UIView!
    @IBOutlet var SecondView: UIView!
    @IBOutlet var BackScroll: UIScrollView!
    
    @IBOutlet var RatingLBL: UILabel!
    @IBOutlet var RatingPerLBL: UILabel!
    @IBOutlet var EventLBL: UILabel!
    @IBOutlet var PhoneLBL: UILabel!
    @IBOutlet var EmailLBL: UILabel!
    @IBOutlet var PostalLBL: UILabel!
    @IBOutlet var DOBLBL: UILabel!
    @IBOutlet var NameLBL: UILabel!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!
    override func viewDidLoad() {
        super.viewDidLoad()

        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        
        FirstView.clipsToBounds=true
        FirstView.layer.cornerRadius=5
        FirstView.layer.borderWidth=1.0
        FirstView.layer.borderColor=UIColor.whiteColor().CGColor
        
        SecondView.clipsToBounds=true
        SecondView.layer.cornerRadius=5
        SecondView.layer.borderWidth=1.0
        SecondView.layer.borderColor=UIColor.whiteColor().CGColor
        
        ThirdView.clipsToBounds=true
        ThirdView.layer.cornerRadius=5
        ThirdView.layer.borderWidth=1.0
        ThirdView.layer.borderColor=UIColor.whiteColor().CGColor
        BackScroll.contentSize=CGSizeMake(0, ThirdView.frame.size.height+ThirdView.frame.origin.y+10)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "MyRecordsLoad:", name:"MyRecords", object: nil)
        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func MyRecordsLoad(notification: NSNotification){
        HUD.show(true)
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        
        let ParamsDict=NSDictionary()
        let RequestDict=NSDictionary(objectsAndKeys: "getMyUserDetails","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "User",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
        }
    }
    
    @IBAction func ChangeEmailBTNClick(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("ChangeEmailVC") as! ChangeEmailVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    
    @IBAction func ChangePhoneBTNClick(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
        NameLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
            DOBLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("userdetDobDate") as? String
            PostalLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("userdetPostal") as? String
            EmailLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("userdetEmail") as? String
            PhoneLBL.text=NSString(format: "%@ %@",Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("Phone")!.valueForKey("phoneAreacode") as! String,Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("Phone")!.valueForKey("phoneNumber") as! String ) as String
            
            ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
            KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
            let IdentiDict=NSDictionary(objectsAndKeys: 102,"userId")
            let ParamsDict=NSDictionary(objectsAndKeys: IdentiDict,"Identifier")
            let RequestDict=NSDictionary(objectsAndKeys: "getUserRating","method","1","id",ParamsDict,"params")
            print(RequestDict)
            let param = [
                "id"    : ID,
                "key"    : KEY,
                "callBack"    : "myCallBackMethod",
                "serviceName"    : "User",
                "request" : RequestDict
            ]
            println(param)
            Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
                .responseJSON {response in
                    self.HandleAPIResponse2(response.3, Dict: response.2 as? NSDictionary)
            }
        }
    }
    func HandleAPIResponse2(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            //print(Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("RatingSummary")!.valueForKey("ratsumPositivePercent"))
            EventLBL.text=NSString(format: "%i", Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("RatingSummary")!.valueForKey("ratsumNumEvents") as! Int) as String
            RatingPerLBL.text=NSString(format: "%.2f", Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("RatingSummary")!.valueForKey("ratsumPositivePercent") as! Float) as String
            
            //print(Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("RatingSummary")!.valueForKey("ratsumNumRatings"))
            
            RatingLBL.text=NSString(format: "%i", Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("RatingSummary")!.valueForKey("ratsumNumRatings") as! Int) as String
            
            HUD.hide(true)
        }
    }
}
