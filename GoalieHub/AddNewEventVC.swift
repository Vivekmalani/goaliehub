//
//  AddNewEventVC.swift
//  GoalieHub
//
//  Created by Vivek on 16/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class AddNewEventVC: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate,MBProgressHUDDelegate {
    @IBOutlet var BarBTN: UIBarButtonItem!
    @IBOutlet var BackScroll: UIScrollView!
    @IBOutlet var CreateBTN: UIButton!
    @IBOutlet var EventTypeBTN: UIButton!
    @IBOutlet var TimeBTN: UIButton!
    @IBOutlet var DateBTN: UIButton!
    @IBOutlet var LocationBTN: UIButton!
    @IBOutlet var TeamBTN: UIButton!
    @IBOutlet var TeamIMG: UIImageView!
    @IBOutlet var TXTName: UITextField!
    @IBOutlet var TXTNote: UITextField!
    var animateDistance: CGFloat = 0.0
    var pickerView:UIPickerView!
    var DatePicker:UIDatePicker!
    var pickerToolbar:UIToolbar!
    var PickerValue=NSMutableArray()
    var ID:NSNumber!,KEY:NSNumber!,detectPicker:Int!,TeamID:Int!,LocID:Int!,EventType:Int!
    var DateStr:String!,TimeStr:String!
    var HUD:MBProgressHUD!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        BackScroll.contentSize=CGSizeMake(0, CreateBTN.frame.size.height+CreateBTN.frame.origin.y+60)
        
        TXTName.attributedPlaceholder = NSAttributedString(string:"Opponent Name",attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        TXTNote.attributedPlaceholder = NSAttributedString(string:"Event Notes",attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        pickerView=UIPickerView(frame: CGRectMake(0, self.view.frame.size.height-265, self.view.frame.size.width, 200))
        pickerView.dataSource=self
        pickerView.delegate=self
        pickerView.backgroundColor=UIColor.whiteColor()
        self.view.addSubview(pickerView)
        
        DatePicker=UIDatePicker(frame: CGRectMake(0, self.view.frame.size.height-265, self.view.frame.size.width, 200))
        DatePicker.datePickerMode=UIDatePickerMode.Date
        DatePicker.backgroundColor=UIColor.whiteColor()
        DatePicker.addTarget(self, action: "ChangeDate:", forControlEvents: UIControlEvents.ValueChanged)
        self.view.addSubview(DatePicker)
        
        pickerToolbar=UIToolbar(frame: CGRectMake(0, pickerView.frame.origin.y-35, self.view.frame.size.width, 35))
        pickerToolbar.barStyle = UIBarStyle.Default
        pickerToolbar.translucent = true
        pickerToolbar.tintColor = UIColor.blackColor()
        pickerToolbar.barTintColor=UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
        //pickerToolbar.sizeToFit()
        
        var doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: "donePicker")
        var CButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        pickerToolbar.setItems([CButton,doneButton], animated: false)
        pickerToolbar.userInteractionEnabled = true
        self.view.addSubview(pickerToolbar)
        pickerView.hidden=true
        pickerToolbar.hidden=true
        DatePicker.hidden=true
        
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID") as! NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY") as! NSNumber
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func ChangeDate(sender:UIDatePicker!)
    {
        if detectPicker == 3
        {
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "EE,MMM dd,yyyy"
            DateBTN.setTitle(DateFormatter.stringFromDate(sender.date), forState: UIControlState.Normal)
            
            let APIDateFormatter = NSDateFormatter()
            APIDateFormatter.dateFormat = "yyyy-MM-dd"
            DateStr=APIDateFormatter.stringFromDate(sender.date)
            
            //DateFormatter.stringFromDate(DateObj)
        }
        else
        {
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "hh:mm a"
            TimeBTN.setTitle(DateFormatter.stringFromDate(sender.date), forState: UIControlState.Normal)
            
            let APIDateFormatter = NSDateFormatter()
            APIDateFormatter.dateFormat = "HH:mm:ss"
            TimeStr=APIDateFormatter.stringFromDate(sender.date)
        }
    }
    
    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func donePicker()
    {
        pickerView.hidden=true
        pickerToolbar.hidden=true
        DatePicker.hidden=true
    }
    
    @IBAction func CreateEventBTNClick(sender: UIButton) {
       
        if TeamBTN.titleLabel!.text == "Select A Team"
        {
            UIAlertView(title: nil,message:"Please select team name.",delegate: self,cancelButtonTitle:"OK").show()
        }
        else if TXTName.text.isEmpty == true
        {
            UIAlertView(title: nil,message:"Please enter opponent name.",delegate: self,cancelButtonTitle:"OK").show()
        }
        else if LocationBTN.titleLabel!.text == "Select Location"
        {
            UIAlertView(title: nil,message:"Please select location.",delegate: self,cancelButtonTitle:"OK").show()
        }
        else if DateBTN.titleLabel!.text == "Event Date"
        {
            UIAlertView(title: nil,message:"Please select event date.",delegate: self,cancelButtonTitle:"OK").show()
        }
        else if TimeBTN.titleLabel!.text == "Event Time"
        {
            UIAlertView(title: nil,message:"Please select event time.",delegate: self,cancelButtonTitle:"OK").show()
        }
        else if EventTypeBTN.titleLabel!.text == "Event Type"
        {
            UIAlertView(title: nil,message:"Please select event type.",delegate: self,cancelButtonTitle:"OK").show()
        }
        else if TXTNote.text.isEmpty==true
        {
            UIAlertView(title: nil,message:"Please enter event notes.",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            HUD.show(true)
            ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
            KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
            let ValueDict=NSDictionary(objectsAndKeys: 1,"sportId",TeamID,"teamId",EventType,"eventType",LocID,"locId","","eventLocationnote",1,"eventDuration",DateStr+" "+TimeStr,"eventTs",NSTimeZone.localTimeZone().name,"timezone",TXTName.text,"eventName","Event Desc","eventDesc",TXTNote.text,"eventNotes")
            let ParamsDict=NSDictionary(objectsAndKeys:ValueDict,"Value")
            let RequestDict=NSDictionary(objectsAndKeys: "createEvent","method","1","id",ParamsDict,"params")
            print(RequestDict)
            let param = [
                "id"    : ID,
                "key"    : KEY,
                "callBack"    : "myCallBackMethod",
                "serviceName"    : "Event",
                "request" : RequestDict
            ]
            println(param)
            Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
                .responseJSON {response in
                    self.HandleAPIResponse3(response.3, Dict: response.2 as? NSDictionary)
            }
        }
        
    }
    @IBAction func EventTypeBTNClick(sender: UIButton) {
        detectPicker=5
        PickerValue=NSMutableArray()
        PickerValue=NSMutableArray(objects: "Shinny Unorganized","Shinny Organized","Pickup Hockey","League","Tournament")
        EventType=0
        EventTypeBTN.setTitle(PickerValue.objectAtIndex(0) as? String, forState: UIControlState.Normal)
        pickerView.hidden=false
        pickerToolbar.hidden=false
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: false)
    }
    @IBAction func TimeBTNClick(sender: UIButton) {
        
        let DateFormatter = NSDateFormatter()
        DateFormatter.dateFormat = "hh:mm a"
        TimeBTN.setTitle(DateFormatter.stringFromDate(NSDate()), forState: UIControlState.Normal)
        
        let APIDateFormatter = NSDateFormatter()
        APIDateFormatter.dateFormat = "HH:mm:ss"
        TimeStr=APIDateFormatter.stringFromDate(NSDate())
        
        detectPicker=4
        pickerView.hidden=true
        DatePicker.hidden=false
        pickerToolbar.hidden=false
        DatePicker.datePickerMode=UIDatePickerMode.Time
    }
    @IBAction func DateBTNClick(sender: UIButton) {
        
        let DateFormatter = NSDateFormatter()
        DateFormatter.dateFormat = "EE,MMM dd,yyyy"
        DateBTN.setTitle(DateFormatter.stringFromDate(NSDate()), forState: UIControlState.Normal)
        
        let APIDateFormatter = NSDateFormatter()
        APIDateFormatter.dateFormat = "yyyy-MM-dd"
        DateStr=APIDateFormatter.stringFromDate(NSDate())
        
        detectPicker=3
        pickerView.hidden=true
        DatePicker.hidden=false
        pickerToolbar.hidden=false
        DatePicker.datePickerMode=UIDatePickerMode.Date
    }
    @IBAction func LocationBTNClick(sender: UIButton) {
        if TeamID != nil
        {
            detectPicker=2
            PickerValue=NSMutableArray()
            HUD.show(true)
            ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
            KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
            let IdentiDict=NSDictionary(objectsAndKeys: TeamID,"teamId")
            let ParamsDict=NSDictionary(objectsAndKeys:IdentiDict,"Identifier")
            let RequestDict=NSDictionary(objectsAndKeys: "getTeamLocations","method","1","id",ParamsDict,"params")
            print(RequestDict)
            let param = [
                "id"    : ID,
                "key"    : KEY,
                "callBack"    : "myCallBackMethod",
                "serviceName"    : "Team",
                "request" : RequestDict
            ]
            println(param)
            Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
                .responseJSON {response in
                    self.HandleAPIResponse2(response.3, Dict: response.2 as? NSDictionary)
            }
        }
        else
        {
            UIAlertView(title: nil,message:"Please select team name first.",delegate: self,cancelButtonTitle:"OK").show()
        }
    }
    @IBAction func TeamBTNClick(sender: UIButton) {
        detectPicker=1
        PickerValue=NSMutableArray()
        HUD.show(true)
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID") as! NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY") as! NSNumber
        let ValueDict=NSDictionary(objectsAndKeys: "all","active")
        let ParamsDict=NSDictionary(objectsAndKeys:ValueDict,"Value")
        let RequestDict=NSDictionary(objectsAndKeys: "getMyTeams","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Team",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse1(response.3, Dict: response.2 as? NSDictionary)
        }
    }
    
    func HandleAPIResponse1(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            if let Check=Dict.valueForKey("response")!.valueForKey("result") as? NSDictionary
            {
                PickerValue.addObject(Dict.valueForKey("response")!.valueForKey("result")!)
            }
            else
            {
                PickerValue=Dict.valueForKey("response")!.valueForKey("result") as! NSMutableArray
            }
            
            let decodedData = NSData(base64EncodedString: PickerValue.objectAtIndex(0).valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TeamIMG.image=UIImage(data: decodedData!)
            TeamBTN.setTitle(PickerValue.objectAtIndex(0).valueForKey("vTeamSmall")!.valueForKey("teamName") as? String, forState: UIControlState.Normal)
            
            TeamID=PickerValue.objectAtIndex(0).valueForKey("vTeamSmall")!.valueForKey("teamId") as! Int
            DatePicker.hidden=true
            pickerView.hidden=false
            pickerToolbar.hidden=false
            pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            HUD.hide(true)
        }
    }
    
    func HandleAPIResponse2(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            PickerValue=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("TeamLocation") as! NSMutableArray
            LocID=PickerValue.objectAtIndex(0).valueForKey("vLocationSmall")!.valueForKey("locId") as! Int
            LocationBTN.setTitle(PickerValue.objectAtIndex(0).valueForKey("vLocationSmall")!.valueForKey("locName") as? String, forState: UIControlState.Normal)
            DatePicker.hidden=true
            pickerView.hidden=false
            pickerToolbar.hidden=false
            pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            HUD.hide(true)
        }
    }
    
    func HandleAPIResponse3(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
    }
    /******************************************************
    MARK:TextField Delegate Method
    ******************************************************/
    func textFieldDidBeginEditing(textField: UITextField) {
        let textFieldRect : CGRect = self.view.window!.convertRect(textField.bounds, fromView: textField)
        let viewRect : CGRect = self.view.window!.convertRect(self.view.bounds, fromView: self.view)
        let midline : CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
        let numerator : CGFloat = midline - viewRect.origin.y - MoveKeyboard.MINIMUM_SCROLL_FRACTION * viewRect.size.height
        let denominator : CGFloat = (MoveKeyboard.MAXIMUM_SCROLL_FRACTION - MoveKeyboard.MINIMUM_SCROLL_FRACTION) * viewRect.size.height
        var heightFraction : CGFloat = numerator / denominator
        if heightFraction < 0.0 {
            heightFraction = 0.0
        } else if heightFraction > 1.0 {
            heightFraction = 1.0
        }
        let orientation : UIInterfaceOrientation = UIApplication.sharedApplication().statusBarOrientation
        if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown) {
            animateDistance = floor(MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
        } else {
            animateDistance = floor(MoveKeyboard.LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
        }
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y -= animateDistance
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        self.view.frame = viewFrame
        UIView.commitAnimations()
    }
    func textFieldDidEndEditing(textField: UITextField) {
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y += animateDistance
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        self.view.frame = viewFrame
        UIView.commitAnimations()
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    /******************************************************
    MARK:PickerView Delegate Method
    ******************************************************/
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return PickerValue.count
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if detectPicker==1
        {
            let decodedData = NSData(base64EncodedString: PickerValue.objectAtIndex(row).valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TeamIMG.image=UIImage(data: decodedData!)
            TeamBTN.setTitle(PickerValue.objectAtIndex(row).valueForKey("vTeamSmall")!.valueForKey("teamName") as? String, forState: UIControlState.Normal)
            TeamID=PickerValue.objectAtIndex(row).valueForKey("vTeamSmall")!.valueForKey("teamId") as! Int
        }
        else if detectPicker==2
        {
            LocID=PickerValue.objectAtIndex(row).valueForKey("vLocationSmall")!.valueForKey("locId") as! Int
            LocationBTN.setTitle(PickerValue.objectAtIndex(row).valueForKey("vLocationSmall")!.valueForKey("locName") as? String, forState: UIControlState.Normal)
        }
        else
        {
            EventTypeBTN.setTitle(PickerValue.objectAtIndex(row) as? String, forState: UIControlState.Normal)
            EventType=row
            //return PickerValue.objectAtIndex(row) as? String
        }
    }
    
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        if detectPicker==1
        {
            return PickerValue.objectAtIndex(row).valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
        }
        else if detectPicker == 2
        {
            return PickerValue.objectAtIndex(row).valueForKey("vLocationSmall")!.valueForKey("locName") as? String
        }
        else
        {
            return PickerValue.objectAtIndex(row) as? String
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }

}
