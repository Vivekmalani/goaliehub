//
//  ChangePasswordVC.swift
//  GoalieHub
//
//  Created by Vivek on 16/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class ChangePasswordVC: UIViewController,MBProgressHUDDelegate {
    @IBOutlet var LogoIMG: UIImageView!
    @IBOutlet var BarBTN: UIBarButtonItem!
    @IBOutlet var LastSyncDateLBL: UILabel!
    @IBOutlet var WorkingLBL: UILabel!
    @IBOutlet var StatusLBL: UILabel!
    @IBOutlet var StatusIMG: UIImageView!
    @IBOutlet var DateLBL: UILabel!
    @IBOutlet var UserNameLBL: UILabel!
    @IBOutlet var UserIMG: UIImageView!
    @IBOutlet var StatusView: UIView!
    @IBOutlet var PhoneLBL: UILabel!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        HUD.show(true)
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        
        let ParamsDict=NSDictionary()
        let RequestDict=NSDictionary(objectsAndKeys: "getMyUserDetails","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "User",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            let decodedData = NSData(base64EncodedString: Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("vUserSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            UserIMG.image=UIImage(data: decodedData!)
            
            UserNameLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
            DateLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("vUserSmall")!.valueForKey("userName") as? String
            let LastSyncDateFormatter = NSDateFormatter()
            LastSyncDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let LastSyncDateObj = LastSyncDateFormatter.dateFromString(Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("vUserLastseen")!.valueForKey("lastseenTs") as! String)
            LastSyncDateFormatter.dateFormat = "dd/MM/yyyy HH:mm a"
            self.view.setHeader(NSString(format: "Last Sync:%@", LastSyncDateFormatter.stringFromDate(LastSyncDateObj!)) as String)
            PhoneLBL.text=NSString(format: "%@ %@",Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("Phone")!.valueForKey("phoneAreacode") as! String,Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("Phone")!.valueForKey("phoneNumber") as! String ) as String
            HUD.hide(true)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }

}
