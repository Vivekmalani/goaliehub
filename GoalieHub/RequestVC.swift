//
//  RequestVC.swift
//  GoalieHub
//
//  Created by Vivek on 14/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class RequestVC: UIViewController,MBProgressHUDDelegate {
    let cellIdentifier = "cellIdentifier"
    var HUD:MBProgressHUD!
    var ID:Int!,KEY:Int!,Index:Int!
    @IBOutlet var RequestTBL: UITableView!
    
    @IBOutlet var CancelBTN: UIButton!
    @IBOutlet var EventBTN: UIButton!
    @IBOutlet var PopupView: UIView!
    @IBOutlet var BackView: UIView!
    var RequestAry=NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        HUD.show(true)
        
        EventBTN.clipsToBounds=true
        EventBTN.layer.cornerRadius=5.0
        EventBTN.layer.borderWidth=1.0
        EventBTN.layer.borderColor=UIColor.whiteColor().CGColor
        
        CancelBTN.clipsToBounds=true
        CancelBTN.layer.cornerRadius=5.0
        CancelBTN.layer.borderWidth=1.0
        CancelBTN.layer.borderColor=UIColor.whiteColor().CGColor
        
        self.RequestTBL?.registerClass(UITableViewCell.self, forCellReuseIdentifier: self.cellIdentifier)
        RequestTBL.tableFooterView = UIView(frame: CGRectZero)
        RequestTBL.separatorStyle=UITableViewCellSeparatorStyle.None
        
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as Int
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as Int
        let ValueDict=NSDictionary(objectsAndKeys: 25,"limitResult",1,"pageNumber")
        let ParamsDict=NSDictionary(objectsAndKeys: ValueDict,"Value")
        let RequestDict=NSDictionary(objectsAndKeys: "getMyRequests","method","1","id",ParamsDict,"params")
        //print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "User",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
        }
        
        let gesture = UITapGestureRecognizer(target: self, action: "PopupAction:")
        BackView.addGestureRecognizer(gesture)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func PopupAction(sender:UITapGestureRecognizer){
        BackView.hidden=true
        PopupView.hidden=true
    }
    
    @IBAction func RequestBTNClick(sender: UIButton) {
    }
    @IBAction func EventBTNClick(sender: UIButton) {
    }
    /********************************************
    MARK:TableView Delegate
    ********************************************/
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RequestAry.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as! UITableViewCell
        cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: self.cellIdentifier)
        cell.frame=CGRectMake(0, 0, self.view.frame.size.width, 40)
        cell.backgroundColor=UIColor.blackColor()
        
        let BackView=UIView(frame: CGRectMake(10, 10, cell.frame.size.width-20, 90))
        
        let MessageIcon=UIImageView(frame: CGRectMake(15, 40, 20, 20))
        BackView.addSubview(MessageIcon)
        
        let PlayLBL=UILabel(frame: CGRectMake(MessageIcon.frame.origin.x+MessageIcon.frame.size.width+5, 5, 30,15))
        PlayLBL.text="PLAY"
        PlayLBL.font=UIFont.systemFontOfSize(10)
        PlayLBL.textColor=UIColor.greenColor()
        BackView.addSubview(PlayLBL)
        
        let RequestLBL=UILabel(frame: CGRectMake(PlayLBL.frame.origin.x+PlayLBL.frame.size.width+5, 5, 110,15))
        
        if RequestAry.objectAtIndex(indexPath.row).valueForKey("type")!.isEqualToString("sendPrireq")
        {
            RequestLBL.text="REQUEST SENT"
        }
        else
        {
            RequestLBL.text="REQUEST RECEIVED"
        }
        
        RequestLBL.font=UIFont.systemFontOfSize(10)
        RequestLBL.textColor=UIColor.whiteColor()
        BackView.addSubview(RequestLBL)
        
        let DateFormatter = NSDateFormatter()
        DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let DateObj = DateFormatter.dateFromString(RequestAry.objectAtIndex(indexPath.row).valueForKey("EventPosition")!.valueForKey("Event")!.valueForKey("eventTs") as! String)
        DateFormatter.dateFormat = "dd/MM hh:mm a"
        
        let DateLBL=UILabel(frame: CGRectMake(BackView.frame.size.width-110,5,100,15))
        DateLBL.textColor=UIColor.whiteColor()
        DateLBL.font=UIFont.systemFontOfSize(12)
        DateLBL.text=DateFormatter.stringFromDate(DateObj!)
        BackView.addSubview(DateLBL)
        
        let decodedData = NSData(base64EncodedString: RequestAry.objectAtIndex(indexPath.row).valueForKey("vUserSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
        let UserIMG=UIImageView(frame: CGRectMake(MessageIcon.frame.origin.x+MessageIcon.frame.size.width+5, PlayLBL.frame.origin.y+PlayLBL.frame.size.height+10, 20, 20))
        UserIMG.image=UIImage(data: decodedData!)
        UserIMG.clipsToBounds=true
        UserIMG.layer.cornerRadius=UserIMG.frame.size.width/2
        BackView.addSubview(UserIMG)
        
        let UserNameLBL=UILabel(frame: CGRectMake(UserIMG.frame.origin.x+UserIMG.frame.size.width+5, PlayLBL.frame.origin.y+PlayLBL.frame.size.height+10, 100, 20))
        UserNameLBL.textColor=UIColor.whiteColor()
        UserNameLBL.font=UIFont.systemFontOfSize(12)
        UserNameLBL.text=RequestAry.objectAtIndex(indexPath.row).valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
        BackView.addSubview(UserNameLBL)
        
        let LocationIMG=UIImageView(frame: CGRectMake(UserNameLBL.frame.origin.x+UserNameLBL.frame.size.width+5, PlayLBL.frame.origin.y+PlayLBL.frame.size.height+10, 13, 15))
        LocationIMG.image=UIImage(named: "ic_home_location_small.png")
        BackView.addSubview(LocationIMG)
        
        let LocationLBL=UILabel(frame: CGRectMake(LocationIMG.frame.origin.x+LocationIMG.frame.size.width+5, PlayLBL.frame.origin.y+PlayLBL.frame.size.height+10, BackView.frame.size.width-LocationIMG.frame.origin.x-20, 15))
        LocationLBL.textColor=UIColor.whiteColor()
        LocationLBL.font=UIFont.systemFontOfSize(12)
        LocationLBL.text=RequestAry.objectAtIndex(indexPath.row).valueForKey("EventPosition")!.valueForKey("Event")!.valueForKey("vLocationSmall")!.valueForKey("locName") as? String
        BackView.addSubview(LocationLBL)
        
        let TeamData = NSData(base64EncodedString: RequestAry.objectAtIndex(indexPath.row).valueForKey("EventPosition")!.valueForKey("Event")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
        let TeamIMG=UIImageView(frame: CGRectMake(MessageIcon.frame.origin.x+MessageIcon.frame.size.width+5, LocationLBL.frame.origin.y+LocationLBL.frame.size.height+10, 20, 20))
        TeamIMG.image=UIImage(data: TeamData!)
        TeamIMG.clipsToBounds=true
        TeamIMG.layer.cornerRadius=TeamIMG.frame.size.width/2
        BackView.addSubview(TeamIMG)
        
        let TeamNameLBL=UILabel(frame: CGRectMake(TeamIMG.frame.origin.x+TeamIMG.frame.size.width+5, LocationLBL.frame.origin.y+LocationLBL.frame.size.height+10, 90, 20))
        TeamNameLBL.textColor=UIColor.whiteColor()
        TeamNameLBL.font=UIFont.systemFontOfSize(12)
        TeamNameLBL.text=RequestAry.objectAtIndex(indexPath.row).valueForKey("EventPosition")!.valueForKey("Event")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//        TeamNameLBL.numberOfLines=0
//        TeamNameLBL.sizeToFit()
        BackView.addSubview(TeamNameLBL)
        
        let SkillIMG=UIImageView(frame: CGRectMake(TeamNameLBL.frame.origin.x+TeamNameLBL.frame.size.width+5, LocationLBL.frame.origin.y+LocationLBL.frame.size.height+10, 15, 15))
        SkillIMG.image=UIImage(named: "ic_event_levels.png")
        BackView.addSubview(SkillIMG)
        
        let SkillNameLBL=UILabel(frame: CGRectMake(SkillIMG.frame.origin.x+SkillIMG.frame.size.width+5, LocationLBL.frame.origin.y+LocationLBL.frame.size.height+10, 50, 15))
        SkillNameLBL.textColor=UIColor.whiteColor()
        SkillNameLBL.font=UIFont.systemFontOfSize(10)
        SkillNameLBL.text=RequestAry.objectAtIndex(indexPath.row).valueForKey("EventPosition")!.valueForKey("Event")!.valueForKey("vTeamSmall")!.valueForKey("SportSkill")!.valueForKey("spskillName") as? String
        BackView.addSubview(SkillNameLBL)
        
        let DollarIMG=UIImageView(frame: CGRectMake(SkillNameLBL.frame.origin.x+SkillNameLBL.frame.size.width+5, LocationLBL.frame.origin.y+LocationLBL.frame.size.height+10, 15, 15))
        DollarIMG.image=UIImage(named: "ic_blue_dollor.png")
        BackView.addSubview(DollarIMG)
        
        let DollarNameLBL=UILabel(frame: CGRectMake(DollarIMG.frame.origin.x+DollarIMG.frame.size.width, LocationLBL.frame.origin.y+LocationLBL.frame.size.height+12, 30, 15))
        DollarNameLBL.textColor=UIColor.whiteColor()
        DollarNameLBL.font=UIFont.systemFontOfSize(10)
        DollarNameLBL.text=NSString(format: "%i", RequestAry.objectAtIndex(indexPath.row).valueForKey("rate") as! Int) as String
        DollarNameLBL.numberOfLines=0
        DollarNameLBL.sizeToFit()
        BackView.addSubview(DollarNameLBL)
        
        let HourIMG=UIImageView(frame: CGRectMake(DollarNameLBL.frame.origin.x+DollarNameLBL.frame.size.width+10, LocationLBL.frame.origin.y+LocationLBL.frame.size.height+10, 15, 15))
        HourIMG.image=UIImage(named: "ic_clock_blue.png")
        BackView.addSubview(HourIMG)
        
        let HourNameLBL=UILabel(frame: CGRectMake(HourIMG.frame.origin.x+HourIMG.frame.size.width+5, LocationLBL.frame.origin.y+LocationLBL.frame.size.height+10, 30, 15))
        HourNameLBL.textColor=UIColor.whiteColor()
        HourNameLBL.font=UIFont.systemFontOfSize(10)
        HourNameLBL.text=NSString(format: "%i h", RequestAry.objectAtIndex(indexPath.row).valueForKey("EventPosition")!.valueForKey("Event")!.valueForKey("eventDuration") as! Int) as String
        BackView.addSubview(HourNameLBL)
        
        let LineIMG=UIImageView(frame: CGRectMake(0, 0, 10, BackView.frame.size.height))
        LineIMG.backgroundColor=UIColor.whiteColor()
        BackView.addSubview(LineIMG)
        
        let SmallLineIMG=UIImageView(frame: CGRectMake(0, BackView.frame.size.height-1, BackView.frame.size.width, 1))
        SmallLineIMG.backgroundColor=UIColor.whiteColor()
        BackView.addSubview(SmallLineIMG)
        
        let LastIMG=UIImageView(frame: CGRectMake(BackView.frame.size.width-20, 5, 20, 20))
        LastIMG.image=UIImage(named: "ic_general_more_details.png")
        BackView.addSubview(LastIMG)
        
        cell.addSubview(BackView)
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        
        RequestTBL.rowHeight = BackView.frame.size.height+20
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        Index=indexPath.row
        BackView.hidden=false
        PopupView.hidden=false
    }

    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
            
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            
            if let checkDict=Dict.valueForKey("response")!.valueForKey("result") as? NSDictionary
            {
                RequestAry.addObject(checkDict)
            }
            else if let checkAry=Dict.valueForKey("response")!.valueForKey("result") as? NSArray
            {
                RequestAry=NSMutableArray(array: checkAry)
            }
            RequestTBL.reloadData()
        }
        HUD.hide(true)
    }

}
