//
//  AddAvailabilityVC.swift
//  GoalieHub
//
//  Created by Vivek on 07/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class AddAvailabilityVC: UIViewController,MBProgressHUDDelegate,UITextFieldDelegate {
    var DayDict:NSDictionary!
    @IBOutlet var DayLBL: UILabel!
    var TitleStr:String!
    var HUD:MBProgressHUD!
    var ID:Int!,KEY:Int!,UserID:Int!
    
    @IBOutlet var SettimeView: UIView!
    @IBOutlet var AddBTN: UIButton!
    @IBOutlet var ButtonBackView: UIView!
    @IBOutlet var BackScroll: UIScrollView!
    var AddCount:Int=0
    var NotAvaLBL:UILabel!
    var PatternAry=NSMutableArray()
    var DayAry:NSMutableArray!
    
    var CurrentDate:String!
    var activeTextField = UITextField()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(DayDict)
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        let CurrentDateForm = NSDateFormatter()
        CurrentDateForm.dateFormat = "HH:mm"
        CurrentDate = CurrentDateForm.stringFromDate(NSDate())
        
        DayLBL.text = TitleStr
        DayAry=NSMutableArray(array: DayDict.valueForKey("Day") as! NSArray)
        if DayAry.count > 0
        {
            var Height:CGFloat=10.0
            var Tag:Int=0
            for var i=0;i<DayAry.count;i++
            {
                let DateFormatter1 = NSDateFormatter()
                DateFormatter1.dateFormat = "HH:mm:ss"
                let DateObj1 = DateFormatter1.dateFromString(DayAry.objectAtIndex(i).valueForKey("availpatdaysStartTime") as! String)
                DateFormatter1.dateFormat = "HH:mm"
                
                let TextField1=UITextField(frame: CGRectMake(20,Height,(SettimeView.frame.size.width/3)+10,35))
                TextField1.textAlignment=NSTextAlignment.Center
                TextField1.textColor=UIColor.whiteColor()
                TextField1.font=UIFont.systemFontOfSize(15)
                TextField1.delegate=self
                TextField1.text=DateFormatter1.stringFromDate(DateObj1!)
                TextField1.tag=Tag
                SettimeView.addSubview(TextField1)
                
                Tag++
                
                let border1 = CALayer()
                let width1 = CGFloat(1.0)
                border1.borderColor = UIColor.whiteColor().CGColor
                border1.frame = CGRect(x: 0, y: TextField1.frame.size.height - width1, width:  TextField1.frame.size.width, height: TextField1.frame.size.height)
                border1.borderWidth = width1
                TextField1.layer.addSublayer(border1)
                TextField1.layer.masksToBounds = true
                
                let datePickerView1:UIDatePicker = UIDatePicker()
                datePickerView1.datePickerMode = UIDatePickerMode.Time
                datePickerView1.locale = NSLocale(localeIdentifier: "da_DK")
                TextField1.inputView = datePickerView1
                datePickerView1.addTarget(self, action: "datePickerValueChanged1:", forControlEvents: UIControlEvents.ValueChanged)
                
                let Toolbar1=UIToolbar()
                Toolbar1.barStyle = UIBarStyle.Default
                Toolbar1.translucent = true
                Toolbar1.tintColor = UIColor.blackColor()
                Toolbar1.barTintColor=UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
                Toolbar1.sizeToFit()
                var doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: "donePicker")
                var CButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
                Toolbar1.setItems([CButton,doneButton],animated: false)
                Toolbar1.userInteractionEnabled = true
                TextField1.inputAccessoryView=Toolbar1
                
                let LBL=UILabel(frame: CGRectMake(TextField1.frame.size.width+TextField1.frame.origin.x+5, Height+2, 20, 35))
                LBL.text="to"
                LBL.textAlignment=NSTextAlignment.Center
                LBL.textColor=UIColor.lightGrayColor()
                LBL.font=UIFont.systemFontOfSize(15)
                SettimeView.addSubview(LBL)
                
                let DateFormatter2 = NSDateFormatter()
                DateFormatter2.dateFormat = "HH:mm:ss"
                let DateObj2 = DateFormatter2.dateFromString(DayAry.objectAtIndex(i).valueForKey("availpatdaysEndTime") as! String)
                DateFormatter2.dateFormat = "HH:mm"
                
                let TextField2=UITextField(frame: CGRectMake(LBL.frame.size.width+LBL.frame.origin.x+5, Height, (SettimeView.frame.size.width/3)+10, 35))
                TextField2.textAlignment=NSTextAlignment.Center
                TextField2.textColor=UIColor.whiteColor()
                TextField2.font=UIFont.systemFontOfSize(15)
                TextField2.delegate=self
                TextField2.text=DateFormatter2.stringFromDate(DateObj2!)
                TextField2.tag=Tag
                SettimeView.addSubview(TextField2)
                
                Tag++
                
                let border2 = CALayer()
                let width2 = CGFloat(1.0)
                border2.borderColor = UIColor.whiteColor().CGColor
                border2.frame = CGRect(x: 0, y: TextField2.frame.size.height - width2, width:  TextField2.frame.size.width, height: TextField2.frame.size.height)
                border2.borderWidth = width2
                TextField2.layer.addSublayer(border2)
                TextField2.layer.masksToBounds = true
                
                let datePickerView2:UIDatePicker = UIDatePicker()
                datePickerView2.datePickerMode = UIDatePickerMode.Time
                datePickerView2.locale = NSLocale(localeIdentifier: "da_DK")
                TextField2.inputView = datePickerView2
                datePickerView2.addTarget(self, action: "datePickerValueChanged1:", forControlEvents: UIControlEvents.ValueChanged)
                
                let Toolbar2=UIToolbar()
                Toolbar2.barStyle = UIBarStyle.Default
                Toolbar2.translucent = true
                Toolbar2.tintColor = UIColor.blackColor()
                Toolbar2.barTintColor=UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
                Toolbar2.sizeToFit()
                var doneButton2 = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: "donePicker")
                var CButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
                Toolbar2.setItems([CButton2,doneButton2], animated: false)
                Toolbar2.userInteractionEnabled = true
                TextField2.inputAccessoryView=Toolbar2
                
                
                let CloseBTN=UIButton(frame: CGRectMake(TextField2.frame.size.width+TextField2.frame.origin.x+10, Height+7, 25, 25))
                CloseBTN.setTitle("X", forState: UIControlState.Normal)
                CloseBTN.clipsToBounds=true
                CloseBTN.layer.cornerRadius=CloseBTN.frame.size.width/2
                CloseBTN.backgroundColor=UIColor.redColor()
                CloseBTN.addTarget(self, action: "CloseBTNClick:", forControlEvents: UIControlEvents.TouchUpInside)
                SettimeView.addSubview(CloseBTN)
                
                Height = Height + 45
                var PatternDict=NSMutableDictionary()
                PatternDict.setValue(DayAry.objectAtIndex(i).valueForKey("availpatdaysStartTime") as! String, forKey: "availpatdaysStartTime")
                PatternDict.setValue(DayAry.objectAtIndex(i).valueForKey("availpatdaysEndTime") as! String, forKey: "availpatdaysEndTime")
                
                //print(PatternDict)
                PatternAry.addObject(PatternDict)
               // print()
                //print(PatternAry)
                AddCount++
            }
            SettimeView.frame=CGRectMake(SettimeView.frame.origin.x, SettimeView.frame.origin.y, ButtonBackView.frame.size.width, Height+20)
            ButtonBackView.frame=CGRectMake(ButtonBackView.frame.origin.x, SettimeView.frame.size.height+SettimeView.frame.origin.y+20, ButtonBackView.frame.size.width, ButtonBackView.frame.size.height)
            if AddCount > 2
            {
                AddBTN.hidden=true
            }
        }
        else
        {
            let NotAvaLBL=UILabel(frame: CGRectMake(0, 15, SettimeView.frame.size.width, 20))
            NotAvaLBL.text="Not Available On This Day"
            NotAvaLBL.textAlignment=NSTextAlignment.Center
            NotAvaLBL.textColor=UIColor.whiteColor()
            NotAvaLBL.font=UIFont.systemFontOfSize(14)
            SettimeView.addSubview(NotAvaLBL)
            SettimeView.frame=CGRectMake(SettimeView.frame.origin.x, SettimeView.frame.origin.y, ButtonBackView.frame.size.width, 50)
            ButtonBackView.frame=CGRectMake(ButtonBackView.frame.origin.x, SettimeView.frame.size.height+SettimeView.frame.origin.y+20, ButtonBackView.frame.size.width, ButtonBackView.frame.size.height)
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func CloseBTNClick(sender:UIButton!)
    {
        PatternAry=NSMutableArray()
        AddCount--
        print(AddCount)
        SettimeView.subviews.map({ $0.removeFromSuperview() })
        var Height:CGFloat=10.0
        var Tag:Int=0
        for var i=0;i<AddCount;i++
        {
            let TextField1=UITextField(frame: CGRectMake(20, Height, (SettimeView.frame.size.width/3)+10, 35))
            TextField1.textAlignment=NSTextAlignment.Center
            TextField1.textColor=UIColor.whiteColor()
            TextField1.font=UIFont.systemFontOfSize(15)
            TextField1.delegate=self
            TextField1.tag=Tag
            SettimeView.addSubview(TextField1)
            
            Tag++
            
            let border1 = CALayer()
            let width1 = CGFloat(1.0)
            border1.borderColor = UIColor.whiteColor().CGColor
            border1.frame = CGRect(x: 0, y: TextField1.frame.size.height - width1, width:  TextField1.frame.size.width, height: TextField1.frame.size.height)
            border1.borderWidth = width1
            TextField1.layer.addSublayer(border1)
            TextField1.layer.masksToBounds = true
            
            let datePickerView1:UIDatePicker = UIDatePicker()
            datePickerView1.datePickerMode = UIDatePickerMode.Time
            datePickerView1.locale = NSLocale(localeIdentifier: "da_DK")
            TextField1.inputView = datePickerView1
            datePickerView1.addTarget(self, action: "datePickerValueChanged1:", forControlEvents: UIControlEvents.ValueChanged)
            
            let Toolbar1=UIToolbar()
            Toolbar1.barStyle = UIBarStyle.Default
            Toolbar1.translucent = true
            Toolbar1.tintColor = UIColor.blackColor()
            Toolbar1.barTintColor=UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
            Toolbar1.sizeToFit()
            var doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: "donePicker")
            var CButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            Toolbar1.setItems([CButton,doneButton], animated: false)
            Toolbar1.userInteractionEnabled = true
            TextField1.inputAccessoryView=Toolbar1
            
            
            let LBL=UILabel(frame: CGRectMake(TextField1.frame.size.width+TextField1.frame.origin.x+5, Height+2, 20, 35))
            LBL.text="to"
            LBL.textAlignment=NSTextAlignment.Center
            LBL.textColor=UIColor.lightGrayColor()
            LBL.font=UIFont.systemFontOfSize(15)
            SettimeView.addSubview(LBL)
            
            let TextField2=UITextField(frame: CGRectMake(LBL.frame.size.width+LBL.frame.origin.x+5, Height, (SettimeView.frame.size.width/3)+10, 35))
            TextField2.textAlignment=NSTextAlignment.Center
            TextField2.textColor=UIColor.whiteColor()
            TextField2.font=UIFont.systemFontOfSize(15)
            TextField2.delegate=self
            TextField2.tag=Tag
            SettimeView.addSubview(TextField2)
            
            Tag++
            
            let border2 = CALayer()
            let width2 = CGFloat(1.0)
            border2.borderColor = UIColor.whiteColor().CGColor
            border2.frame = CGRect(x: 0, y: TextField2.frame.size.height - width2, width:  TextField2.frame.size.width, height: TextField2.frame.size.height)
            border2.borderWidth = width2
            TextField2.layer.addSublayer(border2)
            TextField2.layer.masksToBounds = true
            
            let datePickerView2:UIDatePicker = UIDatePicker()
            datePickerView2.datePickerMode = UIDatePickerMode.Time
            datePickerView2.locale = NSLocale(localeIdentifier: "da_DK")
            TextField2.inputView = datePickerView2
            datePickerView2.addTarget(self, action: "datePickerValueChanged1:", forControlEvents: UIControlEvents.ValueChanged)
            
            let Toolbar2=UIToolbar()
            Toolbar2.barStyle = UIBarStyle.Default
            Toolbar2.translucent = true
            Toolbar2.tintColor = UIColor.blackColor()
            Toolbar2.barTintColor=UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
            Toolbar2.sizeToFit()
            var doneButton2 = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: "donePicker")
            var CButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            Toolbar2.setItems([CButton2,doneButton2], animated: false)
            Toolbar2.userInteractionEnabled = true
            TextField2.inputAccessoryView=Toolbar2
            
            let CloseBTN=UIButton(frame: CGRectMake(TextField2.frame.size.width+TextField2.frame.origin.x+10, Height+7, 25, 25))
            CloseBTN.setTitle("X", forState: UIControlState.Normal)
            CloseBTN.clipsToBounds=true
            CloseBTN.layer.cornerRadius=CloseBTN.frame.size.width/2
            CloseBTN.backgroundColor=UIColor.redColor()
            CloseBTN.addTarget(self, action: "CloseBTNClick:", forControlEvents: UIControlEvents.TouchUpInside)
            SettimeView.addSubview(CloseBTN)
            
            if i <= (DayAry.count-1)
            {
                let DateFormatter1 = NSDateFormatter()
                DateFormatter1.dateFormat = "HH:mm:ss"
                let DateObj1 = DateFormatter1.dateFromString(DayAry.objectAtIndex(i).valueForKey("availpatdaysStartTime") as! String)
                DateFormatter1.dateFormat = "HH:mm"
                TextField1.text=DateFormatter1.stringFromDate(DateObj1!)
                
                let DateFormatter2 = NSDateFormatter()
                DateFormatter2.dateFormat = "HH:mm:ss"
                let DateObj2 = DateFormatter2.dateFromString(DayAry.objectAtIndex(i).valueForKey("availpatdaysEndTime") as! String)
                DateFormatter2.dateFormat = "HH:mm"
                TextField2.text=DateFormatter2.stringFromDate(DateObj2!)
                var PatternDict=NSMutableDictionary()
                PatternDict.setValue(DayAry.objectAtIndex(i).valueForKey("availpatdaysStartTime") as! String, forKey: "availpatdaysStartTime")
                PatternDict.setValue(DayAry.objectAtIndex(i).valueForKey("availpatdaysEndTime") as! String, forKey: "availpatdaysEndTime")
                PatternAry.addObject(PatternDict)
            }
            else
            {
                TextField1.text=CurrentDate
                TextField2.text=CurrentDate
                
                let Current = NSDateFormatter()
                Current.dateFormat = "HH:mm:ss"
                let CurrentFinalDate = Current.stringFromDate(NSDate())
                var PatternDict=NSMutableDictionary()
                PatternDict.setValue(CurrentFinalDate, forKey: "availpatdaysStartTime")
                PatternDict.setValue(CurrentFinalDate, forKey: "availpatdaysEndTime")
                PatternAry.addObject(PatternDict)
            }
            Height = Height + 45
        }
        
        if AddCount==0
        {
            let NotAvaLBL=UILabel(frame: CGRectMake(0, 15, SettimeView.frame.size.width, 20))
            NotAvaLBL.text="Not Available On This Day"
            NotAvaLBL.textAlignment=NSTextAlignment.Center
            NotAvaLBL.textColor=UIColor.whiteColor()
            NotAvaLBL.font=UIFont.systemFontOfSize(14)
            SettimeView.addSubview(NotAvaLBL)
        }
        AddBTN.hidden=false
        SettimeView.frame=CGRectMake(SettimeView.frame.origin.x, SettimeView.frame.origin.y, ButtonBackView.frame.size.width, Height+20)
        ButtonBackView.frame=CGRectMake(ButtonBackView.frame.origin.x, SettimeView.frame.size.height+SettimeView.frame.origin.y+20, ButtonBackView.frame.size.width, ButtonBackView.frame.size.height)
    }
    
    @IBAction func SaveChangeBTNClick(sender: UIButton) {
        
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as Int
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as Int
        UserID=NSUserDefaults.standardUserDefaults().valueForKey("UserID") as! Int
        let IdentiDict=NSDictionary(objectsAndKeys: UserID,"agentId",-1,"locId",7,"availpatdaysDay")
        let ValueDict=NSDictionary(objectsAndKeys: PatternAry,"AvailabilityPatternDays")
        let ParamsDict=NSDictionary(objectsAndKeys: IdentiDict,"Identifier",ValueDict,"Value")
        let RequestDict=NSDictionary(objectsAndKeys: "setAgentAvailabilityDay","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Agent",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
        }
        
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
        }
    }
    
    @IBAction func CancelBTNClick(sender: UIButton) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func datePickerValueChanged1(sender:UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        activeTextField.text = dateFormatter.stringFromDate(sender.date)
        print(activeTextField.tag)
        let Current = NSDateFormatter()
        Current.dateFormat = "HH:mm:ss"
        let CurrentFinalDate = Current.stringFromDate(sender.date)
        //print(PatternAry)
        var PatternDict=NSMutableDictionary()
        if activeTextField.tag==0
        {
            PatternDict.setValue(CurrentFinalDate, forKey: "availpatdaysStartTime")
            PatternDict.setValue(PatternAry.objectAtIndex(0).valueForKey("availpatdaysEndTime") as! String, forKey: "availpatdaysEndTime")
            PatternAry.replaceObjectAtIndex(0, withObject: PatternDict)
        }
        else if activeTextField.tag==1
        {
            PatternDict.setValue(PatternAry.objectAtIndex(0).valueForKey("availpatdaysStartTime") as! String, forKey: "availpatdaysStartTime")
            PatternDict.setValue(CurrentFinalDate, forKey: "availpatdaysEndTime")
            PatternAry.replaceObjectAtIndex(0, withObject: PatternDict)
        }
        else if activeTextField.tag==2
        {
            PatternDict.setValue(PatternAry.objectAtIndex(1).valueForKey("availpatdaysStartTime") as! String, forKey: "availpatdaysStartTime")
            PatternDict.setValue(CurrentFinalDate, forKey: "availpatdaysEndTime")
            PatternAry.replaceObjectAtIndex(1, withObject: PatternDict)
        }
        else if activeTextField.tag==3
        {
            PatternDict.setValue(CurrentFinalDate, forKey: "availpatdaysStartTime")
            PatternDict.setValue(PatternAry.objectAtIndex(1).valueForKey("availpatdaysEndTime") as! String, forKey: "availpatdaysEndTime")
            PatternAry.replaceObjectAtIndex(1, withObject: PatternDict)
        }
        else if activeTextField.tag==4
        {
            PatternDict.setValue(PatternAry.objectAtIndex(2).valueForKey("availpatdaysStartTime") as! String, forKey: "availpatdaysStartTime")
            PatternDict.setValue(CurrentFinalDate, forKey: "availpatdaysEndTime")
            PatternAry.replaceObjectAtIndex(2, withObject: PatternDict)
        }
        else if activeTextField.tag==5
        {
            PatternDict.setValue(CurrentFinalDate, forKey: "availpatdaysStartTime")
            PatternDict.setValue(PatternAry.objectAtIndex(2).valueForKey("availpatdaysEndTime") as! String, forKey: "availpatdaysEndTime")
            PatternAry.replaceObjectAtIndex(2, withObject: PatternDict)
        }
        print(PatternAry)
    }
    func donePicker()
    {
       self.activeTextField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.activeTextField = textField
    }
    @IBAction func SaveBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func AddBTNClick(sender: UIButton) {
        PatternAry=NSMutableArray()
        AddCount++
        SettimeView.subviews.map({ $0.removeFromSuperview() })
        var Height:CGFloat=10.0
        var Tag:Int=0
        for var i=0;i<AddCount;i++
        {
            let TextField1=UITextField(frame: CGRectMake(20, Height, (SettimeView.frame.size.width/3)+10, 35))
            TextField1.textAlignment=NSTextAlignment.Center
            TextField1.textColor=UIColor.whiteColor()
            TextField1.font=UIFont.systemFontOfSize(15)
            TextField1.delegate=self
            TextField1.tag=Tag
            SettimeView.addSubview(TextField1)
            
            Tag++
            
            let border1 = CALayer()
            let width1 = CGFloat(1.0)
            border1.borderColor = UIColor.whiteColor().CGColor
            border1.frame = CGRect(x: 0, y: TextField1.frame.size.height - width1, width:  TextField1.frame.size.width, height: TextField1.frame.size.height)
            border1.borderWidth = width1
            TextField1.layer.addSublayer(border1)
            TextField1.layer.masksToBounds = true
            
            let datePickerView1:UIDatePicker = UIDatePicker()
            datePickerView1.datePickerMode = UIDatePickerMode.Time
            datePickerView1.locale = NSLocale(localeIdentifier: "da_DK")
            TextField1.inputView = datePickerView1
            datePickerView1.addTarget(self, action: "datePickerValueChanged1:", forControlEvents: UIControlEvents.ValueChanged)
            
            let Toolbar1=UIToolbar()
            Toolbar1.barStyle = UIBarStyle.Default
            Toolbar1.translucent = true
            Toolbar1.tintColor = UIColor.blackColor()
            Toolbar1.barTintColor=UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
            Toolbar1.sizeToFit()
            var doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: "donePicker")
            var CButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            Toolbar1.setItems([CButton,doneButton], animated: false)
            Toolbar1.userInteractionEnabled = true
            TextField1.inputAccessoryView=Toolbar1
            
            let LBL=UILabel(frame: CGRectMake(TextField1.frame.size.width+TextField1.frame.origin.x+5, Height+2, 20, 35))
            LBL.text="to"
            LBL.textAlignment=NSTextAlignment.Center
            LBL.textColor=UIColor.lightGrayColor()
            LBL.font=UIFont.systemFontOfSize(15)
            SettimeView.addSubview(LBL)
            
            let TextField2=UITextField(frame: CGRectMake(LBL.frame.size.width+LBL.frame.origin.x+5, Height, (SettimeView.frame.size.width/3)+10, 35))
            TextField2.textAlignment=NSTextAlignment.Center
            TextField2.textColor=UIColor.whiteColor()
            TextField2.font=UIFont.systemFontOfSize(15)
            TextField2.delegate=self
            TextField2.tag=Tag
            SettimeView.addSubview(TextField2)
            
            Tag++
            
            
            let border2 = CALayer()
            let width2 = CGFloat(1.0)
            border2.borderColor = UIColor.whiteColor().CGColor
            border2.frame = CGRect(x: 0, y: TextField2.frame.size.height - width2, width:  TextField2.frame.size.width, height: TextField2.frame.size.height)
            border2.borderWidth = width2
            TextField2.layer.addSublayer(border2)
            TextField2.layer.masksToBounds = true
            
            let datePickerView2:UIDatePicker = UIDatePicker()
            datePickerView2.datePickerMode = UIDatePickerMode.Time
            datePickerView2.locale = NSLocale(localeIdentifier: "da_DK")
            TextField2.inputView = datePickerView2
            datePickerView2.addTarget(self, action: "datePickerValueChanged1:", forControlEvents: UIControlEvents.ValueChanged)
            
            let Toolbar2=UIToolbar()
            Toolbar2.barStyle = UIBarStyle.Default
            Toolbar2.translucent = true
            Toolbar2.tintColor = UIColor.blackColor()
            Toolbar2.barTintColor=UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
            Toolbar2.sizeToFit()
            var doneButton2 = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: "donePicker")
            var CButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            Toolbar2.setItems([CButton2,doneButton2], animated: false)
            Toolbar2.userInteractionEnabled = true
            TextField2.inputAccessoryView=Toolbar2
            
            let CloseBTN=UIButton(frame: CGRectMake(TextField2.frame.size.width+TextField2.frame.origin.x+10, Height+7, 25, 25))
            CloseBTN.setTitle("X", forState: UIControlState.Normal)
            CloseBTN.clipsToBounds=true
            CloseBTN.layer.cornerRadius=CloseBTN.frame.size.width/2
            CloseBTN.backgroundColor=UIColor.redColor()
            CloseBTN.addTarget(self, action: "CloseBTNClick:", forControlEvents: UIControlEvents.TouchUpInside)
            SettimeView.addSubview(CloseBTN)
            
            Height = Height + 45
            
            if i <= (DayAry.count-1)
            {
                let DateFormatter1 = NSDateFormatter()
                DateFormatter1.dateFormat = "HH:mm:ss"
                let DateObj1 = DateFormatter1.dateFromString(DayAry.objectAtIndex(i).valueForKey("availpatdaysStartTime") as! String)
                DateFormatter1.dateFormat = "HH:mm"
                TextField1.text=DateFormatter1.stringFromDate(DateObj1!)
                
                let DateFormatter2 = NSDateFormatter()
                DateFormatter2.dateFormat = "HH:mm:ss"
                let DateObj2 = DateFormatter2.dateFromString(DayAry.objectAtIndex(i).valueForKey("availpatdaysEndTime") as! String)
                DateFormatter2.dateFormat = "HH:mm"
                TextField2.text=DateFormatter2.stringFromDate(DateObj2!)
                var PatternDict=NSMutableDictionary()
                PatternDict.setValue(DayAry.objectAtIndex(i).valueForKey("availpatdaysStartTime") as! String, forKey: "availpatdaysStartTime")
                PatternDict.setValue(DayAry.objectAtIndex(i).valueForKey("availpatdaysEndTime") as! String, forKey: "availpatdaysEndTime")
                PatternAry.addObject(PatternDict)
                
            }
            else
            {
                TextField1.text=CurrentDate
                TextField2.text=CurrentDate
                
                let Current = NSDateFormatter()
                Current.dateFormat = "HH:mm:ss"
                let CurrentFinalDate = Current.stringFromDate(NSDate())
                var PatternDict=NSMutableDictionary()
                PatternDict.setValue(CurrentFinalDate, forKey: "availpatdaysStartTime")
                PatternDict.setValue(CurrentFinalDate, forKey: "availpatdaysEndTime")
                PatternAry.addObject(PatternDict)
            }
        }
        SettimeView.frame=CGRectMake(SettimeView.frame.origin.x, SettimeView.frame.origin.y, ButtonBackView.frame.size.width, Height+20)
        ButtonBackView.frame=CGRectMake(ButtonBackView.frame.origin.x, SettimeView.frame.size.height+SettimeView.frame.origin.y+20, ButtonBackView.frame.size.width, ButtonBackView.frame.size.height)
        
        if AddCount > 2
        {
            AddBTN.hidden=true
        }
        
    }
    override func viewWillAppear(animated: Bool) {
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }

}
