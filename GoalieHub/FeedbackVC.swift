//
//  FeedbackVC.swift
//  GoalieHub
//
//  Created by Vivek on 11/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class FeedbackVC: UIViewController,MBProgressHUDDelegate{

    @IBOutlet var BackScroll: UIScrollView!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "FeedbackLoad:", name:"Feedback", object: nil)
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func FeedbackLoad(notification: NSNotification){
        HUD.show(true)
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        let IdentiDict=NSDictionary(objectsAndKeys: 102,"userId")
        let ValueDict=NSDictionary(objectsAndKeys: "user","sortBy","desc","sortDirection","all","viewType",25,"limitResult",1,"pageNumber")
        let ParamsDict=NSDictionary(objectsAndKeys: IdentiDict,"Identifier",ValueDict,"Value")
        let RequestDict=NSDictionary(objectsAndKeys: "getUserFeedback","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "User",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
        }
    }
    
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            var FeedBackAry=NSMutableArray(array: Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("RatingDetail") as! NSArray)
            var Height:CGFloat=10.0
            for var i=0;i<FeedBackAry.count;i++
            {
                let BackView=UIView(frame: CGRectMake(10, Height, BackScroll.frame.size.width-20, 50))
                //BackView.backgroundColor=UIColor.whiteColor()
                
                let PImage=UIImageView(frame: CGRectMake(25, 0, 20, 20))
                BackView.addSubview(PImage)
                
                let PLBL=UILabel(frame: CGRectMake(15, PImage.frame.size.height+5, 45, 15))
                if FeedBackAry.objectAtIndex(i).valueForKey("ratdetValue")!.isEqualToString("P")
                {
                    PLBL.text = "Positive"
                    PImage.image=UIImage(named: "ic_right_big.png")
                }
                else
                {
                    PLBL.text = "Negative"
                    PImage.image=UIImage(named: "ic_feed_back_wrong.png")
                }
                PLBL.textColor=UIColor.whiteColor()
                PLBL.font=UIFont.systemFontOfSize(10)
                BackView.addSubview(PLBL)
                
                let decodedData = NSData(base64EncodedString: FeedBackAry.objectAtIndex(i).valueForKey("vUserSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
                let UserIMG=UIImageView(frame: CGRectMake(PLBL.frame.size.width+PLBL.frame.origin.x+3, 5, 20, 20))
                UserIMG.image=UIImage(data: decodedData!)
                BackView.addSubview(UserIMG)
                
                let DateFormatter = NSDateFormatter()
                DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let DateObj = DateFormatter.dateFromString(FeedBackAry.objectAtIndex(i).valueForKey("ratdetCreateTs") as! String)
                DateFormatter.dateFormat = "dd/MM HH:mm a"
                
                let DateLBL=UILabel(frame: CGRectMake(BackView.frame.size.width-90, 8, 75, 15))
                DateLBL.text=DateFormatter.stringFromDate(DateObj!)
                DateLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                DateLBL.font=UIFont.systemFontOfSize(9)
                BackView.addSubview(DateLBL)
                
                let WroteLBL=UILabel(frame: CGRectMake(DateLBL.frame.origin.x-30, 8, 30, 15))
                WroteLBL.text="Wrote:"
                WroteLBL.textColor=UIColor.whiteColor()
                WroteLBL.font=UIFont.systemFontOfSize(9)
                BackView.addSubview(WroteLBL)
                
                let NameLBL=UILabel(frame: CGRectMake(UserIMG.frame.size.width+UserIMG.frame.origin.x+3, 8, BackView.frame.size.width-108, 15))
                NameLBL.text=FeedBackAry.objectAtIndex(i).valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
                NameLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                NameLBL.font=UIFont.systemFontOfSize(11)
                BackView.addSubview(NameLBL)
                
                let CommentLBL=UILabel(frame: CGRectMake(PLBL.frame.size.width+PLBL.frame.origin.x+3, UserIMG.frame.size.height+UserIMG.frame.origin.y+5, BackView.frame.size.width, 15))
                CommentLBL.text=FeedBackAry.objectAtIndex(i).valueForKey("ratdetComments") as? String
                CommentLBL.textColor=UIColor.whiteColor()
                CommentLBL.font=UIFont.systemFontOfSize(13)
                CommentLBL.numberOfLines=0
                CommentLBL.sizeToFit()
                BackView.addSubview(CommentLBL)
                
                BackView.frame=CGRectMake(10, Height, BackScroll.frame.size.width-20, CommentLBL.frame.size.height+CommentLBL.frame.origin.y+7)
                
                let LineIMG=UIImageView(frame: CGRectMake(0, 0, 10, BackView.frame.size.height))
                LineIMG.backgroundColor=UIColor.whiteColor()
                BackView.addSubview(LineIMG)
                
                let SmallLineIMG=UIImageView(frame: CGRectMake(0, BackView.frame.size.height-1, BackView.frame.size.width, 1))
                SmallLineIMG.backgroundColor=UIColor.whiteColor()
                BackView.addSubview(SmallLineIMG)
                
                let LastIMG=UIImageView(frame: CGRectMake(BackView.frame.size.width-20, 5, 20, 20))
                LastIMG.image=UIImage(named: "ic_general_more_details.png")
                BackView.addSubview(LastIMG)
                
                BackScroll.addSubview(BackView)
                
                Height = Height + BackView.frame.size.height + 10
            }
            BackScroll.contentSize=CGSizeMake(0, Height+10)
            HUD.hide(true)
        }
    }
}
