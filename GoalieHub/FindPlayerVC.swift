//
//  FindPlayerVC.swift
//  GoalieHub
//
//  Created by Vivek on 22/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
var TeamID:Int=0
var TeamIMGStr:String!
var EventDetailsDict:NSMutableDictionary!
class FindPlayerVC: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate,MBProgressHUDDelegate {

    @IBOutlet var ORLBL: UILabel!
    @IBOutlet var RequestBTN: UIButton!
    @IBOutlet var FindPlayerBTN: UIButton!
    @IBOutlet var TextLBL: UILabel!
    @IBOutlet var LineIMG1: UIImageView!
    @IBOutlet var EventBTN: UIButton!
    @IBOutlet var SelectEventLBL: UILabel!
    @IBOutlet var LineIMG: UIImageView!
    @IBOutlet var EventLBL: UILabel!
    @IBOutlet var TimeLBL: UILabel!
    @IBOutlet var DateLBL: UILabel!
    @IBOutlet var TitleLBL: UILabel!
    @IBOutlet var EventBackView: UIView!
    @IBOutlet var TeamNameLBL: UILabel!
    @IBOutlet var TeamIMG: UIImageView!
    var TeamAry=NSMutableArray(),EventAry=NSMutableArray()
    var pickerView:UIPickerView!
    var pickerToolbar:UIToolbar!
    var DetectPicker:Int=0
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        TeamIMG.clipsToBounds=true
        TeamIMG.layer.cornerRadius=TeamIMG.frame.size.width/2
        
        pickerView=UIPickerView(frame: CGRectMake(0, self.view.frame.size.height-265, self.view.frame.size.width, 150))
        pickerView.dataSource=self
        pickerView.delegate=self
        pickerView.backgroundColor=UIColor.whiteColor()
        self.view.addSubview(pickerView)
        pickerToolbar=UIToolbar(frame: CGRectMake(0, pickerView.frame.origin.y-35, self.view.frame.size.width, 35))
        pickerToolbar.barStyle = UIBarStyle.Default
        pickerToolbar.translucent = true
        pickerToolbar.tintColor = UIColor.blackColor()
        pickerToolbar.barTintColor=UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
        //pickerToolbar.sizeToFit()
        
        var doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: "donePicker")
        var CButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        pickerToolbar.setItems([CButton,doneButton], animated: false)
        pickerToolbar.userInteractionEnabled = true
        self.view.addSubview(pickerToolbar)
        pickerView.hidden=true
        pickerToolbar.hidden=true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func RequestBTNClick(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("PublicPostingVC") as! PublicPostingVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    @IBAction func FindPlayerBTNClick(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("CriteriaVC") as! CriteriaVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    @IBAction func EventBTNClick(sender: UIButton) {
        //DetectPicker=2
        pickerView.hidden=false
        pickerToolbar.hidden=false
        EventBackView.hidden=false
        SelectEventLBL.hidden=true
        TextLBL.hidden=false
        FindPlayerBTN.hidden=false
        RequestBTN.hidden=false
        ORLBL.hidden=false
        print(EventAry)
        TitleLBL.text=EventAry.objectAtIndex(0).valueForKey("eventName") as? String
        let DateFormatter = NSDateFormatter()
        DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let DateObj = DateFormatter.dateFromString(EventAry.objectAtIndex(0).valueForKey("eventCreateTs") as! String)
        DateFormatter.dateFormat = "EE,M/dd h:mm a"
        DateLBL.text=DateFormatter.stringFromDate(DateObj!)
        TimeLBL.text=EventAry.objectAtIndex(0).valueForKey("eventTs") as? String
        
        EventDetailsDict=EventAry.objectAtIndex(0) as! NSMutableDictionary
    }
    func donePicker()
    {
        pickerView.hidden=true
        pickerToolbar.hidden=true
        
        if DetectPicker==1
        {
            HUD.show(true)
            EventAry.removeAllObjects()
            ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
            KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
            let IdentiDict=NSDictionary(objectsAndKeys: TeamID,"teamId")
            let ValueDict=NSDictionary(objectsAndKeys: "all","viewType")
            let ParamsDict=NSDictionary(objectsAndKeys:IdentiDict,"Identifier",ValueDict,"Value")
            let RequestDict=NSDictionary(objectsAndKeys: "getTeamEvents","method","1","id",ParamsDict,"params")
            print(RequestDict)
            let param = [
                "id"    : ID,
                "key"    : KEY,
                "callBack"    : "myCallBackMethod",
                "serviceName"    : "Event",
                "request" : RequestDict
            ]
            println(param)
            Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
                .responseJSON {response in
                    self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
            }
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func TeamBTNClick(sender: UIButton) {
        
        DetectPicker=1
        
        pickerView.hidden=false
        pickerToolbar.hidden=false
        TeamIMGStr=TeamAry.objectAtIndex(0).valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String
        let decodedData = NSData(base64EncodedString: TeamAry.objectAtIndex(0).valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
        TeamIMG.image=UIImage(data: decodedData!)
        TeamNameLBL.text=TeamAry.objectAtIndex(0).valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
        
        TeamID=TeamAry.objectAtIndex(0).valueForKey("vTeamSmall")!.valueForKey("teamId") as! Int
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: false)
    }

    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            if let CheckDict = Dict.valueForKey("response")!.valueForKey("result") as? NSDictionary
            {
                EventAry.addObject(CheckDict)
            }
            else if let CheckAry = Dict.valueForKey("response")!.valueForKey("result") as? NSArray
            {
                EventAry=NSMutableArray(array: CheckAry)
            }
            DetectPicker=2
            EventLBL.hidden=false
            SelectEventLBL.hidden=false
            EventBTN.hidden=false
            LineIMG.hidden=false
            LineIMG1.hidden=false
            pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
        }
        HUD.hide(true)
    }
    
    /******************************************************
    MARK:PickerView Delegate Method
    ******************************************************/
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if DetectPicker==1
        {
            return TeamAry.count
        }
        else
        {
            return EventAry.count
        }
        
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if DetectPicker==1
        {
            let decodedData = NSData(base64EncodedString: TeamAry.objectAtIndex(row).valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TeamIMG.image=UIImage(data: decodedData!)
            TeamNameLBL.text=TeamAry.objectAtIndex(row).valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
            TeamID=TeamAry.objectAtIndex(row).valueForKey("vTeamSmall")!.valueForKey("teamId") as! Int
            TeamIMGStr=TeamAry.objectAtIndex(row).valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String
        }
        else
        {
            TitleLBL.text=EventAry.objectAtIndex(row).valueForKey("eventName") as? String
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(EventAry.objectAtIndex(row).valueForKey("eventCreateTs") as! String)
            DateFormatter.dateFormat = "EE,M/dd h:mm a"
            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
            TimeLBL.text=EventAry.objectAtIndex(row).valueForKey("eventTs") as? String
            EventDetailsDict=EventAry.objectAtIndex(row) as! NSMutableDictionary
        }
        
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        if DetectPicker==1
        {
            return TeamAry.objectAtIndex(row).valueForKey("vTeamSmall")!.valueForKey("teamName") as! String
        }
        else
        {
            return EventAry.objectAtIndex(row).valueForKey("eventName") as? String
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }

}
