//
//  GeneralVC.swift
//  GoalieHub
//
//  Created by Vivek on 02/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class GeneralVC: UIViewController,MBProgressHUDDelegate,UIPickerViewDataSource,UIPickerViewDelegate {
    var registerUDIDConnectionAgentDetails=NSURLConnection()
    var registerUDIDDataAgentDetails : NSMutableData!
    var registerUDIDConnectionSave=NSURLConnection()
    var registerUDIDDataSave : NSMutableData!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!,detectPicker:Int!
    var AgentDetailAry=NSMutableArray(),PickerValue=NSMutableArray()
    var pickerView:UIPickerView!
    var pickerToolbar:UIToolbar!
    var HourBTN:UIButton!
    
    @IBOutlet var CheckIMG5: UIImageView!
    @IBOutlet var CheckIMG4: UIImageView!
    @IBOutlet var CheckIMG3: UIImageView!
    @IBOutlet var CheckIMG2: UIImageView!
    @IBOutlet var CheckIMG1: UIImageView!
    @IBOutlet var MHBGButton: UIButton!
    @IBOutlet var MGPDButton: UIButton!
    @IBOutlet var MGPWButton: UIButton!
    @IBOutlet var MaxButton: UIButton!
    @IBOutlet var MinButton: UIButton!
    @IBOutlet var RateButton: UIButton!
    @IBOutlet var BackScroll: UIScrollView!
    @IBOutlet var SaveBTN: UIButton!
    var MaxBookDay:Int!,MaxBookWeek:Int!,MinHrsRest:Int!,EventRate:Int!,MinSkillID:Int!,MaxSkillID:Int!
    var AgentTypeArray=NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        
        BackScroll.contentSize=CGSizeMake(0, SaveBTN.frame.size.height+SaveBTN.frame.origin.y+10)
        
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "GeneralLoad:", name:"General", object: nil)
        
        pickerView=UIPickerView(frame: CGRectMake(0, (self.view.frame.size.height/3)+20, self.view.frame.size.width, self.view.frame.size.height-(self.view.frame.size.height/3)+20))
        pickerView.dataSource=self
        pickerView.delegate=self
        pickerView.backgroundColor=UIColor.whiteColor()
        self.view.addSubview(pickerView)
        pickerToolbar=UIToolbar(frame: CGRectMake(0, pickerView.frame.origin.y-35, self.view.frame.size.width, 35))
        pickerToolbar.barStyle = UIBarStyle.Default
        pickerToolbar.translucent = true
        pickerToolbar.tintColor = UIColor.blackColor()
        pickerToolbar.barTintColor=UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
        //pickerToolbar.sizeToFit()
        
        var doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: "donePicker")
        var CButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        pickerToolbar.setItems([CButton,doneButton], animated: false)
        pickerToolbar.userInteractionEnabled = true
        self.view.addSubview(pickerToolbar)
        pickerView.hidden=true
        pickerToolbar.hidden=true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func GeneralLoad(notification: NSNotification){
        AgentTypeArray.removeAllObjects()
        HUD.show(true)
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        let ParamsDict=NSDictionary()
        let RequestDict=NSDictionary(objectsAndKeys: "getMyAgents","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Agent",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse1(response.3, Dict: response.2 as? NSDictionary)
        }
        
        
    }
    
    func HandleAPIResponse1(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            AgentID=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("agentId") as! Int
            LocationID=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AgentSettings")!.valueForKey("locId") as! Int
            
            ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
            KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
            let ParamsDict=NSDictionary(objectsAndKeys: AgentID,"agentId",LocationID,"locId")
            let RequestDict=NSDictionary(objectsAndKeys: "getMyAgentSettings","method","1","id",ParamsDict,"params")
            print(RequestDict)
            let param = [
                "id"    : ID,
                "key"    : KEY,
                "callBack"    : "myCallBackMethod",
                "serviceName"    : "Agent",
                "request" : RequestDict
            ]
            println(param)
            Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
                .responseJSON {response in
                    self.HandleAPIResponse2(response.3, Dict: response.2 as? NSDictionary)
            }
        }
        HUD.hide(true)
    }
    func HandleAPIResponse2(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            if let CheckNull=Dict.valueForKey("response")!.valueForKey("result") as? NSNull
            {
                
            }
            else
            {
                NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
                NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
                EventRate = Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AgentSettings")!.valueForKey("agentsetEventrate") as! Int
                
                RateButton.setTitle(NSString(format: "$%@.0", Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AgentSettings")!.valueForKey("agentsetEventrate") as! NSNumber) as String, forState: UIControlState.Normal)
                var SetMinStr=NSString(format: "%@", Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AgentSettings")!.valueForKey("agentsetMinSkillid") as! NSNumber)
                MinSkillID=SetMinStr.integerValue
                if SetMinStr.isEqualToString("7")
                {
                    SetMinStr="DIV A"
                }
                else if SetMinStr.isEqualToString("6")
                {
                    SetMinStr="DIV A/B"
                }
                else if SetMinStr.isEqualToString("5")
                {
                    SetMinStr="DIV B"
                }
                else if SetMinStr.isEqualToString("4")
                {
                    SetMinStr="DIV B/C"
                }
                else if SetMinStr.isEqualToString("3")
                {
                    SetMinStr="DIV C"
                }
                else if SetMinStr.isEqualToString("2")
                {
                    SetMinStr="DIV C/D"
                }
                else if SetMinStr.isEqualToString("1")
                {
                    SetMinStr="DIV D"
                }
                else if SetMinStr.isEqualToString("0")
                {
                    SetMinStr="Beginner"
                }
                
                MinButton.setTitle(SetMinStr as String, forState: UIControlState.Normal)
                
                var SetMaxStr=NSString(format: "%@", Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AgentSettings")!.valueForKey("agentsetMaxSkillid") as! NSNumber)
                MaxSkillID=SetMaxStr.integerValue
                if SetMinStr.isEqualToString("7")
                {
                    SetMaxStr="DIV A"
                }
                else if SetMinStr.isEqualToString("6")
                {
                    SetMaxStr="DIV A/B"
                }
                else if SetMinStr.isEqualToString("5")
                {
                    SetMaxStr="DIV B"
                }
                else if SetMinStr.isEqualToString("4")
                {
                    SetMaxStr="DIV B/C"
                }
                else if SetMinStr.isEqualToString("3")
                {
                    SetMaxStr="DIV C"
                }
                else if SetMinStr.isEqualToString("2")
                {
                    SetMaxStr="DIV C/D"
                }
                else if SetMinStr.isEqualToString("1")
                {
                    SetMaxStr="DIV D"
                }
                else if SetMinStr.isEqualToString("0")
                {
                    SetMaxStr="Beginner"
                }
                MaxButton.setTitle(SetMaxStr as String, forState: UIControlState.Normal)
                
                if var AgentEventTypeAry=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AgentEventType") as? NSMutableArray
                {
                    for var i=0;i<AgentEventTypeAry.count;i++
                    {
                        let typecodeSTR=AgentEventTypeAry.objectAtIndex(i).valueForKey("TypeCode")!.valueForKey("typcodeId") as! NSNumber
                        AgentTypeArray.addObject(typecodeSTR)
                        if typecodeSTR==1
                        {
                            CheckIMG3.image=UIImage(named: "bg_small_checkbox_checked.png")
                        }
                        else if typecodeSTR == 2
                        {
                            CheckIMG1.image=UIImage(named: "bg_small_checkbox_checked.png")
                        }
                        else if typecodeSTR == 3
                        {
                            CheckIMG2.image=UIImage(named: "bg_small_checkbox_checked.png")
                        }
                        else if typecodeSTR == 4
                        {
                            CheckIMG4.image=UIImage(named: "bg_small_checkbox_checked.png")
                        }
                        else if typecodeSTR == 5
                        {
                            CheckIMG5.image=UIImage(named: "bg_small_checkbox_checked.png")
                        }
                    }
                }
                else
                {
                    let typecodeSTR=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AgentEventType")!.valueForKey("TypeCode")!.valueForKey("typcodeId") as! NSNumber
                    AgentTypeArray.addObject(typecodeSTR)
                    if typecodeSTR==1
                    {
                        CheckIMG3.image=UIImage(named: "bg_small_checkbox_checked.png")
                    }
                    else if typecodeSTR == 2
                    {
                        CheckIMG1.image=UIImage(named: "bg_small_checkbox_checked.png")
                    }
                    else if typecodeSTR == 3
                    {
                        CheckIMG2.image=UIImage(named: "bg_small_checkbox_checked.png")
                    }
                    else if typecodeSTR == 4
                    {
                        CheckIMG4.image=UIImage(named: "bg_small_checkbox_checked.png")
                    }
                    else if typecodeSTR == 5
                    {
                        CheckIMG5.image=UIImage(named: "bg_small_checkbox_checked.png")
                    }
                }
                MaxBookWeek=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AgentSettings")!.valueForKey("agentsetMaxBookWeek") as! Int
                MaxBookDay=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AgentSettings")!.valueForKey("agentsetMaxBookDay") as! Int
                MinHrsRest=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AgentSettings")!.valueForKey("agentsetMinHrsRest") as! Int
                
                MGPWButton.setTitle(NSString(format: "%@", Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AgentSettings")!.valueForKey("agentsetMaxBookWeek") as! NSNumber) as String, forState: UIControlState.Normal)
                MGPDButton.setTitle(NSString(format: "%@", Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AgentSettings")!.valueForKey("agentsetMaxBookDay") as! NSNumber) as String, forState: UIControlState.Normal)
                MHBGButton.setTitle(NSString(format: "%@", Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AgentSettings")!.valueForKey("agentsetMinHrsRest") as! NSNumber) as String, forState: UIControlState.Normal)
            }
        }
        HUD.hide(true)
    }
    func HandleAPIResponse3(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
        }
        HUD.hide(true)
    }
    @IBAction func MaximumBTN(sender: UIButton) {
        detectPicker=1
        PickerValue.removeAllObjects()
        PickerValue=NSMutableArray(objects: "Beginner","DIV D","DIV C/D","DIV C","DIV B/C","DIV B","DIV A/B","DIV A")
        OpenPicker()
    }
    
    @IBAction func MinimumBTN(sender: UIButton) {
        detectPicker=2
        PickerValue.removeAllObjects()
        PickerValue=NSMutableArray(objects: "Beginner","DIV D","DIV C/D","DIV C","DIV B/C","DIV B","DIV A/B","DIV A")
        OpenPicker()
    }
    
    @IBAction func rateBTN(sender: UIButton) {
        detectPicker=3
        PickerValue.removeAllObjects()
        PickerValue=NSMutableArray(objects: "0","5","10","15","20","25","30","35","40","45","50","55","60","65","70")
        OpenPicker()
    }
    @IBAction func BTN4(sender: UIButton) {
        if CheckIMG4.image!.isEqual(UIImage(named: "bg_small_checkbox_checked.png"))
        {
            CheckIMG4.image=UIImage(named: "bg_small_checkbox_un_checked.png")
            for var i=0;i<AgentTypeArray.count;i++
            {
                if AgentTypeArray.objectAtIndex(i).integerValue == 4
                {
                    AgentTypeArray.removeObjectAtIndex(i)
                }
            }
        }
        else
        {
            CheckIMG4.image=UIImage(named: "bg_small_checkbox_checked.png")
            AgentTypeArray.addObject(4)
        }
    }
    @IBAction func BTN5(sender: UIButton) {
        if CheckIMG5.image!.isEqual(UIImage(named: "bg_small_checkbox_checked.png"))
        {
            CheckIMG5.image=UIImage(named: "bg_small_checkbox_un_checked.png")
            for var i=0;i<AgentTypeArray.count;i++
            {
                if AgentTypeArray.objectAtIndex(i).integerValue == 5
                {
                    AgentTypeArray.removeObjectAtIndex(i)
                }
            }
        }
        else
        {
            CheckIMG5.image=UIImage(named: "bg_small_checkbox_checked.png")
            AgentTypeArray.addObject(5)
        }
    }
    @IBAction func BTN3(sender: UIButton) {
        if CheckIMG3.image!.isEqual(UIImage(named: "bg_small_checkbox_checked.png"))
        {
            CheckIMG3.image=UIImage(named: "bg_small_checkbox_un_checked.png")
            for var i=0;i<AgentTypeArray.count;i++
            {
                if AgentTypeArray.objectAtIndex(i).integerValue == 1
                {
                    AgentTypeArray.removeObjectAtIndex(i)
                }
            }
        }
        else
        {
            CheckIMG3.image=UIImage(named: "bg_small_checkbox_checked.png")
            AgentTypeArray.addObject(1)
        }
    }
    @IBAction func BTN2(sender: UIButton) {
        if CheckIMG2.image!.isEqual(UIImage(named: "bg_small_checkbox_checked.png"))
        {
            CheckIMG2.image=UIImage(named: "bg_small_checkbox_un_checked.png")
            for var i=0;i<AgentTypeArray.count;i++
            {
                if AgentTypeArray.objectAtIndex(i).integerValue == 3
                {
                    AgentTypeArray.removeObjectAtIndex(i)
                }
            }
        }
        else
        {
            CheckIMG2.image=UIImage(named: "bg_small_checkbox_checked.png")
            AgentTypeArray.addObject(3)
        }
    }
    @IBAction func BTN1(sender: UIButton)
    {
        if CheckIMG1.image!.isEqual(UIImage(named: "bg_small_checkbox_checked.png"))
        {
            CheckIMG1.image=UIImage(named: "bg_small_checkbox_un_checked.png")
            
            for var i=0;i<AgentTypeArray.count;i++
            {
                if AgentTypeArray.objectAtIndex(i).integerValue == 2
                {
                    AgentTypeArray.removeObjectAtIndex(i)
                }
            }
        }
        else
        {
            CheckIMG1.image=UIImage(named: "bg_small_checkbox_checked.png")
            AgentTypeArray.addObject(2)
        }
    }
    
    @IBAction func mgwBTN(sender: UIButton) {
        detectPicker=4
        PickerValue.removeAllObjects()
        PickerValue=NSMutableArray(objects: "1","2","3","4","5","6","7")
        OpenPicker()
    }
    @IBAction func mgdBTN(sender: UIButton) {
        detectPicker=5
        PickerValue.removeAllObjects()
        PickerValue=NSMutableArray(objects: "1","2","3","4")
        OpenPicker()
    }
    
    @IBAction func mhbgBTN(sender: UIButton) {
        detectPicker=6
        PickerValue.removeAllObjects()
        PickerValue=NSMutableArray(objects: "0","1","2","3","4","5","6")
        OpenPicker()
    }
    
    
    @IBAction func SaveBTNClick(sender: UIButton) {
        
        HUD.show(true)
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        let IdentifierDict=NSDictionary(objectsAndKeys: AgentID,"agentId",LocationID,"locId")
        let AgentSettingDict=NSDictionary(objectsAndKeys: MaxBookDay,"agentsetMaxBookDay",MaxBookWeek,"agentsetMaxBookWeek",MinHrsRest,"agentsetMinHrsRest","Y","agentsetAllowsConsecutive",EventRate,"agentsetEventrate",MinSkillID,"agentsetMinSkillid",MaxSkillID,"agentsetMaxSkillid")
        let ValueDict=NSDictionary(objectsAndKeys: NSUserDefaults.standardUserDefaults().valueForKey("Status") as! String,"agentActive",AgentTypeArray,"AgentEventType",AgentSettingDict,"AgentSettings")
        let ParamsDict=NSDictionary(objectsAndKeys: IdentifierDict,"Identifier",ValueDict,"Value")
        let RequestDict=NSDictionary(objectsAndKeys: "setAgentSettings","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Agent",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse3(response.3, Dict: response.2 as? NSDictionary)
        }
    }
    
    func OpenPicker()
    {
        if detectPicker==1
        {
            MaxButton.setTitle(PickerValue.objectAtIndex(0) as? String, forState: UIControlState.Normal)
            MaxSkillID=0
        }
        else if detectPicker==2
        {
            MinButton.setTitle(PickerValue.objectAtIndex(0) as? String, forState: UIControlState.Normal)
            MinSkillID=0
        }
        else if detectPicker==3
        {
            RateButton.setTitle(NSString(format: "$%@.0",PickerValue.objectAtIndex(0) as! String) as String, forState: UIControlState.Normal)
            EventRate=PickerValue.objectAtIndex(0).integerValue
        }
        else if detectPicker==4
        {
            MGPWButton.setTitle(PickerValue.objectAtIndex(0) as? String, forState: UIControlState.Normal)
            MaxBookWeek=PickerValue.objectAtIndex(0).integerValue
        }
        else if detectPicker==5
        {
            MGPDButton.setTitle(PickerValue.objectAtIndex(0) as? String, forState: UIControlState.Normal)
            MaxBookDay=PickerValue.objectAtIndex(0).integerValue
        }
        else if detectPicker==6
        {
            MHBGButton.setTitle(PickerValue.objectAtIndex(0) as? String, forState: UIControlState.Normal)
            MinHrsRest=PickerValue.objectAtIndex(0).integerValue
        }
        pickerView.hidden=false
        pickerToolbar.hidden=false
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: false)
        //self.pickerView(self.myPickerView, didSelectRow: 0, inComponent: 0)
    }
    
    func donePicker()
    {
        pickerView.hidden=true
        pickerToolbar.hidden=true
    }
    
    /******************************************************
    MARK:PickerView Delegate Method
    ******************************************************/
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return PickerValue.count
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if detectPicker==1
        {
            MaxButton.setTitle(PickerValue.objectAtIndex(row) as? String, forState: UIControlState.Normal)
            MaxSkillID=row
        }
        else if detectPicker==2
        {
            MinButton.setTitle(PickerValue.objectAtIndex(row) as? String, forState: UIControlState.Normal)
            MinSkillID=row
        }
        else if detectPicker==3
        {
            RateButton.setTitle(NSString(format: "$%@.0",PickerValue.objectAtIndex(row) as! String) as String, forState: UIControlState.Normal)
            EventRate=PickerValue.objectAtIndex(row).integerValue
        }
        else if detectPicker==4
        {
            MGPWButton.setTitle(PickerValue.objectAtIndex(row) as? String, forState: UIControlState.Normal)
            MaxBookWeek=PickerValue.objectAtIndex(row).integerValue
        }
        else if detectPicker==5
        {
            MGPDButton.setTitle(PickerValue.objectAtIndex(row) as? String, forState: UIControlState.Normal)
            MaxBookDay=PickerValue.objectAtIndex(row).integerValue
        }
        else if detectPicker==6
        {
            MHBGButton.setTitle(PickerValue.objectAtIndex(row) as? String, forState: UIControlState.Normal)
            MinHrsRest=PickerValue.objectAtIndex(row).integerValue
        }
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return PickerValue.objectAtIndex(row) as! String
    }
//    func pickerView(pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
//        let title=PickerValue.objectAtIndex(row) as! String
//        let attString=NSAttributedString(string: title, attributes:[NSForegroundColorAttributeName:UIColor.whiteColor()])
//        return attString
//    }
}
