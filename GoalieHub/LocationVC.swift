//
//  LocationVC.swift
//  GoalieHub
//
//  Created by Vivek on 02/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class LocationVC: UIViewController,MBProgressHUDDelegate {
    var registerUDIDConnectionLocation=NSURLConnection()
    var registerUDIDDataLocation : NSMutableData!
    var registerUDIDConnectionRemoveLocation=NSURLConnection()
    var registerUDIDDataRemoveLocation : NSMutableData!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!,selectedIndex:Int!
    @IBOutlet var BackScroll: UIScrollView!
    @IBOutlet var BackView: UIView!
    @IBOutlet var RemoveBTN: UIButton!
    @IBOutlet var MapBTN: UIButton!
    @IBOutlet var PopupView: UIView!
    var LocationAry=NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()

        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        RemoveBTN.clipsToBounds=true
        RemoveBTN.layer.cornerRadius=5.0
        RemoveBTN.layer.borderWidth=1.0
        RemoveBTN.layer.borderColor=UIColor.whiteColor().CGColor
        
        MapBTN.clipsToBounds=true
        MapBTN.layer.cornerRadius=5.0
        MapBTN.layer.borderWidth=1.0
        MapBTN.layer.borderColor=UIColor.whiteColor().CGColor
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "LocationLoad:", name:"Location", object: nil)
        
        let gesture = UITapGestureRecognizer(target: self, action: "someAction:")
        BackView.addGestureRecognizer(gesture)
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func someAction(sender:UITapGestureRecognizer){
        BackView.hidden=true
        PopupView.hidden=true
    }
    
    func LocationLoad(notification: NSNotification){
        CallAPI()
    }
    func CallAPI()
    {
        HUD.show(true)
        let ParamsDict=NSDictionary(objectsAndKeys: AgentID,"agentId",LocationID,"locId")
        let RequestDict=NSDictionary(objectsAndKeys: "getMyAgentLocations","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Agent",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse1(response.3, Dict: response.2 as? NSDictionary)
        }
    }
    @IBAction func RemoveBTNClick(sender: UIButton) {
        
        BackView.hidden=true
        PopupView.hidden=true
        
        HUD.show(true)
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        let IdAry=NSMutableArray(object: LocationAry.objectAtIndex(selectedIndex).valueForKey("Location")!.valueForKey("locId")!)
        let ValueDict=NSDictionary(objectsAndKeys: IdAry,"AgentLocation")
        let IdentDict=NSDictionary(objectsAndKeys: 102,"agentId")
        let ParamsDict=NSDictionary(objectsAndKeys: IdentDict,"Identifier",ValueDict,"Value")
        let RequestDict=NSDictionary(objectsAndKeys: "removeAgentLocations","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Agent",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse2(response.3, Dict: response.2 as? NSDictionary)
        }
        
    }
    @IBAction func MapBTNClick(sender: UIButton) {
        
        var AddressDict=LocationAry.objectAtIndex(selectedIndex).valueForKey("Location")!.valueForKey("Address") as! NSDictionary
        var AddressStr=NSString(format: "http://maps.google.com/maps?q=%@,%@,%@", AddressDict.valueForKey("addrStreetaddress") as! String,AddressDict.valueForKey("addrCity") as! String,AddressDict.valueForKey("addrCountry") as! String)
        UIApplication.sharedApplication().openURL(NSURL(string: AddressStr.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!)!)
        
    }
    
    func HandleAPIResponse1(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            if let CheckResu = Dict.valueForKey("response")!.valueForKey("result") as? NSNull
            {
                
            }
            else
            {
                NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
                NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
                BackScroll.subviews.map({ $0.removeFromSuperview() })
                LocationAry=NSMutableArray(array: Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AgentLocation") as! NSArray)
                var Height:CGFloat=10.0
                for var i=0;i<LocationAry.count;i++
                {
                    let BackView=UIView(frame: CGRectMake(10, Height, BackScroll.frame.size.width-20, 50))
                    //BackView.backgroundColor=UIColor.whiteColor()
                    let LineIMG=UIImageView(frame: CGRectMake(0, 0, 10, 50))
                    LineIMG.backgroundColor=UIColor.whiteColor()
                    BackView.addSubview(LineIMG)
                    
                    print(LocationAry.objectAtIndex(i).valueForKey("agentlocDist"))
                    let tempString = LocationAry.objectAtIndex(i).valueForKey("agentlocDist") as! NSNumber
                    let DistanceLBL=UILabel(frame: CGRectMake(BackView.frame.size.width-20, 5, 200, 20))
                    DistanceLBL.numberOfLines=0
                    DistanceLBL.text="(\(tempString))"
                    DistanceLBL.sizeToFit()
                    DistanceLBL.frame=CGRectMake(BackView.frame.size.width-DistanceLBL.frame.size.width-20, 5, DistanceLBL.frame.size.width, 20)
                    DistanceLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                    DistanceLBL.font=UIFont.boldSystemFontOfSize(13)
                    DistanceLBL.textAlignment=NSTextAlignment.Right
                    //                DistanceLBL.layer.borderWidth=1.0
                    //                DistanceLBL.layer.borderColor=UIColor.whiteColor().CGColor
                    //DistanceLBL.backgroundColor=UIColor.whiteColor()
                    BackView.addSubview(DistanceLBL)
                    
                    let TitleLBL=UILabel(frame: CGRectMake(33, 5, BackView.frame.size.width-DistanceLBL.frame.size.width-40, 20))
                    TitleLBL.text=LocationAry.objectAtIndex(i).valueForKey("Location")!.valueForKey("locName") as? String
                    TitleLBL.textColor=UIColor.lightGrayColor()
                    TitleLBL.font=UIFont.systemFontOfSize(13)
                    TitleLBL.numberOfLines=0
                    TitleLBL.sizeToFit()
                    BackView.addSubview(TitleLBL)
                    
                    let addressLBL=UILabel(frame: CGRectMake(33, TitleLBL.frame.size.height+TitleLBL.frame.origin.y+5, BackView.frame.size.width-40, 20))
                    addressLBL.text=NSString(format: "%@,%@,%@", LocationAry.objectAtIndex(i).valueForKey("Location")!.valueForKey("Address")!.valueForKey("addrStreetaddress") as! String,LocationAry.objectAtIndex(i).valueForKey("Location")!.valueForKey("Address")!.valueForKey("addrCity") as! String,LocationAry.objectAtIndex(i).valueForKey("Location")!.valueForKey("Address")!.valueForKey("addrCountry") as! String) as String
                    addressLBL.textColor=UIColor.lightGrayColor()
                    addressLBL.font=UIFont.systemFontOfSize(13)
                    addressLBL.numberOfLines=0
                    addressLBL.sizeToFit()
                    BackView.addSubview(addressLBL)
                    
                    BackView.frame=CGRectMake(10, Height, BackScroll.frame.size.width-20, addressLBL.frame.size.height+addressLBL.frame.origin.y+10)
                    LineIMG.frame=CGRectMake(0, 0, 10, BackView.frame.size.height)
                    
                    let LocationIcon=UIImageView(frame: CGRectMake(LineIMG.frame.size.width+LineIMG.frame.origin.x+5, (BackView.frame.size.height/2)-10, 13, 20))
                    LocationIcon.image=UIImage(named: "ic_home_location_small.png")
                    BackView.addSubview(LocationIcon)
                    
                    let SmallLineIMG=UIImageView(frame: CGRectMake(0, BackView.frame.size.height-1, BackView.frame.size.width, 1))
                    SmallLineIMG.backgroundColor=UIColor.whiteColor()
                    BackView.addSubview(SmallLineIMG)
                    
                    let LastIMG=UIImageView(frame: CGRectMake(BackView.frame.size.width-20, 5, 20, 20))
                    LastIMG.image=UIImage(named: "ic_general_more_details.png")
                    BackView.addSubview(LastIMG)
                    
                    BackScroll.addSubview(BackView)
                    
                    let BackBTN=UIButton(frame: CGRectMake(BackView.frame.origin.x, Height, BackView.frame.size.width, BackView.frame.size.height))
                    BackBTN.addTarget(self, action: "BackBTNClick:", forControlEvents: UIControlEvents.TouchUpInside)
                    BackBTN.tag=i
                    BackScroll.addSubview(BackBTN)
                    
                    Height = Height + BackView.frame.size.height + 10
                    
                }
                BackScroll.contentSize=CGSizeMake(0, Height+10)
            }
            
            HUD.hide(true)
        }
    }
    
    func HandleAPIResponse2(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            CallAPI()
        }
    }
    func BackBTNClick(sender:UIButton!)
    {
        selectedIndex=sender.tag
        BackView.hidden=false
        PopupView.hidden=false
    }
    @IBAction func LocationBTN(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("AddLocationVC") as! AddLocationVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    
    override func didMoveToParentViewController(parent: UIViewController?) {
    
    }
}
