//
//  HomeTabbarVC.swift
//  GoalieHub
//
//  Created by Vivek on 02/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit

class HomeTabbarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.barTintColor = UIColor.blackColor()
        var tabBarItem1 = self.tabBar.items![0] as! UITabBarItem
        var tabBarItem2 = self.tabBar.items![1] as! UITabBarItem
        var tabBarItem3 = self.tabBar.items![2] as! UITabBarItem
        var tabBarItem4 = self.tabBar.items![3] as! UITabBarItem
        var tabBarItem5 = self.tabBar.items![4] as! UITabBarItem
        
        tabBarItem1.image = UIImage(named: "ic_tab_bar_search.png")!.imageWithRenderingMode(.AlwaysOriginal)
        tabBarItem1.selectedImage=UIImage(named: "ic_tab_bar_search.png")!.imageWithRenderingMode(.AlwaysOriginal)
        
        tabBarItem2.image = UIImage(named: "ic_tab_bar_notification.png")!.imageWithRenderingMode(.AlwaysOriginal)
        tabBarItem2.selectedImage=UIImage(named: "ic_tab_bar_notification.png")!.imageWithRenderingMode(.AlwaysOriginal)
        
        tabBarItem3.image = UIImage(named: "ic_tab_bar_home_new.png")!.imageWithRenderingMode(.AlwaysOriginal)
        tabBarItem3.selectedImage=UIImage(named: "ic_tab_bar_home_new.png")!.imageWithRenderingMode(.AlwaysOriginal)
        
        tabBarItem4.image = UIImage(named: "ic_tab_bar_team.png")!.imageWithRenderingMode(.AlwaysOriginal)
        tabBarItem4.selectedImage=UIImage(named: "ic_tab_bar_team.png")!.imageWithRenderingMode(.AlwaysOriginal)
        
        tabBarItem5.image = UIImage(named: "ic_tab_bar_calendar.png")!.imageWithRenderingMode(.AlwaysOriginal)
        tabBarItem5.selectedImage=UIImage(named: "ic_tab_bar_calendar.png")!.imageWithRenderingMode(.AlwaysOriginal)
        
        tabBarItem3.enabled=false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        //self.tabBarController!.selectedIndex=2
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
