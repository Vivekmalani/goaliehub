//
//  TeamVC.swift
//  GoalieHub
//
//  Created by Vivek on 08/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class TeamVC: UIViewController,MBProgressHUDDelegate {
    @IBOutlet var BarBTN: UIBarButtonItem!
    @IBOutlet var BackScroll: UIScrollView!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!
    var TeamAry = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func RequestBTNClick(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("TeamRequestVC") as! TeamRequestVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    @IBAction func CreateTeamBTNClick(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("CreateTeamVC") as! CreateTeamVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    
    func LoadScrollView()
    {
        for view in BackScroll.subviews {
            view.removeFromSuperview()
        }
        var Height:CGFloat=10.0
        for var i=0;i<TeamAry.count;i++
        {
            let BackView=UIView(frame: CGRectMake(10, Height, BackScroll.frame.size.width-20, 50))
            //BackView.backgroundColor=UIColor.whiteColor()
            BackView.tag=i+100
            let LineIMG=UIImageView(frame: CGRectMake(0, 0, 10, 50))
            LineIMG.backgroundColor=UIColor.whiteColor()
            BackView.addSubview(LineIMG)
            //print(TeamAry.objectAtIndex(i).valueForKey("Team")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro"))
            let decodedData = NSData(base64EncodedString: TeamAry.objectAtIndex(i).valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            let TeamIMG=UIImageView(frame: CGRectMake(15, 5, 40, 40))
            TeamIMG.image=UIImage(data: decodedData!)
            BackView.addSubview(TeamIMG)
            
            
            let TitleLBL=UILabel(frame: CGRectMake(TeamIMG.frame.size.width+TeamIMG.frame.origin.x+5, 0, BackView.frame.size.width-60, 50))
            TitleLBL.text=TeamAry.objectAtIndex(i).valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
            TitleLBL.textColor=UIColor.whiteColor()
            TitleLBL.font=UIFont.systemFontOfSize(13)
            TitleLBL.numberOfLines=2
            BackView.addSubview(TitleLBL)
            
            
            let SmallLineIMG=UIImageView(frame: CGRectMake(0, BackView.frame.size.height-1, BackView.frame.size.width, 1))
            SmallLineIMG.backgroundColor=UIColor.whiteColor()
            BackView.addSubview(SmallLineIMG)

            BackScroll.addSubview(BackView)
            
            let BackBTN=UIButton(frame: CGRectMake(BackView.frame.origin.x, Height, BackView.frame.size.width, BackView.frame.size.height))
            BackBTN.addTarget(self, action: "BackBTNClick:", forControlEvents: UIControlEvents.TouchUpInside)
            BackBTN.tag=i
            BackScroll.addSubview(BackBTN)
            
            Height = Height + BackView.frame.size.height + 10
            
        }
        BackScroll.contentSize=CGSizeMake(0, Height+10)
    }
    
    func BackBTNClick(sender:UIButton!)
    {
        let UserID=NSUserDefaults.standardUserDefaults().valueForKey("UserID")!.integerValue
        if TeamAry.objectAtIndex(sender.tag).valueForKey("teamuserAddedby")!.integerValue == UserID
        {
            var storyboard = UIStoryboard(name: "Main", bundle: nil)
            var initialViewController = storyboard.instantiateViewControllerWithIdentifier("TeamOptionVC") as! TeamOptionVC
            self.navigationController?.pushViewController(initialViewController, animated: true)
            initialViewController.TeamDetailDict=TeamAry.objectAtIndex(sender.tag) as! NSMutableDictionary
        }
        else
        {
            UIAlertView(title: nil,message:"The User does not have permission to perform action on this Team",delegate: self,cancelButtonTitle:"OK").show()
        }
    
    }

    func HandleAPIResponse1(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
            
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            
            if let checkDict=Dict.valueForKey("response")!.valueForKey("result") as? NSDictionary
            {
                TeamAry.addObject(checkDict)
            }
            else if let checkAry=Dict.valueForKey("response")!.valueForKey("result") as? NSArray
            {
                TeamAry=NSMutableArray(array: checkAry)
            }
            LoadScrollView()
        }
        HUD.hide(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        TeamAry.removeAllObjects()
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
        
        HUD.show(true)
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        
        let ValueDict=NSDictionary(objectsAndKeys: "active","active")
        let ParamsDict=NSDictionary(objectsAndKeys: ValueDict,"Value")
        let RequestDict=NSDictionary(objectsAndKeys: "getMyTeams","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Team",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse1(response.3, Dict: response.2 as? NSDictionary)
        }
        
    }

}
