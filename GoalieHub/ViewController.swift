//
//  ViewController.swift
//  GoalieHub
//
//  Created by Vivek on 02/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
struct MoveKeyboard {
    static let KEYBOARD_ANIMATION_DURATION : CGFloat = 0.3
    static let MINIMUM_SCROLL_FRACTION : CGFloat = 0.2;
    static let MAXIMUM_SCROLL_FRACTION : CGFloat = 0.8;
    static let PORTRAIT_KEYBOARD_HEIGHT : CGFloat = 216;
    static let LANDSCAPE_KEYBOARD_HEIGHT : CGFloat = 162;
}
var API_STR="http://www.mygamesaver.com/mgsapid/sessionRequest.php"
class ViewController: UIViewController,MBProgressHUDDelegate {
    var animateDistance: CGFloat = 0.0
    var HUD:MBProgressHUD!
    var registerUDIDConnectionLogin=NSURLConnection()
    var registerUDIDDataLogin : NSMutableData!
    
    @IBOutlet var LoginBackView: UIImageView!
    @IBOutlet var TXTPassword: UITextField!
    @IBOutlet var TXTUserName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        //HUD.labelText="Loading..."
        
        let borderUserName = CALayer()
        let widthUserName = CGFloat(1.0)
        borderUserName.borderColor = UIColor.whiteColor().CGColor
        borderUserName.frame = CGRect(x: 0, y: TXTUserName.frame.size.height - widthUserName, width:  TXTUserName.frame.size.width, height: TXTUserName.frame.size.height)
        borderUserName.borderWidth = widthUserName
        TXTUserName.layer.addSublayer(borderUserName)
        TXTUserName.layer.masksToBounds = true
        TXTUserName.attributedPlaceholder = NSAttributedString(string:"Username or Email",attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        let borderPass = CALayer()
        let widthPass = CGFloat(1.0)
        borderPass.borderColor = UIColor.whiteColor().CGColor
        borderPass.frame = CGRect(x: 0, y: TXTPassword.frame.size.height - widthPass, width:  TXTPassword.frame.size.width, height: TXTPassword.frame.size.height)
        borderPass.borderWidth = widthPass
        TXTPassword.layer.addSublayer(borderPass)
        TXTPassword.layer.masksToBounds = true
        TXTPassword.attributedPlaceholder = NSAttributedString(string:"Password",attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        
        LoginBackView.layer.borderWidth=1.0
        LoginBackView.layer.borderColor=UIColor.whiteColor().CGColor
        LoginBackView.clipsToBounds=true
        LoginBackView.layer.cornerRadius=5.0
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func LoginBTNClick(sender:UIButton) {
        
        if TXTUserName.text.isEmpty == true
        {
            let  Warningalert = UIAlertView(title: nil,message:"Please enter your username or emailID",delegate: self,cancelButtonTitle:"OK")
            Warningalert.show()
        }
        else if TXTPassword.text.isEmpty == true
        {
            let  Warningalert = UIAlertView(title: nil,message:"Please enter your password",delegate: self,cancelButtonTitle:"OK")
            Warningalert.show()
        }
        else
        {
            TXTPassword.resignFirstResponder()
            TXTUserName.resignFirstResponder()
            
            HUD.show(true)
            let ParamsDict=NSDictionary(objectsAndKeys: TXTUserName.text,"user",TXTPassword.text,"password","W","sessionType")
            let RequestDict=NSDictionary(objectsAndKeys: "login","method","1","id",ParamsDict,"params")
            print(RequestDict)
            let param = [
                "id"    : "null",
                "key"    : "null",
                "callBack"    : "processLogin",
                "serviceName"    : "Session",
                "request" : RequestDict
            ]
            println(param)
            Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
                .responseJSON {response in
                    print(response.2)
                    self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
            }
        }
        
    }
    
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            if let FailedStr = Dict!.valueForKey("id") as? String
            {
                let  WarningAlert = UIAlertView(title: nil,message:"Please enter valid email or password",delegate: self,cancelButtonTitle:"OK")
                WarningAlert.show()
            }
            else
            {
                NSUserDefaults.standardUserDefaults().setValue(Dict!.valueForKey("id"), forKey: "ID")
                NSUserDefaults.standardUserDefaults().setValue(Dict!.valueForKey("key"), forKey: "KEY")
                NSUserDefaults.standardUserDefaults().setValue(Dict!.valueForKey("response")!.valueForKey("result")!.valueForKey("userId"), forKey: "UserID")
                var storyboard = UIStoryboard(name: "Main", bundle: nil)
                var initialViewController = storyboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
                self.presentViewController(initialViewController, animated: true, completion: nil)
            }
            HUD.hide(true)
        }
    }
    
    /******************************************************
        MARK:TextField Delegate Method
    ******************************************************/
    func textFieldDidBeginEditing(textField: UITextField) {
//        let textFieldRect : CGRect = self.view.window!.convertRect(textField.bounds, fromView: textField)
//        let viewRect : CGRect = self.view.window!.convertRect(self.view.bounds, fromView: self.view)
//        
//        let midline : CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
//        let numerator : CGFloat = midline - viewRect.origin.y - MoveKeyboard.MINIMUM_SCROLL_FRACTION * viewRect.size.height
//        let denominator : CGFloat = (MoveKeyboard.MAXIMUM_SCROLL_FRACTION - MoveKeyboard.MINIMUM_SCROLL_FRACTION) * viewRect.size.height
//        var heightFraction : CGFloat = numerator / denominator
//        
//        if heightFraction < 0.0 {
//            heightFraction = 0.0
//        } else if heightFraction > 1.0 {
//            heightFraction = 1.0
//        }
//        
//        let orientation : UIInterfaceOrientation = UIApplication.sharedApplication().statusBarOrientation
//        if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown) {
//            animateDistance = floor(MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
//        } else {
//            animateDistance = floor(MoveKeyboard.LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
//        }
//        
//        var viewFrame : CGRect = self.view.frame
//        viewFrame.origin.y -= animateDistance
//        
//        UIView.beginAnimations(nil, context: nil)
//        UIView.setAnimationBeginsFromCurrentState(true)
//        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
//        
//        self.view.frame = viewFrame
//        
//        UIView.commitAnimations()
        
    }
    func textFieldDidEndEditing(textField: UITextField) {
//        var viewFrame : CGRect = self.view.frame
//        viewFrame.origin.y += animateDistance
//        
//        UIView.beginAnimations(nil, context: nil)
//        UIView.setAnimationBeginsFromCurrentState(true)
//        
//        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
//        
//        self.view.frame = viewFrame
//        
//        UIView.commitAnimations()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        var nextTag: Int = textField.tag + 1
        let nextResponder = textField.superview?.viewWithTag(nextTag) as UIResponder!
        if (nextResponder != nil)  {
            nextResponder.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }
        return true
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden=true
    }
}

