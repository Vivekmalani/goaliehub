//
//  BlockVC.swift
//  GoalieHub
//
//  Created by Vivek on 15/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class BlockVC: UIViewController,MBProgressHUDDelegate {
    var HUD:MBProgressHUD!
    var ID:Int!,KEY:Int!
    let cellIdentifier = "cellIdentifier"
    @IBOutlet var BlockTBL: UITableView!
    var BlockAry=NSMutableArray()
    
    @IBOutlet var BackView: UIView!
    @IBOutlet var PopupView: UIView!
    @IBOutlet var RemoveBTN: UIButton!
    @IBOutlet var ProfileBTN: UIButton!
    var Index:Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        self.BlockTBL?.registerClass(UITableViewCell.self, forCellReuseIdentifier: self.cellIdentifier)
        BlockTBL.tableFooterView = UIView(frame: CGRectZero)
        BlockTBL.separatorStyle=UITableViewCellSeparatorStyle.None
        
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as Int
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as Int
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "BlockLoad:", name:"Block", object: nil)
        
        RemoveBTN.clipsToBounds=true
        RemoveBTN.layer.cornerRadius=5.0
        RemoveBTN.layer.borderWidth=1.0
        RemoveBTN.layer.borderColor=UIColor.whiteColor().CGColor
        
        ProfileBTN.clipsToBounds=true
        ProfileBTN.layer.cornerRadius=5.0
        ProfileBTN.layer.borderWidth=1.0
        ProfileBTN.layer.borderColor=UIColor.whiteColor().CGColor
        
        let gesture = UITapGestureRecognizer(target: self, action: "PopupAction:")
        BackView.addGestureRecognizer(gesture)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func RemoveBTNClick(sender: UIButton) {
        BackView.hidden=true
        PopupView.hidden=true
        
        HUD.show(true)
        let ValueDict=NSDictionary(objectsAndKeys: BlockAry.objectAtIndex(Index).valueForKey("vUserSmall")!.valueForKey("userId") as! Int,"blockedId")
        let ParamsDict=NSDictionary(objectsAndKeys:ValueDict,"Value")
        let RequestDict=NSDictionary(objectsAndKeys: "removeMyBlockedUser","method","1","id",ParamsDict,"params")
        //print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "User",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse2(response.3, Dict: response.2 as? NSDictionary)
        }
    }
    @IBAction func ProfileBTNClick(sender: UIButton) {
        BackView.hidden=true
        PopupView.hidden=true
    }
    
    func PopupAction(sender:UITapGestureRecognizer){
        BackView.hidden=true
        PopupView.hidden=true
    }
    
    func BlockLoad(notification: NSNotification){
        HUD.show(true)
        CallAPI()
    }
    func HandleAPIResponse1(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            BlockAry=Dict.valueForKey("response")!.valueForKey("result") as! NSMutableArray
            BlockTBL.reloadData()
            HUD.hide(true)
        }
    }
    func HandleAPIResponse2(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            CallAPI()
        }
        
    }
    
    func CallAPI()
    {
        let ParamsDict=NSDictionary()
        let RequestDict=NSDictionary(objectsAndKeys: "getMyBlockedUsers","method","1","id",ParamsDict,"params")
        //print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "User",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse1(response.3, Dict: response.2 as? NSDictionary)
        }
    }
    
    /********************************************
    MARK:TableView Delegate
    ********************************************/
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return BlockAry.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as! UITableViewCell
        cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: self.cellIdentifier)
        cell.frame=CGRectMake(0, 0, self.view.frame.size.width, 40)
        
        let SubBackView=UIView(frame: CGRectMake(10, 10, cell.frame.size.width-20, 60))
        //BackView.backgroundColor=UIColor.whiteColor()
        let LineIMG=UIImageView(frame: CGRectMake(0, 0, 10, 60))
        LineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(LineIMG)
        let decodedData = NSData(base64EncodedString: BlockAry.objectAtIndex(indexPath.row).valueForKey("vUserSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
        let TitleIcon=UIImageView(frame: CGRectMake(LineIMG.frame.size.width+LineIMG.frame.origin.x+5, 8, 40, 40))
        TitleIcon.image=UIImage(data: decodedData!)
        SubBackView.addSubview(TitleIcon)
        
        let TItleLBL=UILabel(frame: CGRectMake(TitleIcon.frame.size.width+TitleIcon.frame.origin.x+5, 8, SubBackView.frame.size.width-TitleIcon.frame.origin.x-30, 40))
        TItleLBL.text=BlockAry.objectAtIndex(indexPath.row).valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
        TItleLBL.font=UIFont.boldSystemFontOfSize(12)
        TItleLBL.textColor=UIColor.whiteColor()
        SubBackView.addSubview(TItleLBL)
        
        let SmallLineIMG=UIImageView(frame: CGRectMake(0, SubBackView.frame.size.height-1, SubBackView.frame.size.width, 1))
        SmallLineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(SmallLineIMG)
        
        let LastIMG=UIImageView(frame: CGRectMake(SubBackView.frame.size.width-20, 8, 20, 20))
        LastIMG.image=UIImage(named: "ic_general_more_details.png")
        SubBackView.addSubview(LastIMG)
        cell.addSubview(SubBackView)
        cell.backgroundColor=UIColor.blackColor()
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        BlockTBL.rowHeight = SubBackView.frame.size.height+20
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        Index=indexPath.row
        PopupView.hidden=false
        BackView.hidden=false
        
//        var storyboard = UIStoryboard(name: "Main", bundle: nil)
//        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("SWRevealViewController") as! SWRevealViewController
//        self.presentViewController(initialViewController, animated: true, completion: nil)
//        NSUserDefaults.standardUserDefaults().setValue("1", forKey: "CheckNav")
    }
}
