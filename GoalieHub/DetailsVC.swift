//
//  DetailsVC.swift
//  GoalieHub
//
//  Created by Vivek on 14/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class DetailsVC: UIViewController,MBProgressHUDDelegate {

    @IBOutlet var PopupView: UIView!
    @IBOutlet var BackView: UIView!
    @IBOutlet var AttandanceLBL: UILabel!
    @IBOutlet var AddressLBL: UILabel!
    @IBOutlet var EventNoteLBL: UILabel!
    @IBOutlet var EventNameLBL: UILabel!
    @IBOutlet var LegueLBL: UILabel!
    @IBOutlet var TypeLBL: UILabel!
    @IBOutlet var SexLBL: UILabel!
    @IBOutlet var SkillLBL: UILabel!
    @IBOutlet var AgeLBL: UILabel!
    @IBOutlet var FourView: UIView!
    @IBOutlet var ThirdView: UIView!
    @IBOutlet var SecondView: UIView!
    @IBOutlet var FirstView: UIView!
    @IBOutlet var BackScroll: UIScrollView!
    @IBOutlet var LastView: UIView!
    @IBOutlet var PopBTN: UIButton!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!,EventID:Int!
    override func viewDidLoad() {
        super.viewDidLoad()

        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        
        BackScroll.contentSize=CGSizeMake(0, LastView.frame.size.height+LastView.frame.origin.y+10)
        
        FirstView.clipsToBounds=true
        FirstView.layer.cornerRadius=5.0
        FirstView.layer.borderWidth=1.0
        FirstView.layer.borderColor=UIColor.whiteColor().CGColor
        
        SecondView.clipsToBounds=true
        SecondView.layer.cornerRadius=5.0
        SecondView.layer.borderWidth=1.0
        SecondView.layer.borderColor=UIColor.whiteColor().CGColor
        
        ThirdView.clipsToBounds=true
        ThirdView.layer.cornerRadius=5.0
        ThirdView.layer.borderWidth=1.0
        ThirdView.layer.borderColor=UIColor.whiteColor().CGColor
        
        FourView.clipsToBounds=true
        FourView.layer.cornerRadius=5.0
        FourView.layer.borderWidth=1.0
        FourView.layer.borderColor=UIColor.whiteColor().CGColor
        
        LastView.clipsToBounds=true
        LastView.layer.cornerRadius=5.0
        LastView.layer.borderWidth=1.0
        LastView.layer.borderColor=UIColor.whiteColor().CGColor
        
        
        PopBTN.clipsToBounds=true
        PopBTN.layer.cornerRadius=5.0
        PopBTN.layer.borderWidth=1.0
        PopBTN.layer.borderColor=UIColor.whiteColor().CGColor
        
        EventID = NSUserDefaults.standardUserDefaults().valueForKey("EventID") as! Int
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "EventDetailsLoad:", name:"EventDetails", object: nil)
        let gesture = UITapGestureRecognizer(target: self, action: "PopupAction:")
        BackView.addGestureRecognizer(gesture)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func PopupAction(sender:UITapGestureRecognizer){
        BackView.hidden=true
        PopupView.hidden=true
    }
    
    @IBAction func PopBTNClick(sender: UIButton) {
    }
    
    @IBAction func MapBTNClick(sender: UIButton) {
        PopBTN.setTitle("Open Map", forState: UIControlState.Normal)
        BackView.hidden=false
        PopupView.hidden=false
        
    }
    @IBAction func AttandanceBTNClick(sender: UIButton) {
        PopBTN.setTitle("Change Attendance", forState: UIControlState.Normal)
        BackView.hidden=false
        PopupView.hidden=false
        
    }
    func EventDetailsLoad(notification: NSNotification){
        HUD.show(true)
        let IdentiDict=NSDictionary(objectsAndKeys: EventID,"eventId")
        let ParamsDict=NSDictionary(objectsAndKeys:IdentiDict,"Identifier")
        let RequestDict=NSDictionary(objectsAndKeys: "getEventDetails","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Event",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse1(response.3, Dict: response.2 as? NSDictionary)
        }
    }
    
    func HandleAPIResponse1(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            AgeLBL.text="Adults Only"
            let SkillID=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("Team")!.valueForKey("SportSkill")!.valueForKey("spskillId") as! Int
            if SkillID == 0
            {
                SkillLBL.text="Beginner"
            }
            else if SkillID == 1
            {
                SkillLBL.text="DIV D"
            }
            else if SkillID == 2
            {
                SkillLBL.text="DIV C/D"
            }
            else if SkillID == 3
            {
                SkillLBL.text="DIV C"
            }
            else if SkillID == 4
            {
                SkillLBL.text="DIV B/C"
            }
            else if SkillID == 5
            {
                SkillLBL.text="DIV B"
            }
            else if SkillID == 6
            {
                SkillLBL.text="DIV A/B"
            }
            else if SkillID == 7
            {
                SkillLBL.text="DIV A"
            }
            
            let TeamSex=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("Team")!.valueForKey("teamSex") as! NSString
            if TeamSex.isEqualToString("M")
            {
                SexLBL.text="Male"
            }
            else
            {
                SexLBL.text="Female"
            }
            
            TypeLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("TypeCode")!.valueForKey("typcodeName") as? String
            EventNameLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("eventName") as? String
            EventNoteLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("eventNotes") as? String
            
            AttandanceLBL.text = "I Will Attend"
            AddressLBL.text = Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("vLocationSmall")!.valueForKey("addrStreetaddress") as? String
            
        }
        let IdentiDict=NSDictionary(objectsAndKeys: EventID,"eventId",1,"sportposId")
        let ParamsDict=NSDictionary(objectsAndKeys:IdentiDict,"Identifier")
        let RequestDict=NSDictionary(objectsAndKeys: "getPostingDetails","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Posting",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse2(response.3, Dict: response.2 as? NSDictionary)
        }
    }
    
    func HandleAPIResponse2(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
        }
        HUD.hide(true)
    }
    
}
