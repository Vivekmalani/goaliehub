//
//  CreateTeamVC.swift
//  GoalieHub
//
//  Created by Vivek on 08/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class CreateTeamVC: UIViewController,MBProgressHUDDelegate,UIPickerViewDataSource,UIPickerViewDelegate {
    
    @IBOutlet var CreateLBL: UILabel!
    @IBOutlet var ProfileIMG: UIImageView!
    @IBOutlet var BackScroll: UIScrollView!
    @IBOutlet var CreateTeamBTN: UIButton!
    @IBOutlet var LocationBTN: UIButton!
    @IBOutlet var TeamBTN: UIButton!
    @IBOutlet var AgeBTN: UIButton!
    @IBOutlet var SexBTN: UIButton!
    @IBOutlet var SkillBTN: UIButton!
    @IBOutlet var TXTTeamname: UITextField!
    @IBOutlet var BarBTN: UIBarButtonItem!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!,detectPicker:Int!
    var CheckEditOption:Int=0
    var PickerValue=NSMutableArray()
    var pickerView:UIPickerView!
    var pickerToolbar:UIToolbar!
    var TeamDetailDict=NSMutableDictionary(),TeamDetailsVaDict=NSMutableDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        //HUD.show(true)
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        BackScroll.contentSize=CGSizeMake(0, CreateTeamBTN.frame.size.height+CreateTeamBTN.frame.origin.y+60)
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        print(TeamDetailDict)
        print(TeamDetailsVaDict)
        
        if CheckEditOption==1
        {
            CreateLBL.text="Edit Team Settings"
            let decodedData = NSData(base64EncodedString: TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            ProfileIMG.image=UIImage(data:decodedData!)
            TXTTeamname.text=TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
            CreateTeamBTN.setTitle("Update Team", forState: UIControlState.Normal)
        }
        
        NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "ImageStr")
        NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "MinAge")
        NSUserDefaults.standardUserDefaults().setValue(nil, forKey: "MaxAge")
        
        let borderTXT = CALayer()
        let widthTXT = CGFloat(1.0)
        borderTXT.borderColor = UIColor.whiteColor().CGColor
        borderTXT.frame = CGRect(x: 0, y: TXTTeamname.frame.size.height - widthTXT, width:  TXTTeamname.frame.size.width, height: TXTTeamname.frame.size.height)
        borderTXT.borderWidth = widthTXT
        TXTTeamname.layer.addSublayer(borderTXT)
        TXTTeamname.layer.masksToBounds = true
        TXTTeamname.attributedPlaceholder = NSAttributedString(string:"Team Name",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        
        pickerView=UIPickerView(frame: CGRectMake(0, self.view.frame.size.height-265, self.view.frame.size.width, 150))
        pickerView.dataSource=self
        pickerView.delegate=self
        pickerView.backgroundColor=UIColor.whiteColor()
        self.view.addSubview(pickerView)
        pickerToolbar=UIToolbar(frame: CGRectMake(0, pickerView.frame.origin.y-35, self.view.frame.size.width, 35))
        pickerToolbar.barStyle = UIBarStyle.Default
        pickerToolbar.translucent = true
        pickerToolbar.tintColor = UIColor.blackColor()
        pickerToolbar.barTintColor=UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
        //pickerToolbar.sizeToFit()
        
        var doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: "donePicker")
        var CButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        pickerToolbar.setItems([CButton,doneButton], animated: false)
        pickerToolbar.userInteractionEnabled = true
        self.view.addSubview(pickerToolbar)
        pickerView.hidden=true
        pickerToolbar.hidden=true
        
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func ProfileBTNClick(sender: UIButton) {
        
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("ProfilePictureVC") as! ProfilePictureVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
        
    }
    
    
    @IBAction func CreateTeamBTNClick(sender: UIButton) {
        
        if TXTTeamname.text.isEmpty == true
        {
            let  Warningalert = UIAlertView(title: nil,
                message:"Please enter team name",
                delegate: self,
                cancelButtonTitle:"OK")
            Warningalert.show()
        }
        else if SkillBTN.titleLabel!.text == "Skill Level"
        {
            let  Warningalert = UIAlertView(title: nil,
                message:"Please select team skill",
                delegate: self,
                cancelButtonTitle:"OK")
            Warningalert.show()
        }
        else if SexBTN.titleLabel!.text == "Sex"
        {
            let  Warningalert = UIAlertView(title: nil,
                message:"Please select team sex",
                delegate: self,
                cancelButtonTitle:"OK")
            Warningalert.show()
        }
        else if NSUserDefaults.standardUserDefaults().valueForKey("MinAge") == nil
        {
            let  Warningalert = UIAlertView(title: nil,
                message:"Please select team age",
                delegate: self,
                cancelButtonTitle:"OK")
            Warningalert.show()
        }
        else if TeamBTN.titleLabel!.text == "Team Type"
        {
            let  Warningalert = UIAlertView(title: nil,
                message:"Please select team type",
                delegate: self,
                cancelButtonTitle:"OK")
            Warningalert.show()
        }
        else
        {
            let ImageStr:String!
            if NSUserDefaults.standardUserDefaults().valueForKey("ImageStr") != nil
            {
                ImageStr=NSUserDefaults.standardUserDefaults().valueForKey("ImageStr") as! String
            }
            else
            {
                ImageStr=""
            }
            let MinAge=NSUserDefaults.standardUserDefaults().valueForKey("MinAge") as! Int
            let MaxAge=NSUserDefaults.standardUserDefaults().valueForKey("MaxAge") as! Int
            let ValueDict=NSDictionary(objectsAndKeys: TXTTeamname.text,"teamName",Gender(SexBTN.titleLabel!.text),"teamSex","A","teamActive",Skill(SkillBTN.titleLabel!.text),"spskillId",MinAge,"teamAgeMin",MaxAge,"teamAgeMax",ImageStr,"profileImg")
            let ParamsDict=NSDictionary(objectsAndKeys: ValueDict,"Value")
            let RequestDict=NSDictionary(objectsAndKeys: "createTeam","method","1","id",ParamsDict,"params")
            print(RequestDict)
            let param = [
                "id"    : ID,
                "key"    : KEY,
                "callBack"    : "myCallBackMethod",
                "serviceName"    : "Team",
                "request" : RequestDict
            ]
            println(param)
            Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
                .responseJSON {response in
                    self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
            }
        }
        
    }
    
    @IBAction func LocationBTNClick(sender: UIButton) {
    }
    @IBAction func TeamBTNClick(sender: UIButton) {
        PickerValue.removeAllObjects()
        PickerValue=NSMutableArray(objects: "Shinny Unorganized","Shinny Organized","Pickup Hockey","League","Tournament")
        detectPicker=3
        OpenPicker()
    }
    @IBAction func AgeBTNClick(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("TeamAgeVC") as! TeamAgeVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    @IBAction func SexBTNClick(sender: UIButton) {
        PickerValue.removeAllObjects()
        PickerValue=NSMutableArray(objects: "Male Only","Female Only","Co-ed")
        detectPicker=2
        OpenPicker()
    }
    @IBAction func SkillBTNClick(sender: UIButton) {
        PickerValue.removeAllObjects()
        PickerValue=NSMutableArray(objects: "Beginner","DIV D","DIV C/D","DIV C","DIV B/C","DIV B","DIV A/B","DIV A")
        detectPicker=1
        OpenPicker()
    }

    func OpenPicker()
    {
        if detectPicker==1
        {
            SkillBTN.setTitle(PickerValue.objectAtIndex(0) as? String, forState: UIControlState.Normal)
        }
        else if detectPicker==2
        {
            SexBTN.setTitle(PickerValue.objectAtIndex(0) as? String, forState: UIControlState.Normal)
        }
        else if detectPicker==3
        {
            TeamBTN.setTitle(PickerValue.objectAtIndex(0) as? String, forState: UIControlState.Normal)
        }
        pickerToolbar.hidden=false
        pickerView.hidden=false
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: false)
    }
    
    func donePicker()
    {
        pickerView.hidden=true
        pickerToolbar.hidden=true
    }
    
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            self.navigationController?.popViewControllerAnimated(true)
        }
        HUD.hide(true)
    }
    
    /******************************************************
    MARK:PickerView Delegate Method
    ******************************************************/
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return PickerValue.count
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if detectPicker==1
        {
            SkillBTN.setTitle(PickerValue.objectAtIndex(row) as? String, forState: UIControlState.Normal)
        }
        else if detectPicker==2
        {
            SexBTN.setTitle(PickerValue.objectAtIndex(row) as? String, forState: UIControlState.Normal)
        }
        else if detectPicker==3
        {
            TeamBTN.setTitle(PickerValue.objectAtIndex(row) as? String, forState: UIControlState.Normal)
        }
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return PickerValue.objectAtIndex(row) as! String
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func Gender(Str:String!)->String
    {
        if Str=="Male Only"
        {
            return "M"
        }
        else if Str=="Female Only"
        {
            return "F"
        }
        else
        {
            return "C"
        }
    }
    
    func Skill(Str:String!)->Int
    {
        //print(Str)
        if Str=="Beginner"
        {
            return 0
        }
        else if Str=="DIV D"
        {
            return 1
        }
        else if Str=="DIV C/D"
        {
            return 2
        }
        else if Str=="DIV C"
        {
            return 3
        }
        else if Str=="DIV B/C"
        {
            return 4
        }
        else if Str=="DIV B"
        {
            return 5
        }
        else if Str=="DIV A/B"
        {
            return 6
        }
        else
        {
            return 7
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        
        if NSUserDefaults.standardUserDefaults().valueForKey("MinAge") != nil
        {
            AgeBTN.setTitle(NSString(format: "%i to %i", NSUserDefaults.standardUserDefaults().valueForKey("MinAge") as! Int,NSUserDefaults.standardUserDefaults().valueForKey("MaxAge") as! Int) as String, forState: UIControlState.Normal)
        }
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }

}
