//
//  AvailableVC.swift
//  GoalieHub
//
//  Created by Vivek on 02/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class AvailableVC: UIViewController,MBProgressHUDDelegate {
    var registerUDIDConnectionAvailability=NSURLConnection()
    var registerUDIDDataAvailability : NSMutableData!
    var HUD:MBProgressHUD!
    var ID:Int!,KEY:Int!,UserID:Int!
    let cellIdentifier = "cellIdentifier"
    var DayDict=NSDictionary()
    var AvailabilityAry=NSMutableArray()
    @IBOutlet var AvailabilityTBL: UITableView!
    @IBOutlet var PopupView: UIView!
    @IBOutlet var BackView: UIView!
    @IBOutlet var EditBTN: UIButton!
    @IBOutlet var RemoveBTN: UIButton!
    var Index:Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        self.AvailabilityTBL?.registerClass(UITableViewCell.self, forCellReuseIdentifier: self.cellIdentifier)
        AvailabilityTBL.tableFooterView = UIView(frame: CGRectZero)
        AvailabilityTBL.separatorStyle=UITableViewCellSeparatorStyle.None
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "AvailableLoad:", name:"Available", object: nil)
        
        EditBTN.clipsToBounds=true
        EditBTN.layer.cornerRadius=5.0
        EditBTN.layer.borderWidth=1.0
        EditBTN.layer.borderColor=UIColor.whiteColor().CGColor
        
        RemoveBTN.clipsToBounds=true
        RemoveBTN.layer.cornerRadius=5.0
        RemoveBTN.layer.borderWidth=1.0
        RemoveBTN.layer.borderColor=UIColor.whiteColor().CGColor
        
        let gesture = UITapGestureRecognizer(target: self, action: "PopupAction:")
        BackView.addGestureRecognizer(gesture)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func PopupAction(sender:UITapGestureRecognizer){
        BackView.hidden=true
        PopupView.hidden=true
    }
    
    @IBAction func RemoveBTNClick(sender: UIButton) {
        BackView.hidden=true
        PopupView.hidden=true
        
    }
    @IBAction func EditBTNClick(sender: UIButton) {
        BackView.hidden=true
        PopupView.hidden=true
        
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("AddAvailabilityVC") as! AddAvailabilityVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
        initialViewController.DayDict=AvailabilityAry.objectAtIndex(Index) as! NSMutableDictionary
        initialViewController.TitleStr=FindDay(Index)
    }
    func AvailableLoad(notification: NSNotification){
        AvailabilityAry=NSMutableArray()
        HUD.show(true)
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as Int
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as Int
        let ParamsDict=NSDictionary(objectsAndKeys: AgentID,"agentId",LocationID,"locId")
        let RequestDict=NSDictionary(objectsAndKeys: "getMyAgentAvailability","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Agent",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
        }
    }
    
    
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            var TempAry=NSMutableArray()
            if let checkDict = Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AvailabilityPattern") as? NSDictionary
            {
                TempAry.addObject(Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AvailabilityPattern")!)
            }
            else
            {
                TempAry=NSMutableArray(objects: Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AvailabilityPattern") as! NSArray)
            }
            //print(TempAry)
            var FInalTempAry=NSMutableArray()
            for var i=0;i<TempAry.count;i++
            {
                var DayAry=TempAry.objectAtIndex(i).valueForKey("AvailabilityPatternDays") as! NSArray
                for var j=0;j<DayAry.count;j++
                {
                    FInalTempAry.addObject(DayAry.objectAtIndex(j))
                }
            }
            //print(FInalTempAry)
            for var h=0;h<7;h++
            {
                var TempDict=NSMutableDictionary()
                var Ary=NSMutableArray()
                for var k=0;k<FInalTempAry.count;k++
                {
                    if FInalTempAry.objectAtIndex(k).valueForKey("availpatdaysDay")!.integerValue == h
                    {
                        Ary.addObject(FInalTempAry.objectAtIndex(k))
                    }
                }
                TempDict.setValue(Ary, forKey: "Day")
                print(TempDict)
                AvailabilityAry.addObject(TempDict)
            }
            print(AvailabilityAry)
            AvailabilityTBL.reloadData()
            HUD.hide(true)
        }
    }
    
    /********************************************
    MARK:TableView Delegate
    ********************************************/
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return AvailabilityAry.count
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as! UITableViewCell
        cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: self.cellIdentifier)
        cell.frame=CGRectMake(0, 0, self.view.frame.size.width, 40)
        
        
        let SubBackView=UIView(frame: CGRectMake(10, 10, cell.frame.size.width-20, 60))
        //BackView.backgroundColor=UIColor.whiteColor()
        
        
        let TItleLBL=UILabel(frame: CGRectMake(20, 10, SubBackView.frame.size.width-35, 15))
        TItleLBL.text="Times:"
        TItleLBL.font=UIFont.boldSystemFontOfSize(12)
        TItleLBL.textColor=UIColor.lightGrayColor()
        SubBackView.addSubview(TItleLBL)
        var Height:CGFloat=TItleLBL.frame.origin.y+TItleLBL.frame.size.height+10
        var RowAry=NSMutableArray(array: AvailabilityAry.objectAtIndex(indexPath.section).valueForKey("Day") as! NSArray)
        if RowAry.count > 0
        {
            for var i=0;i<RowAry.count;i++
            {
                let FromDateFormatter = NSDateFormatter()
                FromDateFormatter.dateFormat = "HH:mm:ss"
                let FromDateObj = FromDateFormatter.dateFromString(RowAry.objectAtIndex(i).valueForKey("availpatdaysStartTime") as! String)
                FromDateFormatter.dateFormat = "hh:mm a"
                
                
                let ToDateFormatter = NSDateFormatter()
                ToDateFormatter.dateFormat = "HH:mm:ss"
                let ToDateObj = ToDateFormatter.dateFromString(RowAry.objectAtIndex(i).valueForKey("availpatdaysEndTime") as! String)
                ToDateFormatter.dateFormat = "hh:mm a"
                
                
                let TimeLBL=UILabel(frame: CGRectMake(20, Height, SubBackView.frame.size.width-40, 15))
                TimeLBL.textColor=UIColor.lightGrayColor()
                var attrString: NSMutableAttributedString = NSMutableAttributedString(string: NSString(format: "%@ to %@", FromDateFormatter.stringFromDate(FromDateObj!),ToDateFormatter.stringFromDate(ToDateObj!)) as String)
                attrString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0), range: NSMakeRange(0, count(FromDateFormatter.stringFromDate(FromDateObj!))))
                attrString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0), range: NSMakeRange(count(FromDateFormatter.stringFromDate(FromDateObj!))+4, count(ToDateFormatter.stringFromDate(ToDateObj!))))
                TimeLBL.attributedText=attrString
                TimeLBL.textAlignment=NSTextAlignment.Center
                TimeLBL.font=UIFont.systemFontOfSize(12)
                
                SubBackView.addSubview(TimeLBL)
                
                Height = Height + 30
            }
        }
        else
        {
            let TimeLBL=UILabel(frame: CGRectMake(20, Height, SubBackView.frame.size.width-40, 15))
            TimeLBL.text="DAY OFF "
            TimeLBL.textColor=UIColor.lightGrayColor()
            TimeLBL.textAlignment=NSTextAlignment.Center
            TimeLBL.font=UIFont.systemFontOfSize(12)
            TimeLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            SubBackView.addSubview(TimeLBL)
            Height = Height + 30
        }
        
        
        SubBackView.frame=CGRectMake(10, 10, cell.frame.size.width-20, Height)
        let LineIMG=UIImageView(frame: CGRectMake(0, 0, 10, Height))
        LineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(LineIMG)
        
        let SmallLineIMG=UIImageView(frame: CGRectMake(0, SubBackView.frame.size.height-1, SubBackView.frame.size.width, 1))
        SmallLineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(SmallLineIMG)
        
        let LastIMG=UIImageView(frame: CGRectMake(SubBackView.frame.size.width-20, 5, 20, 20))
        LastIMG.image=UIImage(named: "ic_general_more_details.png")
        SubBackView.addSubview(LastIMG)
        cell.addSubview(SubBackView)
        cell.backgroundColor=UIColor.blackColor()
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        
        AvailabilityTBL.rowHeight = SubBackView.frame.size.height+20
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        Index=indexPath.section
        BackView.hidden=false
        PopupView.hidden=false
    }
    
    func tableView(tableView: UITableView!, viewForHeaderInSection section: Int) -> UIView! {
        
        let SectionBackView=UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, 20))
        SectionBackView.backgroundColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        let iconIMG=UIImageView(frame: CGRectMake(5, 0, 20, 20))
        iconIMG.image=UIImage(named: "ic_calendar_blue.png")
        SectionBackView.addSubview(iconIMG)
        
        let TitleLBL=UILabel(frame: CGRectMake(30, 0, SectionBackView.frame.size.width-40, 20))
        if section==0
        {
            TitleLBL.text="Sundays"
        }
        else if section == 1
        {
            TitleLBL.text="Mondays"
        }
        else if section == 2
        {
            TitleLBL.text="Tuesdays"
        }
        else if section == 3
        {
            TitleLBL.text="Wednesdays"
        }
        else if section == 4
        {
            TitleLBL.text="Thursdays"
        }
        else if section == 5
        {
            TitleLBL.text="Fridays"
        }
        else
        {
            TitleLBL.text="Saturdays"
        }
        TitleLBL.textColor=UIColor.whiteColor()
        TitleLBL.font=UIFont.boldSystemFontOfSize(12)
        SectionBackView.addSubview(TitleLBL)
        
        return SectionBackView
    }
    func FindDay(DayNo:Int!)->String
    {
        if DayNo == 0
        {
            return "Sundays"
        }
        else if DayNo==1
        {
            return "Mondays"
        }
        else if DayNo==2
        {
            return "Tuesdays"
        }
        else if DayNo==3
        {
            return "Wednesdays"
        }
        else if DayNo==4
        {
            return "Thursdays"
        }
        else if DayNo==5
        {
            return "Fridays"
        }
        else
        {
            return "Saturdays"
        }
    }
}
