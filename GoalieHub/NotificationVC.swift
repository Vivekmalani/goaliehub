//
//  NotificationVC.swift
//  GoalieHub
//
//  Created by Vivek on 14/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire

class NotificationVC: UIViewController,MBProgressHUDDelegate {
    let cellIdentifier = "cellIdentifier"
    @IBOutlet var NotificationTBL: UITableView!
    @IBOutlet var BackView: UIView!
    @IBOutlet var PopupView: UIView!
    
    var HUD:MBProgressHUD!
    var ID:Int!,KEY:Int!,LocID:Int!
    var NotificationAry=NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        HUD.show(true)
        
        self.NotificationTBL?.registerClass(UITableViewCell.self, forCellReuseIdentifier: self.cellIdentifier)
        NotificationTBL.tableFooterView = UIView(frame: CGRectZero)
        NotificationTBL.separatorStyle=UITableViewCellSeparatorStyle.None
        
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as Int
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as Int
        let ValueDict=NSDictionary(objectsAndKeys: 25,"limitResult",1,"pageNumber")
        let ParamsDict=NSDictionary(objectsAndKeys: ValueDict,"Value")
        let RequestDict=NSDictionary(objectsAndKeys: "getMyNotifications","method","1","id",ParamsDict,"params")
        //print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "User",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
        }
        
        let gesture = UITapGestureRecognizer(target: self, action: "PopupAction:")
        BackView.addGestureRecognizer(gesture)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func PopupAction(sender:UITapGestureRecognizer){
        BackView.hidden=true
        PopupView.hidden=true
    }
    
    /********************************************
    MARK:TableView Delegate
    ********************************************/
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NotificationAry.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as! UITableViewCell
        cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: self.cellIdentifier)
        cell.frame=CGRectMake(0, 0, self.view.frame.size.width, 40)
        var Height:CGFloat=45.0
        let SubBackView=UIView(frame: CGRectMake(10, 10, cell.frame.size.width-20, 45))
        
        let iconIMG=UIImageView(frame: CGRectMake(15, 10, 15, 15))
        SubBackView.addSubview(iconIMG)
        
        let TextLBL=UILabel(frame: CGRectMake(iconIMG.frame.size.width+iconIMG.frame.origin.x+5, 10, 70, 15))
        //TextLBL.text="ATTENDING"
        TextLBL.textColor=UIColor.whiteColor()
        TextLBL.font=UIFont.systemFontOfSize(10)
        SubBackView.addSubview(TextLBL)
        
        let TitleIcon=UIImageView(frame: CGRectMake(TextLBL.frame.size.width+TextLBL.frame.origin.x+5, 10, 20, 20))
        TitleIcon.clipsToBounds=true
        TitleIcon.layer.cornerRadius=TitleIcon.frame.size.width/2
        SubBackView.addSubview(TitleIcon)
        
        let DateLBL=UILabel(frame: CGRectMake(SubBackView.frame.size.width-120, 0, 100, 15))
        DateLBL.textColor=UIColor.whiteColor()
        DateLBL.font=UIFont.systemFontOfSize(12)
        SubBackView.addSubview(DateLBL)
        //print(NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname"))
        
        let TitleLBL=UILabel(frame: CGRectMake(TitleIcon.frame.size.width+TitleIcon.frame.origin.x+5, 14, SubBackView.frame.size.width-(TitleIcon.frame.origin.x+40), 15))
        TitleLBL.textColor=UIColor.whiteColor()
        TitleLBL.font=UIFont.systemFontOfSize(12)
        
        SubBackView.addSubview(TitleLBL)
        
        
        let data = NotificationAry.objectAtIndex(indexPath.row).valueForKey("notifMessage")!.dataUsingEncoding(NSUTF8StringEncoding)
        
        if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("PREQ")
        {
            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TitleIcon.image=UIImage(data: decodedData!)
            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("eventTs") as! String)
            DateFormatter.dateFormat = "dd/MM hh:mm a"
            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
            
            TextLBL.text="REQUEST"
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("PRRSP")
        {
//            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
//            TitleIcon.image=UIImage(data: decodedData!)
//            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
//            let DateFormatter = NSDateFormatter()
//            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("eventTs") as! String)
//            DateFormatter.dateFormat = "dd/MM hh:mm a"
//            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
            
            TextLBL.text="Pri RESPONSE"
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("CANPR")
        {
            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TitleIcon.image=UIImage(data: decodedData!)
            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("eventTs") as! String)
            DateFormatter.dateFormat = "dd/MM hh:mm a"
            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("PURSP")
        {
//            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
//            TitleIcon.image=UIImage(data: decodedData!)
//            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            
//            let DateFormatter = NSDateFormatter()
//            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("eventTs") as! String)
//            DateFormatter.dateFormat = "dd/MM hh:mm a"
//            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
            
            TextLBL.text="Pub RESPONSE"
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("CANPO")
        {
            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TitleIcon.image=UIImage(data: decodedData!)
            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("eventTs") as! String)
            DateFormatter.dateFormat = "dd/MM hh:mm a"
            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("ACPUR")
        {
            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TitleIcon.image=UIImage(data: decodedData!)
            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("eventTs") as! String)
            DateFormatter.dateFormat = "dd/MM hh:mm a"
            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
            
            TextLBL.text="REQUEST"
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("REJAG")
        {
            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TitleIcon.image=UIImage(data: decodedData!)
            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("eventTs") as! String)
            DateFormatter.dateFormat = "dd/MM hh:mm a"
            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("CANEV")
        {
            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TitleIcon.image=UIImage(data: decodedData!)
            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("eventTs") as! String)
            DateFormatter.dateFormat = "dd/MM hh:mm a"
            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
            
            TextLBL.text="CANCELLED"
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("CANAG")
        {
            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TitleIcon.image=UIImage(data: decodedData!)
            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("eventTs") as! String)
            DateFormatter.dateFormat = "dd/MM hh:mm a"
            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
            
            TextLBL.text="CANCELLED"
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("CANCA")
        {
            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TitleIcon.image=UIImage(data: decodedData!)
            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("eventTs") as! String)
            DateFormatter.dateFormat = "dd/MM hh:mm a"
            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("TMEV")
        {
//            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
//            TitleIcon.image=UIImage(data: decodedData!)
//            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            
//            let DateFormatter = NSDateFormatter()
//            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("eventTs") as! String)
//            DateFormatter.dateFormat = "dd/MM hh:mm a"
//            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
            
            TextLBL.text="EVENT"
            
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("FEED")
        {
            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vUserSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TitleIcon.image=UIImage(data: decodedData!)
            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("ratdetCreateTs") as! String)
            DateFormatter.dateFormat = "dd/MM hh:mm a"
            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
            
            TextLBL.text="FEEDBACK"
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("COMM")
        {
            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("Event")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TitleIcon.image=UIImage(data: decodedData!)
            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("Event")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("Event")!.valueForKey("eventTs") as! String)
            DateFormatter.dateFormat = "dd/MM hh:mm a"
            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
            
            let CreatedLBL=UILabel(frame: CGRectMake(TitleIcon.frame.origin.x, TitleLBL.frame.size.height+TitleLBL.frame.origin.y+5, 50, 15))
            CreatedLBL.text="Left by : "
            CreatedLBL.textColor=UIColor.lightGrayColor()
            CreatedLBL.font=UIFont.systemFontOfSize(12)
            SubBackView.addSubview(CreatedLBL)
            
            let UserData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vUserSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            let UserIMG=UIImageView(frame: CGRectMake(CreatedLBL.frame.size.width+CreatedLBL.frame.origin.x+5, TitleLBL.frame.size.height+TitleLBL.frame.origin.y+5, 20, 20))
            UserIMG.clipsToBounds=true
            UserIMG.layer.cornerRadius=UserIMG.frame.size.width/2
            UserIMG.image=UIImage(data: UserData!)
            SubBackView.addSubview(UserIMG)
            
            let UserNAmeLBL=UILabel(frame: CGRectMake(UserIMG.frame.size.width+UserIMG.frame.origin.x+5, TitleLBL.frame.size.height+TitleLBL.frame.origin.y+8, SubBackView.frame.size.width-UserIMG.frame.origin.x-20, 15))
            UserNAmeLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
            UserNAmeLBL.textColor=UIColor.whiteColor()
            UserNAmeLBL.font=UIFont.systemFontOfSize(12)
            SubBackView.addSubview(UserNAmeLBL)
            
            TextLBL.text="COMMENTS"
            
            Height = UserIMG.frame.origin.y+UserIMG.frame.size.height+5
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("TUSTA")
        {
            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TitleIcon.image=UIImage(data: decodedData!)
            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("teamuserCreateTs") as! String)
            DateFormatter.dateFormat = "dd/MM hh:mm a"
            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
            
            let CreatedLBL=UILabel(frame: CGRectMake(TitleIcon.frame.origin.x, TitleLBL.frame.size.height+TitleLBL.frame.origin.y+5, 70, 15))
            CreatedLBL.text="Change by : "
            CreatedLBL.textColor=UIColor.lightGrayColor()
            CreatedLBL.font=UIFont.systemFontOfSize(12)
            SubBackView.addSubview(CreatedLBL)
            
            let UserData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vUserSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            let UserIMG=UIImageView(frame: CGRectMake(CreatedLBL.frame.size.width+CreatedLBL.frame.origin.x+5, TitleLBL.frame.size.height+TitleLBL.frame.origin.y+5, 20, 20))
            UserIMG.clipsToBounds=true
            UserIMG.layer.cornerRadius=UserIMG.frame.size.width/2
            UserIMG.image=UIImage(data: UserData!)
            SubBackView.addSubview(UserIMG)
            
            let UserNAmeLBL=UILabel(frame: CGRectMake(UserIMG.frame.size.width+UserIMG.frame.origin.x+5, TitleLBL.frame.size.height+TitleLBL.frame.origin.y+8, SubBackView.frame.size.width-UserIMG.frame.origin.x-20, 15))
            UserNAmeLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
            UserNAmeLBL.textColor=UIColor.whiteColor()
            UserNAmeLBL.font=UIFont.systemFontOfSize(12)
            SubBackView.addSubview(UserNAmeLBL)
            
            TextLBL.text="CHANGED"
            
            Height = UserIMG.frame.origin.y+UserIMG.frame.size.height+5
            
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("NWTMU")
        {
            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TitleIcon.image=UIImage(data: decodedData!)
            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("teamuserCreateTs") as! String)
            DateFormatter.dateFormat = "dd/MM hh:mm a"
            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
            
            let CreatedLBL=UILabel(frame: CGRectMake(TitleIcon.frame.origin.x, TitleLBL.frame.size.height+TitleLBL.frame.origin.y+5, 70, 15))
            CreatedLBL.text="Created by : "
            CreatedLBL.textColor=UIColor.lightGrayColor()
            CreatedLBL.font=UIFont.systemFontOfSize(12)
            SubBackView.addSubview(CreatedLBL)
            
            let UserData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vUserSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            let UserIMG=UIImageView(frame: CGRectMake(CreatedLBL.frame.size.width+CreatedLBL.frame.origin.x+5, TitleLBL.frame.size.height+TitleLBL.frame.origin.y+5, 20, 20))
            UserIMG.clipsToBounds=true
            UserIMG.layer.cornerRadius=UserIMG.frame.size.width/2
            UserIMG.image=UIImage(data: UserData!)
            SubBackView.addSubview(UserIMG)
            
            let UserNAmeLBL=UILabel(frame: CGRectMake(UserIMG.frame.size.width+UserIMG.frame.origin.x+5, TitleLBL.frame.size.height+TitleLBL.frame.origin.y+8, SubBackView.frame.size.width-UserIMG.frame.origin.x-20, 15))
            UserNAmeLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
            UserNAmeLBL.textColor=UIColor.whiteColor()
            UserNAmeLBL.font=UIFont.systemFontOfSize(12)
            SubBackView.addSubview(UserNAmeLBL)
            
            TextLBL.text="ADDED"
            
            Height = UserIMG.frame.origin.y+UserIMG.frame.size.height+5
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("ATTRQ")
        {
            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TitleIcon.image=UIImage(data: decodedData!)
            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("eventTs") as! String)
            DateFormatter.dateFormat = "dd/MM hh:mm a"
            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
        }
        else if NotificationAry.objectAtIndex(indexPath.row).valueForKey("vTypes")!.valueForKey("typcodeShortname")!.isEqualToString("ATTCH")
        {
            let decodedData = NSData(base64EncodedString: NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            TitleIcon.image=UIImage(data: decodedData!)
            TitleLBL.text=NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
//            TitleLBL.numberOfLines=0
//            TitleLBL.sizeToFit()
            
            let DateFormatter = NSDateFormatter()
            DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let DateObj = DateFormatter.dateFromString(NotificationAry.objectAtIndex(indexPath.row).valueForKey("Details")!.valueForKey("eventTs") as! String)
            DateFormatter.dateFormat = "dd/MM hh:mm a"
            DateLBL.text=DateFormatter.stringFromDate(DateObj!)
        }
        SubBackView.frame=CGRectMake(10, 10, cell.frame.size.width-20, Height)
        
        let LineIMG=UIImageView(frame: CGRectMake(0, 0, 10, Height))
        LineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(LineIMG)
        
        let SmallLineIMG=UIImageView(frame: CGRectMake(0, SubBackView.frame.size.height-1, SubBackView.frame.size.width, 1))
        SmallLineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(SmallLineIMG)
        
        let LastIMG=UIImageView(frame: CGRectMake(SubBackView.frame.size.width-20, 5, 20, 20))
        LastIMG.image=UIImage(named: "ic_general_more_details.png")
        SubBackView.addSubview(LastIMG)
        
        cell.backgroundColor=UIColor.blackColor()
        cell.addSubview(SubBackView)
        NotificationTBL.rowHeight = SubBackView.frame.size.height+20
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        BackView.hidden=false
        PopupView.hidden=false
        
        let DismissBTN=UIButton(frame: CGRectMake(0, 0, PopupView.frame.size.width, 35))
        DismissBTN.setTitle("Dismiss", forState: UIControlState.Normal)
        DismissBTN.clipsToBounds=true
        DismissBTN.layer.cornerRadius=5.0
        DismissBTN.layer.borderWidth=1.0
        DismissBTN.layer.borderColor=UIColor.whiteColor().CGColor
        DismissBTN.backgroundColor=UIColor.blackColor()
        PopupView.addSubview(DismissBTN)
        
        PopupView.frame.size.height=35.0
    }
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            if let CheckDict=Dict.valueForKey("response")!.valueForKey("result") as? NSDictionary
            {
                NotificationAry.addObject(Dict.valueForKey("response")!.valueForKey("result")!)
            }
            else if let CheckAry=Dict.valueForKey("response")!.valueForKey("result") as? NSArray
            {
                NotificationAry=NSMutableArray(array: CheckAry)
            }
            
            var Noti:Int=0
            for var i=0;i<NotificationAry.count;i++
            {
                if NotificationAry.objectAtIndex(i).valueForKey("notifRead")!.isEqualToString("N")
                {
                    Noti++
                }
            }
            let tabArray = self.tabBarController?.tabBar.items as NSArray!
            let tabItem = tabArray.objectAtIndex(1) as! UITabBarItem
            tabItem.badgeValue = String(Noti)
            NotificationTBL.reloadData()
        }
        HUD.hide(true)
    }
    
}
