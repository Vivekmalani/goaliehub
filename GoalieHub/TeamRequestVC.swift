//
//  TeamRequestVC.swift
//  GoalieHub
//
//  Created by Vivek on 18/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class TeamRequestVC: UIViewController,MBProgressHUDDelegate
{
    @IBOutlet var DeleteBTN: UIButton!
    @IBOutlet var JoinBTN: UIButton!
    @IBOutlet var PopupView: UIView!
    @IBOutlet var BackView: UIView!
    @IBOutlet var RequestTBL: UITableView!
    @IBOutlet var BarBTN: UIBarButtonItem!
    @IBOutlet var TeamNameLBL: UILabel!
    @IBOutlet var TeamIMG: UIImageView!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!
    let cellIdentifier = "cellIdentifier"
    var RequestAry=NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        
        DeleteBTN.clipsToBounds=true
        DeleteBTN.layer.cornerRadius=5.0
        DeleteBTN.layer.borderWidth=1.0
        DeleteBTN.layer.borderColor=UIColor.whiteColor().CGColor
        
        JoinBTN.clipsToBounds=true
        JoinBTN.layer.cornerRadius=5.0
        JoinBTN.layer.borderWidth=1.0
        JoinBTN.layer.borderColor=UIColor.whiteColor().CGColor
        
        self.RequestTBL?.registerClass(UITableViewCell.self, forCellReuseIdentifier: self.cellIdentifier)
        RequestTBL.tableFooterView = UIView(frame: CGRectZero)
        RequestTBL.separatorStyle=UITableViewCellSeparatorStyle.None
        
        HUD.show(true)
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        let ValueDict=NSDictionary(objectsAndKeys: "all","active")
        let ParamsDict=NSDictionary(objectsAndKeys:ValueDict,"Value")
        let RequestDict=NSDictionary(objectsAndKeys: "getMyTeams","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Team",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse1(response.3, Dict: response.2 as? NSDictionary)
        }
        
        let gesture = UITapGestureRecognizer(target: self, action: "PopupAction:")
        BackView.addGestureRecognizer(gesture)
    }

    func PopupAction(sender:UITapGestureRecognizer){
        BackView.hidden=true
        PopupView.hidden=true
    }
    
    @IBAction func DeleteBTNClick(sender: UIButton) {
    }
    @IBAction func JoinBTNClick(sender: UIButton) {
    }
    
    /********************************************
    MARK:TableView Delegate
    ********************************************/
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RequestAry.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as! UITableViewCell
        cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: self.cellIdentifier)
        cell.frame=CGRectMake(0, 0, self.view.frame.size.width, 40)
        
        let SubBackView=UIView(frame: CGRectMake(10, 10, cell.frame.size.width-20, 50))
        
        let LineIMG=UIImageView(frame: CGRectMake(0, 0, 10, 50))
        LineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(LineIMG)
        
        let decodedData = NSData(base64EncodedString: RequestAry.objectAtIndex(indexPath.row).valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
        let TitleIcon=UIImageView(frame: CGRectMake(LineIMG.frame.size.width+LineIMG.frame.origin.x+5, 5, 20, 20))
        TitleIcon.image=UIImage(data: decodedData!)
        SubBackView.addSubview(TitleIcon)
        
        let TItleLBL=UILabel(frame: CGRectMake(TitleIcon.frame.size.width+TitleIcon.frame.origin.x+5, 8, SubBackView.frame.size.width-TitleIcon.frame.origin.x-170, 15))
        TItleLBL.text=RequestAry.objectAtIndex(indexPath.row).valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
        TItleLBL.font=UIFont.boldSystemFontOfSize(12)
        TItleLBL.textColor=UIColor.whiteColor()
//        TItleLBL.layer.borderWidth=1.0
//        TItleLBL.layer.borderColor=UIColor.whiteColor().CGColor
        TItleLBL.numberOfLines=0
        TItleLBL.sizeToFit()
        SubBackView.addSubview(TItleLBL)
        
        let DateFormatter = NSDateFormatter()
        DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let DateObj = DateFormatter.dateFromString(RequestAry.objectAtIndex(indexPath.row).valueForKey("teamuserCreateTs") as! String)
        DateFormatter.dateFormat = "d/m/yy h:mm a"
        let TimeLBL=UILabel(frame: CGRectMake(SubBackView.frame.size.width-120, 5, 120, 15))
        TimeLBL.text=DateFormatter.stringFromDate(DateObj!)
        TimeLBL.font=UIFont.systemFontOfSize(12)
        TimeLBL.textColor=UIColor.whiteColor()
        SubBackView.addSubview(TimeLBL)
        
        let CaleIcon=UIImageView(frame: CGRectMake(TimeLBL.frame.origin.x-20,5,15,15))
        CaleIcon.image=UIImage(named: "ic_calendar_blue.png")
        SubBackView.addSubview(CaleIcon)
        
        
        let AddedLBL=UILabel(frame: CGRectMake(LineIMG.frame.size.width+LineIMG.frame.origin.x+5, CaleIcon.frame.size.height+CaleIcon.frame.origin.y+5, SubBackView.frame.size.width-40, 15))
        
        if RequestAry.objectAtIndex(indexPath.row).valueForKey("teamuserType")!.integerValue == 60
        {
            AddedLBL.text="PENDING USER APPROVAL"
        }
        else
        {
            AddedLBL.text="PENDING TEAM APPROVAL"
        }
        AddedLBL.textAlignment=NSTextAlignment.Right
        AddedLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        AddedLBL.font=UIFont.systemFontOfSize(12)
        SubBackView.addSubview(AddedLBL)
        
        let SmallLineIMG=UIImageView(frame: CGRectMake(0, SubBackView.frame.size.height-1, SubBackView.frame.size.width, 1))
        SmallLineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(SmallLineIMG)
        
        let LastIMG=UIImageView(frame: CGRectMake(SubBackView.frame.size.width-20, 5, 20, 20))
        LastIMG.image=UIImage(named: "ic_general_more_details.png")
        SubBackView.addSubview(LastIMG)
        cell.addSubview(SubBackView)
        cell.backgroundColor=UIColor.blackColor()
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        RequestTBL.rowHeight = SubBackView.frame.size.height+20
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        BackView.hidden=false
        PopupView.hidden=false
    }
    
    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func HandleAPIResponse1(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            var TempAry=NSMutableArray()
            if let CheckDict=Dict.valueForKey("response")!.valueForKey("result") as? NSDictionary
            {
                TempAry.addObject(CheckDict)
            }
            else if let checkAry=Dict.valueForKey("response")!.valueForKey("result") as? NSArray
            {
                TempAry=NSMutableArray(array: checkAry)
            }
            
            for var i=0;i<TempAry.count;i++
            {
                if TempAry.objectAtIndex(i).valueForKey("teamuserType")!.integerValue == 60 || TempAry.objectAtIndex(i).valueForKey("teamuserType")!.integerValue == 70
                {
                    RequestAry.addObject(TempAry.objectAtIndex(i))
                }
            }
            RequestTBL.reloadData()
        }
        HUD.hide(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
