//
//  ProfilePictureVC.swift
//  GoalieHub
//
//  Created by Vivek on 09/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit

class ProfilePictureVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet var BarBTN: UIBarButtonItem!
    @IBOutlet var ProfileIMG: UIImageView!
    @IBOutlet var SaveBTN: UIButton!
    
    @IBOutlet var CheckIMG: UIImageView!
    @IBOutlet var BackScroll: UIScrollView!
    @IBOutlet var ImageBTN: UIButton!
    var picker=UIImagePickerController()
    var popover:UIPopoverController?=nil
    var originalImage:UIImage!
    var cropper:BFCropInterface!
    var CheckImage:Int=0
    var Rotation:CGFloat=0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        picker.delegate=self
        
        ProfileIMG.userInteractionEnabled=true
        self.originalImage=UIImage(named: "user.png")
        ProfileIMG.image=self.originalImage
        self.cropper=BFCropInterface(frame: ProfileIMG.bounds, andImage: self.originalImage)
        self.cropper.shadowColor=UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.60)
        self.cropper.borderColor=UIColor.whiteColor()
        ProfileIMG.addSubview(self.cropper)
        
        //BackScroll.contentSize=CGSizeMake(0, SaveBTN.frame.size.height+SaveBTN.frame.origin.y+10)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func TemsBTNClick(sender: UIButton) {
        if CheckImage==0
        {
            CheckIMG.image=UIImage(named: "ic_checkbox_checked.png")
            CheckImage=1
        }
        else
        {
            CheckIMG.image=UIImage(named: "ic_checkbox_un_check.png")
            CheckImage=0
        }
    }
    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func SaveBTNClick(sender: UIButton) {
        
        let imageData=UIImageJPEGRepresentation(ProfileIMG.image, 1.0)
        let base64String = imageData.base64EncodedStringWithOptions(.allZeros)
        print(base64String)
        NSUserDefaults.standardUserDefaults().setValue(base64String, forKey: "ImageStr")
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func AddIMGBTNClick(sender: UIButton) {
        var alert:UIAlertController=UIAlertController(title: String(format: NSLocalizedString("Choose Image", comment: "")), message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        var cameraAction = UIAlertAction(title: String(format: NSLocalizedString("Camera", comment: "")), style: UIAlertActionStyle.Default)
            {
                UIAlertAction in
                self.openCamera()
                
        }
        var gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.Default)
            {
                UIAlertAction in
                self.openGallary()
        }
        var cancelAction = UIAlertAction(title:"Cancel", style: UIAlertActionStyle.Cancel)
            {
                UIAlertAction in
        }
        // Add the actions
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        // Present the actionsheet
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else
        {
            popover=UIPopoverController(contentViewController: alert)
            popover!.presentPopoverFromRect(ImageBTN.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        }
    }
    @IBAction func CropBTNClick(sender: UIButton) {
        let croppedImage:UIImage=self.cropper.getCroppedImage()
        self.cropper.removeFromSuperview()
        self.cropper=nil
        self.originalImage=croppedImage
        ProfileIMG.image = self.originalImage
        self.cropper = BFCropInterface(frame: ProfileIMG.bounds, andImage: self.originalImage)
        self.cropper.shadowColor=UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.60)
        self.cropper.borderColor=UIColor.whiteColor()
        ProfileIMG.addSubview(self.cropper)
    }
    @IBAction func RotateBTNClick(sender: UIButton) {
        
        Rotation = Rotation + CGFloat(M_PI_2)
        
        self.ProfileIMG.transform = CGAffineTransformMakeRotation(Rotation)
        
        //ProfileIMG.transform=CGAffineTransformMakeRotation(80)
    }

    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            picker.sourceType = UIImagePickerControllerSourceType.Camera
            self .presentViewController(picker, animated: true, completion: nil)
        }
        else
        {
            var alert = UIAlertController(title: nil, message: "No comera found in this device", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: String(format: NSLocalizedString("OK", comment: "")), style: UIAlertActionStyle.Default, handler: nil))
        }
    }
    func openGallary()
    {
        picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(picker, animated: true, completion: nil)
        }
        else
        {
            popover=UIPopoverController(contentViewController: picker)
            popover!.presentPopoverFromRect(ImageBTN.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        }
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject])
    {
        let newImage:UIImage=info[UIImagePickerControllerOriginalImage] as! UIImage
        
        let rect:CGRect=CGRectMake(0, 0, 200, 200)
        UIGraphicsBeginImageContext(rect.size)
        newImage.drawInRect(rect)
        let picture1=UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let imageData=UIImagePNGRepresentation(picture1)
        
        self.originalImage = UIImage(data: imageData)
        self.cropper .removeFromSuperview()
        self.cropper=nil
        ProfileIMG.image=self.originalImage
        self.cropper = BFCropInterface(frame: ProfileIMG.bounds, andImage: self.originalImage)
        self.cropper.shadowColor=UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.60)
        self.cropper.borderColor=UIColor.whiteColor()
        ProfileIMG.addSubview(self.cropper)
        picker.dismissViewControllerAnimated(true, completion: nil)
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
        //println(info[UIImagePickerControllerOriginalImage])
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        self.dismissViewControllerAnimated(true, completion: {});
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
    }
    
    
    override func viewWillAppear(animated: Bool) {
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
        
    }

}
