//
//  ScheduleVC.swift
//  GoalieHub
//
//  Created by Vivek on 12/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class ScheduleVC: UIViewController,MBProgressHUDDelegate,UIPickerViewDataSource,UIPickerViewDelegate {
    @IBOutlet var BarBTN: UIBarButtonItem!
    
    @IBOutlet var EventTypeTXT: UITextField!
    @IBOutlet var EventTBL: UITableView!
    let cellIdentifier = "cellIdentifier"
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!,LocID:Int!
    
    var EventAry=NSMutableArray()
    var pickerView:UIPickerView!
    var pickerToolbar:UIToolbar!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        //HUD.show(true)
        
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        
        self.EventTBL?.registerClass(UITableViewCell.self, forCellReuseIdentifier: self.cellIdentifier)
        EventTBL.tableFooterView = UIView(frame: CGRectZero)
        EventTBL.separatorStyle=UITableViewCellSeparatorStyle.None
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        pickerView=UIPickerView()
        pickerView.dataSource=self
        pickerView.delegate=self
        pickerView.backgroundColor=UIColor.whiteColor()
        EventTypeTXT.inputView=pickerView
        
        pickerToolbar=UIToolbar()
        pickerToolbar.barStyle = UIBarStyle.Default
        pickerToolbar.translucent = true
        pickerToolbar.tintColor = UIColor.blackColor()
        pickerToolbar.barTintColor=UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
        pickerToolbar.sizeToFit()
        
        var doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: "donePicker")
        var CButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        pickerToolbar.setItems([CButton,doneButton], animated: false)
        pickerToolbar.userInteractionEnabled = true
        EventTypeTXT.inputAccessoryView=pickerToolbar
        
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func CreateBTNClick(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("AddNewEventVC") as! AddNewEventVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    
    func donePicker()
    {
        EventTypeTXT.resignFirstResponder()
        CallAPI()
    }
    
    func CallAPI()
    {
        HUD.show(true)
        EventAry.removeAllObjects()
        var TypeString:String!
        if EventTypeTXT.text == "All Events"
        {
            TypeString = "all"
        }
        else if EventTypeTXT.text == "Future Events"
        {
            TypeString = "future"
        }
        else
        {
            TypeString = "past"
        }
        let ValueDict=NSDictionary(objectsAndKeys: "user","sortBy","desc","sortDirection",TypeString,"viewType",25,"limitResult",1,"pageNumber")
        let ParamsDict=NSDictionary(objectsAndKeys:ValueDict,"Value")
        let RequestDict=NSDictionary(objectsAndKeys: "getMyEvents","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Event",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
        }
    }
    
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            var TempAry=NSMutableArray(array: Dict.valueForKey("response")!.valueForKey("result") as! NSArray)
            var DateAry=NSMutableArray()
            for var i=0;i<TempAry.count;i++
            {
                let DateFormatter = NSDateFormatter()
                DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let DateObj = DateFormatter.dateFromString(TempAry.objectAtIndex(i).valueForKey("eventTimestamp") as! String)
                DateFormatter.dateFormat = "yyyy-MM-dd"
                if !DateAry.containsObject(DateFormatter.stringFromDate(DateObj!))
                {
                    DateAry.addObject(DateFormatter.stringFromDate(DateObj!))
                }
            }
            //print(DateAry)
            for var j=0;j<DateAry.count;j++
            {
                var DateWiseDict=NSMutableDictionary()
                var DateWiseAry=NSMutableArray()
                for var k=0;k<TempAry.count;k++
                {
                    let DateFormatter = NSDateFormatter()
                    DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let DateObj = DateFormatter.dateFromString(TempAry.objectAtIndex(k).valueForKey("eventTimestamp") as! String)
                    DateFormatter.dateFormat = "yyyy-MM-dd"
                    
                    if DateAry.objectAtIndex(j).isEqualToString(DateFormatter.stringFromDate(DateObj!))
                    {
                        DateWiseAry.addObject(TempAry.objectAtIndex(k))
                    }
                }
                if DateWiseAry.count > 0
                {
                    DateWiseDict.setValue(DateWiseAry, forKey: "DateAry")
                    EventAry.addObject(DateWiseDict)
                }
            }
            print(EventAry)
            EventTBL.reloadData()
        }
        HUD.hide(true)
    }
    
    /********************************************
    MARK:TableView Delegate
    ********************************************/
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return EventAry.count
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var RowAry=NSMutableArray(array: EventAry.objectAtIndex(section).valueForKey("DateAry") as! NSArray)
        return RowAry.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as! UITableViewCell
        cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: self.cellIdentifier)
        cell.frame=CGRectMake(0, 0, self.view.frame.size.width, 40)
        
        var RowAry=NSMutableArray(array: EventAry.objectAtIndex(indexPath.section).valueForKey("DateAry") as! NSArray)
        let SubBackView=UIView(frame: CGRectMake(10, 10, cell.frame.size.width-20, 60))
        //BackView.backgroundColor=UIColor.whiteColor()
        let LineIMG=UIImageView(frame: CGRectMake(0, 0, 10, 60))
        LineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(LineIMG)
        
        let TitleIcon=UIImageView(frame: CGRectMake(LineIMG.frame.size.width+LineIMG.frame.origin.x+5, 8, 13, 20))
        TitleIcon.image=UIImage(named: "ic_home_location_small.png")
        SubBackView.addSubview(TitleIcon)
        
        let TItleLBL=UILabel(frame: CGRectMake(TitleIcon.frame.size.width+TitleIcon.frame.origin.x+5, 10, SubBackView.frame.size.width-TitleIcon.frame.origin.x-115, 15))
        TItleLBL.text=RowAry.objectAtIndex(indexPath.row).valueForKey("teamName") as? String
        TItleLBL.font=UIFont.boldSystemFontOfSize(12)
        TItleLBL.textColor=UIColor.whiteColor()
//        TItleLBL.layer.borderWidth=1.0
//        TItleLBL.layer.borderColor=UIColor.whiteColor().CGColor
        SubBackView.addSubview(TItleLBL)
        
        
        let WatchIcon=UIImageView(frame: CGRectMake(TItleLBL.frame.size.width+TItleLBL.frame.origin.x+5, 10, 15, 15))
        WatchIcon.image=UIImage(named: "ic_clock_blue.png")
        SubBackView.addSubview(WatchIcon)
        
        let DateFormatter = NSDateFormatter()
        DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let DateObj = DateFormatter.dateFromString(RowAry.objectAtIndex(indexPath.row).valueForKey("eventTimestamp") as! String)
        DateFormatter.dateFormat = "hh:mm a"
        let TimeLBL=UILabel(frame: CGRectMake(WatchIcon.frame.size.width+WatchIcon.frame.origin.x+5, 10, 60, 15))
        TimeLBL.text=DateFormatter.stringFromDate(DateObj!)
        TimeLBL.font=UIFont.systemFontOfSize(12)
        TimeLBL.textColor=UIColor.whiteColor()
        SubBackView.addSubview(TimeLBL)
        
        let LocationIcon=UIImageView(frame: CGRectMake(LineIMG.frame.size.width+LineIMG.frame.origin.x+5, TitleIcon.frame.size.height+TitleIcon.frame.origin.y+5, 13, 20))
        LocationIcon.image=UIImage(named: "ic_home_location_small.png")
        SubBackView.addSubview(LocationIcon)
        
        let LocationLBL=UILabel(frame: CGRectMake(TitleIcon.frame.size.width+TitleIcon.frame.origin.x+5, TitleIcon.frame.size.height+TitleIcon.frame.origin.y+8, SubBackView.frame.size.width-TitleIcon.frame.origin.x-20, 15))
        LocationLBL.text=RowAry.objectAtIndex(indexPath.row).valueForKey("locationName") as? String
        LocationLBL.font=UIFont.boldSystemFontOfSize(12)
        LocationLBL.textColor=UIColor.whiteColor()
//        LocationLBL.layer.borderWidth=1.0
//        LocationLBL.layer.borderColor=UIColor.whiteColor().CGColor
        SubBackView.addSubview(LocationLBL)
        
        //                let LocationIcon=UIImageView(frame: CGRectMake(LineIMG.frame.size.width+LineIMG.frame.origin.x+5, (SubBackView.frame.size.height/2)-10, 13, 20))
        //                LocationIcon.image=UIImage(named: "ic_home_location_small.png")
        //                SubBackView.addSubview(LocationIcon)
        
        let SmallLineIMG=UIImageView(frame: CGRectMake(0, SubBackView.frame.size.height-1, SubBackView.frame.size.width, 1))
        SmallLineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(SmallLineIMG)
        
        let LastIMG=UIImageView(frame: CGRectMake(SubBackView.frame.size.width-20, 5, 20, 20))
        LastIMG.image=UIImage(named: "ic_general_more_details.png")
        SubBackView.addSubview(LastIMG)
        cell.addSubview(SubBackView)
        cell.backgroundColor=UIColor.blackColor()
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        EventTBL.rowHeight = SubBackView.frame.size.height+20
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("EventDetailsVC") as! EventDetailsVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
        
        NSUserDefaults.standardUserDefaults().setInteger(EventAry.objectAtIndex(indexPath.section).valueForKey("DateAry")!.objectAtIndex(indexPath.row).valueForKey("eventId") as! Int, forKey: "EventID")
    }
    
    func tableView(tableView: UITableView!, viewForHeaderInSection section: Int) -> UIView! {
        
        var RowAry=NSMutableArray(array: EventAry.objectAtIndex(section).valueForKey("DateAry") as! NSArray)
        let DateFormatter = NSDateFormatter()
        DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let DateObj = DateFormatter.dateFromString(RowAry.objectAtIndex(0).valueForKey("eventTimestamp") as! String)
        DateFormatter.dateFormat = "EE,MMM dd,yyyy"
        
        let SectionBackView=UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, 20))
        SectionBackView.backgroundColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        
        let iconIMG=UIImageView(frame: CGRectMake(5, 0, 20, 20))
        iconIMG.image=UIImage(named: "ic_calendar_blue.png")
        SectionBackView.addSubview(iconIMG)
        
        let TitleLBL=UILabel(frame: CGRectMake(30, 0, SectionBackView.frame.size.width-40, 20))
        TitleLBL.text = DateFormatter.stringFromDate(DateObj!)
        TitleLBL.textColor=UIColor.whiteColor()
        TitleLBL.font=UIFont.boldSystemFontOfSize(12)
        SectionBackView.addSubview(TitleLBL)
        
        return SectionBackView
    }
    
    /******************************************************
    MARK:PickerView Delegate Method
    ******************************************************/
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row==0
        {
            EventTypeTXT.text = "All Events"
        }
        else if row==1
        {
            EventTypeTXT.text = "Future Events"
        }
        else
        {
            EventTypeTXT.text = "Past Events"
        }
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        
        if row==0
        {
            return "All Events"
        }
        else if row==1
        {
            return "Future Events"
        }
        else
        {
            return "Past Events"
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        CallAPI()
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }

}
