//
//  AttandanceVC.swift
//  GoalieHub
//
//  Created by Vivek on 14/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class AttandanceVC: UIViewController,MBProgressHUDDelegate {

    @IBOutlet var BackView: UIView!
    @IBOutlet var AttandanceTBL: UITableView!
    let cellIdentifier = "cellIdentifier"
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!,EventID:NSNumber!,UserID:Int!
    var AttandanceAry=NSMutableArray()
    var popupView:UIView!
    var AttendanceStr:String!
    override func viewDidLoad() {
        super.viewDidLoad()

        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        self.AttandanceTBL?.registerClass(UITableViewCell.self, forCellReuseIdentifier: self.cellIdentifier)
        AttandanceTBL.tableFooterView = UIView(frame: CGRectZero)
        AttandanceTBL.separatorStyle=UITableViewCellSeparatorStyle.None
        
        EventID = NSUserDefaults.standardUserDefaults().valueForKey("EventID") as! Int
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "AttandanceLoad:", name:"Attandance", object: nil)
        
        let gesture = UITapGestureRecognizer(target: self, action: "PopupAction:")
        BackView.addGestureRecognizer(gesture)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func PopupAction(sender:UITapGestureRecognizer){
        BackView.hidden=true
        popupView.hidden=true
    }
    

    func AttandanceLoad(notification: NSNotification){
        CallAPI()
        //Take Action on Notification
    }

    /********************************************
    MARK:TableView Delegate
    ********************************************/
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return AttandanceAry.count
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var RowAry=NSMutableArray(array: AttandanceAry.objectAtIndex(section).valueForKey("Attending") as! NSArray)
        return RowAry.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as! UITableViewCell
        cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: self.cellIdentifier)
        cell.frame=CGRectMake(0, 0, self.view.frame.size.width, 50)
        var RowAry=NSMutableArray(array: AttandanceAry.objectAtIndex(indexPath.section).valueForKey("Attending") as! NSArray)
        
        let SubBackView=UIView(frame: CGRectMake(10, 10, cell.frame.size.width-20, 45))
        //BackView.backgroundColor=UIColor.whiteColor()
        let LineIMG=UIImageView(frame: CGRectMake(0, 0, 10, 45))
        LineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(LineIMG)
        let decodedData = NSData(base64EncodedString: RowAry.objectAtIndex(indexPath.row).valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
        let TitleIcon=UIImageView(frame: CGRectMake(LineIMG.frame.size.width+LineIMG.frame.origin.x+5, 5, 30, 30))
        TitleIcon.image=UIImage(data: decodedData!)
        TitleIcon.clipsToBounds=true
        TitleIcon.layer.cornerRadius=TitleIcon.frame.size.width/2
        SubBackView.addSubview(TitleIcon)
        
        let TItleLBL=UILabel(frame: CGRectMake(TitleIcon.frame.size.width+TitleIcon.frame.origin.x+5, 5, cell.frame.size.width-50, 30))
        TItleLBL.text=RowAry.objectAtIndex(indexPath.row).valueForKey("userdetFullname") as? String
        TItleLBL.font=UIFont.boldSystemFontOfSize(12)
        TItleLBL.textColor=UIColor.whiteColor()
        SubBackView.addSubview(TItleLBL)
        
        
        //                let LocationIcon=UIImageView(frame: CGRectMake(LineIMG.frame.size.width+LineIMG.frame.origin.x+5, (SubBackView.frame.size.height/2)-10, 13, 20))
        //                LocationIcon.image=UIImage(named: "ic_home_location_small.png")
        //                SubBackView.addSubview(LocationIcon)
        
        let SmallLineIMG=UIImageView(frame: CGRectMake(0, SubBackView.frame.size.height-1, SubBackView.frame.size.width, 1))
        SmallLineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(SmallLineIMG)
        
        let LastIMG=UIImageView(frame: CGRectMake(SubBackView.frame.size.width-20, 5, 20, 20))
        LastIMG.image=UIImage(named: "ic_general_more_details.png")
        SubBackView.addSubview(LastIMG)
        cell.addSubview(SubBackView)
        cell.backgroundColor=UIColor.blackColor()
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        AttandanceTBL.rowHeight = SubBackView.frame.size.height+20
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        BackView.hidden=false
        UserID=AttandanceAry.objectAtIndex(indexPath.section).valueForKey("Attending")!.objectAtIndex(indexPath.row).valueForKey("userId") as! Int
        let PresentDayStr=AttandanceAry.objectAtIndex(indexPath.section).valueForKey("Attending")!.objectAtIndex(indexPath.row).valueForKey("Attendance")!.valueForKey("attendWillattend") as! NSString
        if PresentDayStr.isEqualToString("Y")
        {
            popupView=UIView(frame: CGRectMake(10, (self.view.frame.size.height/2)-20, self.view.frame.size.width-20, 40))
            let NotAttendingBTN=UIButton(frame: CGRectMake(0, 0, popupView.frame.size.width, 35))
            NotAttendingBTN.setTitle("Not Attending", forState: UIControlState.Normal)
            NotAttendingBTN.backgroundColor=UIColor.blackColor()
            NotAttendingBTN.clipsToBounds=true
            NotAttendingBTN.layer.cornerRadius=5.0
            NotAttendingBTN.layer.borderWidth=1.0
            NotAttendingBTN.layer.borderColor=UIColor.whiteColor().CGColor
            NotAttendingBTN.titleLabel!.font=UIFont.boldSystemFontOfSize(15)
            NotAttendingBTN.addTarget(self, action: "NotAttendingBTNClick:", forControlEvents: UIControlEvents.TouchUpInside)
            popupView.addSubview(NotAttendingBTN)
            self.view.addSubview(popupView)
        }
        else if PresentDayStr.isEqualToString("N")
        {
            popupView=UIView(frame: CGRectMake(10, (self.view.frame.size.height/2)-20, self.view.frame.size.width-20, 40))
            let AttendingBTN=UIButton(frame: CGRectMake(0, 0, popupView.frame.size.width, 35))
            AttendingBTN.setTitle("Attending", forState: UIControlState.Normal)
            AttendingBTN.backgroundColor=UIColor.blackColor()
            AttendingBTN.clipsToBounds=true
            AttendingBTN.layer.cornerRadius=5.0
            AttendingBTN.layer.borderWidth=1.0
            AttendingBTN.layer.borderColor=UIColor.whiteColor().CGColor
            AttendingBTN.titleLabel!.font=UIFont.boldSystemFontOfSize(15)
            AttendingBTN.addTarget(self, action: "AttendingBTNClick:", forControlEvents: UIControlEvents.TouchUpInside)
            popupView.addSubview(AttendingBTN)
            self.view.addSubview(popupView)
        }
        else
        {
            popupView=UIView(frame: CGRectMake(10, (self.view.frame.size.height/2)-40, self.view.frame.size.width-20, 80))
            let AttendingBTN=UIButton(frame: CGRectMake(0, 0, popupView.frame.size.width, 35))
            AttendingBTN.setTitle("Attending", forState: UIControlState.Normal)
            AttendingBTN.backgroundColor=UIColor.blackColor()
            AttendingBTN.clipsToBounds=true
            AttendingBTN.layer.cornerRadius=5.0
            AttendingBTN.layer.borderWidth=1.0
            AttendingBTN.layer.borderColor=UIColor.whiteColor().CGColor
            AttendingBTN.titleLabel!.font=UIFont.boldSystemFontOfSize(15)
            AttendingBTN.addTarget(self, action: "AttendingBTNClick:", forControlEvents: UIControlEvents.TouchUpInside)
            popupView.addSubview(AttendingBTN)
            
            
            let NotAttendingBTN=UIButton(frame: CGRectMake(0, AttendingBTN.frame.origin.y+AttendingBTN.frame.size.height, popupView.frame.size.width, 35))
            NotAttendingBTN.setTitle("Not Attending", forState: UIControlState.Normal)
            NotAttendingBTN.backgroundColor=UIColor.blackColor()
            NotAttendingBTN.clipsToBounds=true
            NotAttendingBTN.layer.cornerRadius=5.0
            NotAttendingBTN.layer.borderWidth=1.0
            NotAttendingBTN.layer.borderColor=UIColor.whiteColor().CGColor
            NotAttendingBTN.titleLabel!.font=UIFont.boldSystemFontOfSize(15)
            NotAttendingBTN.addTarget(self, action: "NotAttendingBTNClick:", forControlEvents: UIControlEvents.TouchUpInside)
            popupView.addSubview(NotAttendingBTN)
            
            self.view.addSubview(popupView)
        }
        
    }
    
    func tableView(tableView: UITableView!, viewForHeaderInSection section: Int) -> UIView! {
        
        let SectionBackView=UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, 20))
        SectionBackView.backgroundColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        
        let iconIMG=UIImageView(frame: CGRectMake(5, 0, 20, 20))
        SectionBackView.addSubview(iconIMG)
        
        let TitleLBL=UILabel(frame: CGRectMake(30, 0, SectionBackView.frame.size.width-40, 20))
        if section==0
        {
            TitleLBL.text = "Attending"
            iconIMG.image=UIImage(named: "ic_my_attendance_right_green.png")
        }
        else if section==1
        {
            TitleLBL.text = "Not Attending"
            iconIMG.image=UIImage(named: "ic_stop.png")
        }
        else
        {
            TitleLBL.text = "Unknown"
            //iconIMG.image=UIImage(named: "")
        }
        
        TitleLBL.textColor=UIColor.whiteColor()
        TitleLBL.font=UIFont.boldSystemFontOfSize(12)
        SectionBackView.addSubview(TitleLBL)
        
        return SectionBackView
    }
    
    
    @IBAction func ResendBTNClick(sender: UIButton) {
        HUD.show(true)
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        let IdentiDict=NSDictionary(objectsAndKeys: EventID,"eventId")
        let ParamsDict=NSDictionary(objectsAndKeys:IdentiDict,"Identifier")
        let RequestDict=NSDictionary(objectsAndKeys: "requestEventAttendance","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Event",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse3(response.3, Dict: response.2 as? NSDictionary)
        }
    }
    
    func AttendingBTNClick(sender:UIButton!)
    {
        AttendanceStr="Y"
        CallAttandanceAPI()
    }
    func NotAttendingBTNClick(sender:UIButton!)
    {
        AttendanceStr="N"
        CallAttandanceAPI()
    }
    func CallAttandanceAPI()
    {
        BackView.hidden=true
        popupView.hidden=true
        HUD.show(true)
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        let ValueDict=NSDictionary(objectsAndKeys: AttendanceStr,"willattend")
        let IdentiDict=NSDictionary(objectsAndKeys: EventID,"eventId",UserID,"userId")
        let ParamsDict=NSDictionary(objectsAndKeys:IdentiDict,"Identifier",ValueDict,"Value")
        let RequestDict=NSDictionary(objectsAndKeys: "setUserAttendanceForEvent","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Event",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse2(response.3, Dict: response.2 as? NSDictionary)
        }
    }
    func CallAPI()
    {
        AttandanceAry.removeAllObjects()
        HUD.show(true)
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        let IdentiDict=NSDictionary(objectsAndKeys: EventID,"eventId")
        let ParamsDict=NSDictionary(objectsAndKeys:IdentiDict,"Identifier")
        let RequestDict=NSDictionary(objectsAndKeys: "getEventAttendance","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Event",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse1(response.3, Dict: response.2 as? NSDictionary)
        }
    }
    
    func HandleAPIResponse1(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            if Dict.valueForKey("response")!.valueForKey("result")?.count > 0
            {
                var Attandance=NSMutableArray()
                if let CheckContent=Dict.valueForKey("response")!.valueForKey("result") as? NSDictionary
                {
                    Attandance.addObject(Dict.valueForKey("response")!.valueForKey("result")!)
                }
                else
                {
                    Attandance=Dict.valueForKey("response")!.valueForKey("result") as! NSMutableArray
                }
                var AttendAry=NSMutableArray()
                var NotAttendAry=NSMutableArray()
                var UnknownAry=NSMutableArray()
                for var i=0;i<Attandance.count;i++
                {
                    if Attandance.objectAtIndex(i).valueForKey("Attendance")!.valueForKey("attendWillattend")!.isEqualToString("U")
                    {
                        UnknownAry.addObject(Attandance.objectAtIndex(i))
                    }
                    else if Attandance.objectAtIndex(i).valueForKey("Attendance")!.valueForKey("attendWillattend")!.isEqualToString("N")
                    {
                        NotAttendAry.addObject(Attandance.objectAtIndex(i))
                    }
                    else if Attandance.objectAtIndex(i).valueForKey("Attendance")!.valueForKey("attendWillattend")!.isEqualToString("Y")
                    {
                        AttendAry.addObject(Attandance.objectAtIndex(i))
                    }
                }
                
                for var j=0;j<3;j++
                {
                    var NameDict=NSMutableDictionary()
                    if j==0
                    {
                        NameDict.setValue(AttendAry, forKey: "Attending")
                    }
                    else if j==1
                    {
                        NameDict.setValue(NotAttendAry, forKey: "Attending")
                    }
                    else
                    {
                        NameDict.setValue(UnknownAry, forKey: "Attending")
                    }
                    AttandanceAry.addObject(NameDict)
                }
                //print(AttandanceAry)
                AttandanceTBL.reloadData()
            }
            
            HUD.hide(true)
        }
    }
    func HandleAPIResponse2(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            CallAPI()
            HUD.hide(true)
        }
    }
    func HandleAPIResponse3(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            HUD.hide(true)
        }
    }
}
