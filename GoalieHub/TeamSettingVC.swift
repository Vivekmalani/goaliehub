//
//  TeamSettingVC.swift
//  GoalieHub
//
//  Created by Vivek on 21/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class TeamSettingVC: UIViewController,MBProgressHUDDelegate{
    @IBOutlet var EditBTN: UIButton!
    @IBOutlet var BackView: UIView!
    @IBOutlet var BarBTN: UIBarButtonItem!
    @IBOutlet var TeamNameLBL: UILabel!
    @IBOutlet var TeamIMG: UIImageView!
    @IBOutlet var BackScroll: UIScrollView!
    var TeamDetailDict=NSMutableDictionary(),MDict=NSMutableDictionary()
    var ID:NSNumber!,KEY:NSNumber!
    var HUD:MBProgressHUD!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        EditBTN.clipsToBounds=true
        EditBTN.layer.cornerRadius=5.0
        EditBTN.layer.borderWidth=1.0
        EditBTN.layer.borderColor=UIColor.whiteColor().CGColor
        
        print(TeamDetailDict)
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        TeamIMG.clipsToBounds=true
        TeamIMG.layer.cornerRadius=TeamIMG.frame.size.width/2
        let decodedData = NSData(base64EncodedString: TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
        TeamIMG.image=UIImage(data:decodedData!)
        TeamNameLBL.text=TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
        
        HUD.show(true)
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        let IdentifDict=NSDictionary(objectsAndKeys: TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("teamId") as! Int,"teamId")
        let ParamsDict=NSDictionary(objectsAndKeys:IdentifDict,"Identifier")
        let RequestDict=NSDictionary(objectsAndKeys: "getTeamDetails","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Team",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse1(response.3, Dict: response.2 as? NSDictionary)
        }
        
        
        let gesture = UITapGestureRecognizer(target: self, action: "PopupAction:")
        BackView.addGestureRecognizer(gesture)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func PopupAction(sender:UITapGestureRecognizer){
        BackView.hidden=true
        EditBTN.hidden=true
    }
    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func EditBTNClick(sender: UIButton) {
        BackView.hidden=true
        EditBTN.hidden=true
        
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("CreateTeamVC") as! CreateTeamVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
        initialViewController.CheckEditOption=1
        initialViewController.TeamDetailDict=TeamDetailDict
        initialViewController.TeamDetailsVaDict=MDict
    }
    
    func HandleAPIResponse1(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            
            MDict=NSMutableDictionary(dictionary: Dict)
            
            let BackView=UIView(frame: CGRectMake(5, 5, BackScroll.frame.size.width-10, 220))
            BackView.clipsToBounds=true
            BackView.layer.cornerRadius=5.0
            BackView.layer.borderWidth=1.0
            BackView.layer.borderColor=UIColor.whiteColor().CGColor
            
            let TitleLBL=UILabel(frame: CGRectMake(5, 5, BackView.frame.size.width-10, 20))
            TitleLBL.backgroundColor=UIColor.lightGrayColor()
            TitleLBL.text="Team Settings"
            TitleLBL.font=UIFont.boldSystemFontOfSize(15)
            TitleLBL.textColor=UIColor.whiteColor()
            TitleLBL.textAlignment=NSTextAlignment.Center
            BackView.addSubview(TitleLBL)
            
            let NameLBL=UILabel(frame: CGRectMake(25, TitleLBL.frame.origin.y+TitleLBL.frame.size.height+10, 70, 15))
            NameLBL.text="Name:"
            NameLBL.font=UIFont.systemFontOfSize(12)
            NameLBL.textColor=UIColor.whiteColor()
            NameLBL.textAlignment=NSTextAlignment.Right
            BackView.addSubview(NameLBL)
            
            let NameVLBL=UILabel(frame: CGRectMake(NameLBL.frame.size.width+NameLBL.frame.origin.x+5, TitleLBL.frame.origin.y+TitleLBL.frame.size.height+10, BackView.frame.size.width-NameLBL.frame.size.width-50, 15))
            NameVLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("teamName") as? String
            NameVLBL.font=UIFont.boldSystemFontOfSize(12)
            NameVLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            BackView.addSubview(NameVLBL)
            
            let SkillLBL=UILabel(frame: CGRectMake(25, NameVLBL.frame.origin.y+NameVLBL.frame.size.height+5, 70, 15))
            SkillLBL.text="Skill Level:"
            SkillLBL.font=UIFont.systemFontOfSize(12)
            SkillLBL.textColor=UIColor.whiteColor()
            SkillLBL.textAlignment=NSTextAlignment.Right
            BackView.addSubview(SkillLBL)
            
            let SkillVLBL=UILabel(frame: CGRectMake(SkillLBL.frame.size.width+SkillLBL.frame.origin.x+5, NameVLBL.frame.origin.y+NameVLBL.frame.size.height+5, BackView.frame.size.width-SkillLBL.frame.size.width-50, 15))
            SkillVLBL.text=""
            SkillVLBL.font=UIFont.boldSystemFontOfSize(12)
            SkillVLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            BackView.addSubview(SkillVLBL)
            
            let AgeLBL=UILabel(frame: CGRectMake(25, SkillVLBL.frame.origin.y+SkillVLBL.frame.size.height+5, 70, 15))
            AgeLBL.text="Age:"
            AgeLBL.font=UIFont.systemFontOfSize(12)
            AgeLBL.textColor=UIColor.whiteColor()
            AgeLBL.textAlignment=NSTextAlignment.Right
            BackView.addSubview(AgeLBL)
            
            let AgeVLBL=UILabel(frame: CGRectMake(AgeLBL.frame.size.width+AgeLBL.frame.origin.x+5, SkillVLBL.frame.origin.y+SkillVLBL.frame.size.height+5, BackView.frame.size.width-AgeLBL.frame.size.width-50, 15))
            AgeVLBL.text="Adult(18+)"
            AgeVLBL.font=UIFont.boldSystemFontOfSize(12)
            AgeVLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            BackView.addSubview(AgeVLBL)
            
            let SexLBL=UILabel(frame: CGRectMake(25, AgeVLBL.frame.origin.y+AgeVLBL.frame.size.height+5, 70, 15))
            SexLBL.text="Sex:"
            SexLBL.font=UIFont.systemFontOfSize(12)
            SexLBL.textColor=UIColor.whiteColor()
            SexLBL.textAlignment=NSTextAlignment.Right
//            SexLBL.layer.borderWidth=1.0
//            SexLBL.layer.borderColor=UIColor.whiteColor().CGColor
            BackView.addSubview(SexLBL)
            
            let SexVLBL=UILabel(frame: CGRectMake(SexLBL.frame.size.width+SexLBL.frame.origin.x+5, AgeVLBL.frame.origin.y+AgeVLBL.frame.size.height+5, BackView.frame.size.width-SexLBL.frame.size.width-50, 15))
            if Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("teamSex")!.isEqualToString("M")
            {
                SexVLBL.text="Male Only"
            }
            else
            {
                SexVLBL.text="Female Only"
            }
            
            SexVLBL.font=UIFont.boldSystemFontOfSize(12)
            SexVLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            BackView.addSubview(SexVLBL)
            
            let LineIMG=UIImageView(frame: CGRectMake(10, TitleLBL.frame.origin.y+TitleLBL.frame.size.height+5, 10, SexLBL.frame.size.height+SexLBL.frame.origin.y-15))
            LineIMG.backgroundColor=UIColor.whiteColor()
            BackView.addSubview(LineIMG)
            
            let SmallLineIMG=UIImageView(frame: CGRectMake(10, LineIMG.frame.size.height+LineIMG.frame.origin.y, BackView.frame.size.width-20, 1))
            SmallLineIMG.backgroundColor=UIColor.whiteColor()
            BackView.addSubview(SmallLineIMG)
            
            let LastIMG=UIImageView(frame: CGRectMake(BackView.frame.size.width-20, TitleLBL.frame.origin.y+TitleLBL.frame.size.height+10, 20, 20))
            LastIMG.image=UIImage(named: "ic_general_more_details.png")
            BackView.addSubview(LastIMG)
            
            let TypeLBL=UILabel(frame: CGRectMake(25, SmallLineIMG.frame.origin.y+SmallLineIMG.frame.size.height+15, 80, 15))
            TypeLBL.text="Team Type:"
            TypeLBL.font=UIFont.systemFontOfSize(12)
            TypeLBL.textColor=UIColor.whiteColor()
            TypeLBL.textAlignment=NSTextAlignment.Right
            BackView.addSubview(TypeLBL)
            
            let TypeVLBL=UILabel(frame: CGRectMake(TypeLBL.frame.size.width+TypeLBL.frame.origin.x+5, SmallLineIMG.frame.origin.y+SmallLineIMG.frame.size.height+15, BackView.frame.size.width-NameLBL.frame.size.width-50, 15))
            
            if let CheckNull = Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("League")!.valueForKey("leagueDesc") as? NSNull
            {
                TypeVLBL.text=""
            }
            else
            {
                 TypeVLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("League")!.valueForKey("leagueCode") as? String
            }
            TypeVLBL.font=UIFont.boldSystemFontOfSize(12)
            TypeVLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            BackView.addSubview(TypeVLBL)
            
            let CodeLBL=UILabel(frame: CGRectMake(25, TypeLBL.frame.origin.y+TypeLBL.frame.size.height+5, 80, 15))
            CodeLBL.text="League Code:"
            CodeLBL.font=UIFont.systemFontOfSize(12)
            CodeLBL.textColor=UIColor.whiteColor()
            CodeLBL.textAlignment=NSTextAlignment.Right
            BackView.addSubview(CodeLBL)
            
            let CodeVLBL=UILabel(frame: CGRectMake(CodeLBL.frame.size.width+CodeLBL.frame.origin.x+5, TypeLBL.frame.origin.y+TypeLBL.frame.size.height+5, BackView.frame.size.width-SkillLBL.frame.size.width-50, 15))
            if let CheckNull = Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("League")!.valueForKey("leagueCode") as? NSNull
            {
                CodeVLBL.text=""
            }
            else
            {
                CodeVLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("League")!.valueForKey("leagueCode") as? String
            }
            CodeVLBL.font=UIFont.boldSystemFontOfSize(12)
            CodeVLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            BackView.addSubview(CodeVLBL)
            
            let LegueLBL=UILabel(frame: CGRectMake(25, CodeLBL.frame.origin.y+CodeLBL.frame.size.height+5, 80, 15))
            LegueLBL.text="Leadue:"
            LegueLBL.font=UIFont.systemFontOfSize(12)
            LegueLBL.textColor=UIColor.whiteColor()
            LegueLBL.textAlignment=NSTextAlignment.Right
            BackView.addSubview(LegueLBL)
            
            let LegueVLBL=UILabel(frame: CGRectMake(LegueLBL.frame.size.width+LegueLBL.frame.origin.x+5, CodeLBL.frame.origin.y+CodeLBL.frame.size.height+5, BackView.frame.size.width-AgeLBL.frame.size.width-50, 15))
            LegueVLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("League")!.valueForKey("leagueName") as? String
            LegueVLBL.font=UIFont.boldSystemFontOfSize(12)
            LegueVLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            BackView.addSubview(LegueVLBL)
            
            let LineIMG2=UIImageView(frame: CGRectMake(10, SmallLineIMG.frame.origin.y+SmallLineIMG.frame.size.height+10, 10, 70))
            LineIMG2.backgroundColor=UIColor.whiteColor()
            BackView.addSubview(LineIMG2)
            
            let SmallLineIMG2=UIImageView(frame: CGRectMake(10, LineIMG2.frame.size.height+LineIMG2.frame.origin.y, BackView.frame.size.width-20, 1))
            SmallLineIMG2.backgroundColor=UIColor.whiteColor()
            BackView.addSubview(SmallLineIMG2)
            
            let LastIMG2=UIImageView(frame: CGRectMake(BackView.frame.size.width-20, SmallLineIMG.frame.origin.y+SmallLineIMG.frame.size.height+10, 20, 20))
            LastIMG2.image=UIImage(named: "ic_general_more_details.png")
            BackView.addSubview(LastIMG2)
            
            let BackGBTN=UIButton(frame: CGRectMake(0, 0, BackView.frame.size.width, BackView.frame.size.height))
            BackGBTN.addTarget(self, action: "BackGBTNClick:", forControlEvents: UIControlEvents.TouchUpInside)
            BackView.addSubview(BackGBTN)
            
            BackScroll.addSubview(BackView)
            
            ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
            KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
            let IdentifDict=NSDictionary(objectsAndKeys: TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("teamId") as! Int,"teamId")
            let ParamsDict=NSDictionary(objectsAndKeys:IdentifDict,"Identifier")
            let RequestDict=NSDictionary(objectsAndKeys: "getTeamAutoAttendanceSettings","method","1","id",ParamsDict,"params")
            print(RequestDict)
            let param = [
                "id"    : ID,
                "key"    : KEY,
                "callBack"    : "myCallBackMethod",
                "serviceName"    : "Team",
                "request" : RequestDict
            ]
            println(param)
            Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
                .responseJSON {response in
                    self.HandleAPIResponse2(response.3, Dict: response.2 as? NSDictionary)
            }
            
        }
        
    }
    
    func HandleAPIResponse2(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            
            if let checkNull=Dict.valueForKey("response")!.valueForKey("result") as? NSNull
            {
                
            }
            else
            {
                let BackView=UIView(frame: CGRectMake(5, 230, BackScroll.frame.size.width-10, 90))
                BackView.clipsToBounds=true
                BackView.layer.cornerRadius=5.0
                BackView.layer.borderWidth=1.0
                BackView.layer.borderColor=UIColor.whiteColor().CGColor
                
                let TitleLBL=UILabel(frame: CGRectMake(5, 5, BackView.frame.size.width-10, 20))
                TitleLBL.backgroundColor=UIColor.lightGrayColor()
                TitleLBL.text="Attendance Settings"
                TitleLBL.font=UIFont.boldSystemFontOfSize(15)
                TitleLBL.textColor=UIColor.whiteColor()
                TitleLBL.textAlignment=NSTextAlignment.Center
                BackView.addSubview(TitleLBL)
                
                let NameLBL=UILabel(frame: CGRectMake(25, TitleLBL.frame.origin.y+TitleLBL.frame.size.height+10, 120, 15))
                NameLBL.text="Send Notification:"
                NameLBL.font=UIFont.systemFontOfSize(12)
                NameLBL.textColor=UIColor.whiteColor()
                NameLBL.textAlignment=NSTextAlignment.Right
                BackView.addSubview(NameLBL)
                
                let NameVLBL=UILabel(frame: CGRectMake(NameLBL.frame.size.width+NameLBL.frame.origin.x+5, TitleLBL.frame.origin.y+TitleLBL.frame.size.height+10, BackView.frame.size.width-NameLBL.frame.size.width-50, 15))
                
                if Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("teamattsetSendnotify")!.isEqualToString("Y")
                {
                    NameVLBL.text="ON"
                }
                else
                {
                    NameVLBL.text="OFF"
                }
                NameVLBL.font=UIFont.boldSystemFontOfSize(12)
                NameVLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                BackView.addSubview(NameVLBL)
                
                let SkillLBL=UILabel(frame: CGRectMake(25, NameVLBL.frame.origin.y+NameVLBL.frame.size.height+5, 120, 15))
                SkillLBL.text="Days Prior To Event:"
                SkillLBL.font=UIFont.systemFontOfSize(12)
                SkillLBL.textColor=UIColor.whiteColor()
                SkillLBL.textAlignment=NSTextAlignment.Right
                BackView.addSubview(SkillLBL)
                
                let SkillVLBL=UILabel(frame: CGRectMake(SkillLBL.frame.size.width+SkillLBL.frame.origin.x+5, NameVLBL.frame.origin.y+NameVLBL.frame.size.height+5, BackView.frame.size.width-SkillLBL.frame.size.width-50, 15))
                SkillVLBL.text=NSString(format: "%i days", Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("teamattsetDaysbefore") as! Int) as String
                SkillVLBL.font=UIFont.boldSystemFontOfSize(12)
                SkillVLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                BackView.addSubview(SkillVLBL)
                
                let LineIMG=UIImageView(frame: CGRectMake(10, TitleLBL.frame.origin.y+TitleLBL.frame.size.height+5, 10, 50))
                LineIMG.backgroundColor=UIColor.whiteColor()
                BackView.addSubview(LineIMG)
                
                let SmallLineIMG=UIImageView(frame: CGRectMake(10, LineIMG.frame.size.height+LineIMG.frame.origin.y, BackView.frame.size.width-20, 1))
                SmallLineIMG.backgroundColor=UIColor.whiteColor()
                BackView.addSubview(SmallLineIMG)
                
                let LastIMG=UIImageView(frame: CGRectMake(BackView.frame.size.width-20, TitleLBL.frame.origin.y+TitleLBL.frame.size.height+10, 20, 20))
                LastIMG.image=UIImage(named: "ic_general_more_details.png")
                BackView.addSubview(LastIMG)
                
                let BackGBTN=UIButton(frame: CGRectMake(0, 0, BackView.frame.size.width, BackView.frame.size.height))
                BackGBTN.addTarget(self, action: "BackGBTNClick:", forControlEvents: UIControlEvents.TouchUpInside)
                BackView.addSubview(BackGBTN)
                
                BackScroll.addSubview(BackView)
            }
        }
        HUD.hide(true)
    }
    
    func BackGBTNClick(sender:UIButton!)
    {
        BackView.hidden=false
        EditBTN.hidden=false
    }
    
    override func viewWillAppear(animated: Bool) {
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }

}
