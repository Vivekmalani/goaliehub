//
//  PublicPostingVC.swift
//  GoalieHub
//
//  Created by Vivek on 23/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class PublicPostingVC: UIViewController,MBProgressHUDDelegate {
    @IBOutlet var BarBTN: UIBarButtonItem!
    @IBOutlet var AgeBTN: UIButton!
    @IBOutlet var SexBTN: UIButton!
    @IBOutlet var SkillBTN: UIButton!
    @IBOutlet var DateLBL: UILabel!
    @IBOutlet var LocationLBL: UILabel!
    @IBOutlet var TeamNameLBL: UILabel!
    @IBOutlet var TeamIMG: UIImageView!
    @IBOutlet var RateLBL: UILabel!
    @IBOutlet var NoteLBL: UILabel!
    
    @IBOutlet var BackScroll: UIScrollView!
    @IBOutlet var UpdateBTN: UIButton!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!
    override func viewDidLoad() {
        super.viewDidLoad()

        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        HUD.show(true)
        
        BackScroll.contentSize=CGSizeMake(0, UpdateBTN.frame.size.height+UpdateBTN.frame.origin.y+10)
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        
        NoteLBL.text="Note:\nYou can only adjust these settings only while there are no ACTIVE pending request.\nClick for more info..."
        
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        let IdentiDict=NSDictionary(objectsAndKeys: TeamID,"teamId")
        let ParamsDict=NSDictionary(objectsAndKeys: IdentiDict,"Identifier")
        let RequestDict=NSDictionary(objectsAndKeys: "getTeamDetails","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Team",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse1(response.3, Dict: response.2 as? NSDictionary)
        }
        
        // Do any additional setup after loading the view.
    }

    func HandleAPIResponse1(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            let decodedData = NSData(base64EncodedString: TeamIMGStr, options: NSDataBase64DecodingOptions(rawValue: 0))
            TeamIMG.image=UIImage(data: decodedData!)
            TeamNameLBL.text=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("teamName") as? String
            
            print(EventDetailsDict)
            
            ID=NSUserDefaults.standardUserDefaults().valueForKey("ID") as! NSNumber
            KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY") as! NSNumber
            let IdentiDict=NSDictionary(objectsAndKeys: EventDetailsDict.valueForKey("eventId") as! Int,"eventId",1,"sportposId")
            let ParamsDict=NSDictionary(objectsAndKeys: IdentiDict,"Identifier")
            let RequestDict=NSDictionary(objectsAndKeys: "getPostingDetails","method","1","id",ParamsDict,"params")
            print(RequestDict)
            let param = [
                "id"    : ID,
                "key"    : KEY,
                "callBack"    : "myCallBackMethod",
                "serviceName"    : "Posting",
                "request" : RequestDict
            ]
            println(param)
            Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON).responseJSON {response in
                self.HandleAPIResponse2(response.3, Dict: response.2 as? NSDictionary)
            }
            
        }
        HUD.hide(true)
    }
    func HandleAPIResponse2(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
