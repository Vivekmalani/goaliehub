//
//  SearchVC.swift
//  GoalieHub
//
//  Created by Vivek on 08/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class SearchVC: UIViewController,MBProgressHUDDelegate {
    @IBOutlet var BarBTN: UIBarButtonItem!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!
    var TeamAry=NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.tabBarController?.selectedIndex=2
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SeachPlayerBTNClick(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("FindPlayerVC") as! FindPlayerVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
        initialViewController.TeamAry=TeamAry
    }
    func HandleAPIResponse1(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            
            ID=NSUserDefaults.standardUserDefaults().valueForKey("ID") as! NSNumber
            KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY") as! NSNumber
            let ValueDict=NSDictionary(objectsAndKeys: "all","active")
            let ParamsDict=NSDictionary(objectsAndKeys: ValueDict,"Value")
            let RequestDict=NSDictionary(objectsAndKeys: "getMyTeams","method","1","id",ParamsDict,"params")
            print(RequestDict)
            let param = [
                "id"    : ID,
                "key"    : KEY,
                "callBack"    : "myCallBackMethod",
                "serviceName"    : "Team",
                "request" : RequestDict
            ]
            println(param)
            Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
                .responseJSON {response in
                    self.HandleAPIResponse2(response.3, Dict: response.2 as? NSDictionary)
            }
            
        }
        HUD.hide(true)
    }
    func HandleAPIResponse2(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            
            var TempAry=NSMutableArray()
            
            if let CheckDict = Dict.valueForKey("response")!.valueForKey("result") as? NSDictionary
            {
                TempAry.addObject(CheckDict)
            }
            else if let CheckAry = Dict.valueForKey("response")!.valueForKey("result") as? NSArray
            {
                TempAry=NSMutableArray(array: CheckAry)
            }
            
            for var i=0;i<TempAry.count;i++
            {
                let UserID=NSUserDefaults.standardUserDefaults().valueForKey("UserID")!.integerValue
                
                if TempAry.objectAtIndex(i).valueForKey("teamuserAddedby")!.integerValue == UserID
                {
                    TeamAry.addObject(TempAry.objectAtIndex(i))
                }
            }
        }
        HUD.hide(true)
    }
    override func viewWillAppear(animated: Bool) {
        TeamAry.removeAllObjects()
        HUD.show(true)
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID") as! NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY") as! NSNumber
        
        let ParamsDict=NSDictionary(objectsAndKeys: 102,"agentId",-1,"locId")
        let RequestDict=NSDictionary(objectsAndKeys: "getMyAgentSettings","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Agent",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse1(response.3, Dict: response.2 as? NSDictionary)
        }
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }

}
