//
//  AddLocationVC.swift
//  GoalieHub
//
//  Created by Vivek on 05/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit

class AddLocationVC: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate {

    @IBOutlet var LdBTN: UIButton!
    @IBOutlet var BackScroll: UIScrollView!
    @IBOutlet var SpBTN: UIButton!
    @IBOutlet var LdTXT2: UITextField!
    @IBOutlet var LdTXT1: UITextField!
    @IBOutlet var LnpwTXT: UITextField!
    @IBOutlet var LnpwBTN: UIButton!
    @IBOutlet var LnmwBTN: UIButton!
    @IBOutlet var Radio3: UIImageView!
    @IBOutlet var Radio2: UIImageView!
    @IBOutlet var Radio1: UIImageView!
    @IBOutlet var View3: UIView!
    @IBOutlet var View2: UIView!
    @IBOutlet var View1: UIView!
    @IBOutlet var BarBTN: UIBarButtonItem!
    
    var PickerValue=NSMutableArray()
    var pickerView:UIPickerView!
    var pickerToolbar:UIToolbar!
    var animateDistance: CGFloat = 0.0
    var detectPicker:Int!,Distance:Int!
    var CityNameStr:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        
        let borderLnpwTXT = CALayer()
        let widthLnpwTXT = CGFloat(1.0)
        borderLnpwTXT.borderColor = UIColor.whiteColor().CGColor
        borderLnpwTXT.frame = CGRect(x: 0, y: LnpwTXT.frame.size.height - widthLnpwTXT, width:  LnpwTXT.frame.size.width, height: LnpwTXT.frame.size.height)
        borderLnpwTXT.borderWidth = widthLnpwTXT
        LnpwTXT.layer.addSublayer(borderLnpwTXT)
        LnpwTXT.layer.masksToBounds = true
        LnpwTXT.attributedPlaceholder = NSAttributedString(string:"Postal/Zip",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        
        let borderLdTXT1 = CALayer()
        let widthLdTXT1 = CGFloat(1.0)
        borderLdTXT1.borderColor = UIColor.whiteColor().CGColor
        borderLdTXT1.frame = CGRect(x: 0, y: LdTXT1.frame.size.height - widthLdTXT1, width:  LdTXT1.frame.size.width, height: LdTXT1.frame.size.height)
        borderLdTXT1.borderWidth = widthLdTXT1
        LdTXT1.layer.addSublayer(borderLdTXT1)
        LdTXT1.layer.masksToBounds = true
        LdTXT1.attributedPlaceholder = NSAttributedString(string:"Name",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        
        
        let borderLdTXT2 = CALayer()
        let widthLdTXT2 = CGFloat(1.0)
        borderLdTXT2.borderColor = UIColor.whiteColor().CGColor
        borderLdTXT2.frame = CGRect(x: 0, y: LdTXT2.frame.size.height - widthLdTXT2, width:  LdTXT2.frame.size.width, height: LdTXT2.frame.size.height)
        borderLdTXT2.borderWidth = widthLdTXT2
        LdTXT2.layer.addSublayer(borderLdTXT2)
        LdTXT2.layer.masksToBounds = true
        
        LdTXT2.attributedPlaceholder = NSAttributedString(string:"City",
            attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        
        pickerView=UIPickerView(frame: CGRectMake(0, self.view.frame.size.height-215, self.view.frame.size.width, 150))
        pickerView.dataSource=self
        pickerView.delegate=self
        pickerView.backgroundColor=UIColor.whiteColor()
        self.view.addSubview(pickerView)
        pickerToolbar=UIToolbar(frame: CGRectMake(0, pickerView.frame.origin.y-35, self.view.frame.size.width, 35))
        pickerToolbar.barStyle = UIBarStyle.Default
        pickerToolbar.translucent = true
        pickerToolbar.tintColor = UIColor.blackColor()
        pickerToolbar.barTintColor=UIColor(red: 97.0/255.0, green: 97.0/255.0, blue: 97.0/255.0, alpha: 1.0)
        //pickerToolbar.sizeToFit()
        
        var doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: "donePicker")
        var CButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        pickerToolbar.setItems([CButton,doneButton], animated: false)
        pickerToolbar.userInteractionEnabled = true
        self.view.addSubview(pickerToolbar)
        pickerView.hidden=true
        pickerToolbar.hidden=true
        
        // Do any additional setup after loading the view.
    }

    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func SearchBTNClick(sender: UIButton) {
        
        LdTXT2.resignFirstResponder()
        LdTXT1.resignFirstResponder()
        LnpwTXT.resignFirstResponder()
        
        if LdTXT2.text.isEmpty==false
        {
            Distance=nil
            CityNameStr=LdTXT2.text
        }
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("SaveLocationVC") as! SaveLocationVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
        initialViewController.Distance=Distance
        initialViewController.CityNameStr=CityNameStr
    }
    
    @IBAction func LnmwBTN(sender: UIButton) {
        LdTXT2.resignFirstResponder()
        LdTXT1.resignFirstResponder()
        LnpwTXT.resignFirstResponder()
        Radio1.image=UIImage(named: "ic_radio_checked.png")
        Radio2.image=UIImage(named: "ic_radio_un_checked.png")
        Radio3.image=UIImage(named: "ic_radio_un_checked.png")
        View1.backgroundColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        View2.backgroundColor=UIColor.clearColor()
        View3.backgroundColor=UIColor.clearColor()
        
        PickerValue.removeAllObjects()
        PickerValue=NSMutableArray(objects: "10","15","20","25")
        detectPicker=1
        OpenPicker()
        Distance=10
        CityNameStr=nil
        LdTXT2.text=""
    }
    @IBAction func LnmwBackBTN(sender: UIButton) {
        LdTXT2.resignFirstResponder()
        LdTXT1.resignFirstResponder()
        LnpwTXT.resignFirstResponder()
        Radio1.image=UIImage(named: "ic_radio_checked.png")
        Radio2.image=UIImage(named: "ic_radio_un_checked.png")
        Radio3.image=UIImage(named: "ic_radio_un_checked.png")
        View1.backgroundColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        View2.backgroundColor=UIColor.clearColor()
        View3.backgroundColor=UIColor.clearColor()
        
        Distance=10
        CityNameStr=nil
        LdTXT2.text=""
        
    }
    @IBAction func LnpwBTNClick(sender: UIButton) {
        LdTXT2.resignFirstResponder()
        LdTXT1.resignFirstResponder()
        LnpwTXT.resignFirstResponder()
        Radio1.image=UIImage(named: "ic_radio_un_checked.png")
        Radio2.image=UIImage(named: "ic_radio_checked.png")
        Radio3.image=UIImage(named: "ic_radio_un_checked.png")
        View1.backgroundColor=UIColor.clearColor()
        View2.backgroundColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        View3.backgroundColor=UIColor.clearColor()
        
        Distance=10
        CityNameStr=nil
        LdTXT2.text=""
        
    }
    @IBAction func LdBackBTNClick(sender: UIButton) {
        LdTXT2.resignFirstResponder()
        LdTXT1.resignFirstResponder()
        LnpwTXT.resignFirstResponder()
        Radio1.image=UIImage(named: "ic_radio_un_checked.png")
        Radio2.image=UIImage(named: "ic_radio_un_checked.png")
        Radio3.image=UIImage(named: "ic_radio_checked.png")
        View1.backgroundColor=UIColor.clearColor()
        View2.backgroundColor=UIColor.clearColor()
        View3.backgroundColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        Distance=nil
    }
    @IBAction func LnpwBTN(sender: UIButton) {
        LdTXT2.resignFirstResponder()
        LdTXT1.resignFirstResponder()
        LnpwTXT.resignFirstResponder()
        Radio1.image=UIImage(named: "ic_radio_un_checked.png")
        Radio2.image=UIImage(named: "ic_radio_checked.png")
        Radio3.image=UIImage(named: "ic_radio_un_checked.png")
        View1.backgroundColor=UIColor.clearColor()
        View2.backgroundColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        View3.backgroundColor=UIColor.clearColor()
        
        PickerValue.removeAllObjects()
        PickerValue=NSMutableArray(objects: "10","15","20","25")
        detectPicker=2
        OpenPicker()
        Distance=10
        CityNameStr=nil
        LdTXT2.text=""
    }
    
    @IBAction func LdBTN(sender: UIButton) {
        
        Radio1.image=UIImage(named: "ic_radio_un_checked.png")
        Radio2.image=UIImage(named: "ic_radio_un_checked.png")
        Radio3.image=UIImage(named: "ic_radio_checked.png")
        View1.backgroundColor=UIColor.clearColor()
        View2.backgroundColor=UIColor.clearColor()
        View3.backgroundColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        
        PickerValue.removeAllObjects()
        PickerValue=NSMutableArray(objects: "Ontario-CANADA","Quebec-CANADA","Alberta-CANADA","British Columbia-CANADA")
        detectPicker=3
        OpenPicker()
        Distance=nil
    }
    override func viewWillAppear(animated: Bool) {
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }
    
    func OpenPicker()
    {
//        if detectPicker==1
//        {
//            LnmwBTN.setTitle(PickerValue.objectAtIndex(0) as? String, forState: UIControlState.Normal)
//        }
//        else if detectPicker==2
//        {
//            LnpwBTN.setTitle(PickerValue.objectAtIndex(0) as? String, forState: UIControlState.Normal)
//        }
//        else if detectPicker==3
//        {
//            TeamBTN.setTitle(PickerValue.objectAtIndex(0) as? String, forState: UIControlState.Normal)
//        }
        pickerToolbar.hidden=false
        pickerView.hidden=false
        pickerView.reloadAllComponents()
        pickerView.selectRow(0, inComponent: 0, animated: false)
    }
    func donePicker()
    {
        pickerView.hidden=true
        pickerToolbar.hidden=true
    }
    /******************************************************
    MARK:PickerView Delegate Method
    ******************************************************/
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return PickerValue.count
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if detectPicker==1
        {
            LnmwBTN.setTitle(NSString(format: "%@ kms", PickerValue.objectAtIndex(row) as! String)  as String, forState: UIControlState.Normal)
            Distance=PickerValue.objectAtIndex(row).integerValue
        }
        else if detectPicker==2
        {
            LnpwBTN.setTitle(NSString(format: "%@ kms", PickerValue.objectAtIndex(row) as! String)  as String, forState: UIControlState.Normal)
            Distance=PickerValue.objectAtIndex(row).integerValue
        }
        else if detectPicker==3
        {
            LdBTN.setTitle(PickerValue.objectAtIndex(row) as? String, forState: UIControlState.Normal)
        }
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return NSString(format: "%@ kms", PickerValue.objectAtIndex(row) as! String)  as String
    }
    
    /******************************************************
    MARK:TextField Delegate Method
    ******************************************************/
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == LnpwTXT
        {
            Radio1.image=UIImage(named: "ic_radio_un_checked.png")
            Radio2.image=UIImage(named: "ic_radio_checked.png")
            Radio3.image=UIImage(named: "ic_radio_un_checked.png")
            View1.backgroundColor=UIColor.clearColor()
            View2.backgroundColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            View3.backgroundColor=UIColor.clearColor()
//            let borderLnpwTXT = CALayer()
//            let widthLnpwTXT = CGFloat(2.0)
//            borderLnpwTXT.borderColor = UIColor.whiteColor().CGColor
//            borderLnpwTXT.frame = CGRect(x: 0, y: LnpwTXT.frame.size.height - widthLnpwTXT, width:  LnpwTXT.frame.size.width, height: LnpwTXT.frame.size.height)
//            borderLnpwTXT.borderWidth = widthLnpwTXT
//            LnpwTXT.layer.addSublayer(borderLnpwTXT)
//            LnpwTXT.layer.masksToBounds = true
        }
        else if textField == LdTXT1
        {
            Radio1.image=UIImage(named: "ic_radio_un_checked.png")
            Radio2.image=UIImage(named: "ic_radio_un_checked.png")
            Radio3.image=UIImage(named: "ic_radio_checked.png")
            View1.backgroundColor=UIColor.clearColor()
            View2.backgroundColor=UIColor.clearColor()
            View3.backgroundColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
//            let borderLdTXT1 = CALayer()
//            let widthLdTXT1 = CGFloat(2.0)
//            borderLdTXT1.borderColor = UIColor.whiteColor().CGColor
//            borderLdTXT1.frame = CGRect(x: 0, y: LdTXT1.frame.size.height - widthLdTXT1, width:  LdTXT1.frame.size.width, height: LdTXT1.frame.size.height)
//            borderLdTXT1.borderWidth = widthLdTXT1
//            LdTXT1.layer.addSublayer(borderLdTXT1)
//            LdTXT1.layer.masksToBounds = true
        }
        else
        {
            Radio1.image=UIImage(named: "ic_radio_un_checked.png")
            Radio2.image=UIImage(named: "ic_radio_un_checked.png")
            Radio3.image=UIImage(named: "ic_radio_checked.png")
            View1.backgroundColor=UIColor.clearColor()
            View2.backgroundColor=UIColor.clearColor()
            View3.backgroundColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
//            let borderLdTXT2 = CALayer()
//            let widthLdTXT2 = CGFloat(2.0)
//            borderLdTXT2.borderColor = UIColor.whiteColor().CGColor
//            borderLdTXT2.frame = CGRect(x: 0, y: LdTXT2.frame.size.height - widthLdTXT2, width:  LdTXT2.frame.size.width, height: LdTXT2.frame.size.height)
//            borderLdTXT2.borderWidth = widthLdTXT2
//            LdTXT2.layer.addSublayer(borderLdTXT2)
//            LdTXT2.layer.masksToBounds = true
        }
        let textFieldRect : CGRect = self.view.window!.convertRect(textField.bounds, fromView: textField)
        let viewRect : CGRect = self.view.window!.convertRect(self.view.bounds, fromView: self.view)
        let midline : CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
        let numerator : CGFloat = midline - viewRect.origin.y - MoveKeyboard.MINIMUM_SCROLL_FRACTION * viewRect.size.height
        let denominator : CGFloat = (MoveKeyboard.MAXIMUM_SCROLL_FRACTION - MoveKeyboard.MINIMUM_SCROLL_FRACTION) * viewRect.size.height
        var heightFraction : CGFloat = numerator / denominator
        if heightFraction < 0.0 {
            heightFraction = 0.0
        } else if heightFraction > 1.0 {
            heightFraction = 1.0
        }
        let orientation : UIInterfaceOrientation = UIApplication.sharedApplication().statusBarOrientation
        if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown) {
            animateDistance = floor(MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
        } else {
            animateDistance = floor(MoveKeyboard.LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
        }
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y -= animateDistance
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        self.view.frame = viewFrame
        UIView.commitAnimations()
        
    }
    func textFieldDidEndEditing(textField: UITextField) {
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y += animateDistance
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        self.view.frame = viewFrame
        UIView.commitAnimations()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
}
