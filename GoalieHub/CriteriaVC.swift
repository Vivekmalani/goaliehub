//
//  CriteriaVC.swift
//  GoalieHub
//
//  Created by Vivek on 06/05/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit

class CriteriaVC: UIViewController {
    
    @IBOutlet var Radio3: UIImageView!
    @IBOutlet var Radio2: UIImageView!
    @IBOutlet var Radio1: UIImageView!
    @IBOutlet var ParamsView: UIView!
    @IBOutlet var FavView: UIView!
    @IBOutlet var UsernameView: UIView!
    @IBOutlet var BackScroll: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ParamsView.clipsToBounds=true
        ParamsView.layer.cornerRadius=5.0
        ParamsView.layer.borderWidth=1.0
        ParamsView.layer.borderColor=UIColor.lightGrayColor().CGColor
        
        FavView.clipsToBounds=true
        FavView.layer.cornerRadius=5.0
        FavView.layer.borderWidth=1.0
        FavView.layer.borderColor=UIColor.lightGrayColor().CGColor
        
        UsernameView.clipsToBounds=true
        UsernameView.layer.cornerRadius=5.0
        UsernameView.layer.borderWidth=1.0
        UsernameView.layer.borderColor=UIColor.lightGrayColor().CGColor
        
        let FirstViewgesture = UITapGestureRecognizer(target: self, action: "FirstViewAction:")
        UsernameView.addGestureRecognizer(FirstViewgesture)
        
        let SecondViewgesture = UITapGestureRecognizer(target: self, action: "SecondViewAction:")
        FavView.addGestureRecognizer(SecondViewgesture)
        
        let ThirdViewgesture = UITapGestureRecognizer(target: self, action: "ThirdViewAction:")
        ParamsView.addGestureRecognizer(ThirdViewgesture)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func FirstViewAction(sender:UITapGestureRecognizer){
        Radio1.image=UIImage(named: "ic_team_add_location_radio_checked.png")
        Radio2.image=UIImage(named: "ic_team_add_location_radio_un_checked.png")
        Radio3.image=UIImage(named: "ic_team_add_location_radio_un_checked.png")
    }
    func SecondViewAction(sender:UITapGestureRecognizer){
        Radio1.image=UIImage(named: "ic_team_add_location_radio_un_checked.png")
        Radio2.image=UIImage(named: "ic_team_add_location_radio_checked.png")
        Radio3.image=UIImage(named: "ic_team_add_location_radio_un_checked.png")
    }
    func ThirdViewAction(sender:UITapGestureRecognizer){
        Radio1.image=UIImage(named: "ic_team_add_location_radio_un_checked.png")
        Radio2.image=UIImage(named: "ic_team_add_location_radio_un_checked.png")
        Radio3.image=UIImage(named: "ic_team_add_location_radio_checked.png")
    }
    
    
    
    @IBAction func UserNameBTNClick(sender: UIButton) {
        
    }
}
