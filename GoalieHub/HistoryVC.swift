//
//  HistoryVC.swift
//  GoalieHub
//
//  Created by Vivek on 15/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class HistoryVC: UIViewController,MBProgressHUDDelegate {
    
    @IBOutlet var HistoryTBL: UITableView!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!,EventID:Int!
    let cellIdentifier = "cellIdentifier"
    var HistoryAry=NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()

        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        self.HistoryTBL?.registerClass(UITableViewCell.self, forCellReuseIdentifier: self.cellIdentifier)
        HistoryTBL.tableFooterView = UIView(frame: CGRectZero)
        HistoryTBL.separatorStyle=UITableViewCellSeparatorStyle.None
        
        EventID = NSUserDefaults.standardUserDefaults().valueForKey("EventID") as! Int
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "HistoryLoad:", name:"History", object: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func HistoryLoad(notification: NSNotification){
        HUD.show(true)
        let IdentiDict=NSDictionary(objectsAndKeys: EventID,"eventId")
        let ParamsDict=NSDictionary(objectsAndKeys:IdentiDict,"Identifier")
        let RequestDict=NSDictionary(objectsAndKeys: "getEventHistory","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Event",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
        }
    }

    /********************************************
    MARK:TableView Delegate
    ********************************************/
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return HistoryAry.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as! UITableViewCell
        cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: self.cellIdentifier)
        cell.frame=CGRectMake(0, 0, self.view.frame.size.width, 50)
        let SubBackView=UIView(frame: CGRectMake(10, 10, cell.frame.size.width-20, 45))
        
        let decodedData = NSData(base64EncodedString: HistoryAry.objectAtIndex(indexPath.row).valueForKey("vUserSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
        let TitleIcon=UIImageView(frame: CGRectMake(15, 5, 15, 15))
        TitleIcon.image=UIImage(data: decodedData!)
        TitleIcon.clipsToBounds=true
        TitleIcon.layer.cornerRadius=TitleIcon.frame.size.width/2
        SubBackView.addSubview(TitleIcon)
        
        let TItleLBL=UILabel(frame: CGRectMake(TitleIcon.frame.size.width+TitleIcon.frame.origin.x+5, 5, cell.frame.size.width-50, 15))
        TItleLBL.text=HistoryAry.objectAtIndex(indexPath.row).valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
        TItleLBL.font=UIFont.boldSystemFontOfSize(12)
        TItleLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        TItleLBL.numberOfLines=0
        TItleLBL.sizeToFit()
        SubBackView.addSubview(TItleLBL)
        
        let WroteLBL=UILabel(frame: CGRectMake(TItleLBL.frame.size.width+TItleLBL.frame.origin.x+5, 5, 35, 15))
        WroteLBL.text="wrote:"
        WroteLBL.textColor=UIColor.whiteColor()
        WroteLBL.font=UIFont.systemFontOfSize(10)
        SubBackView.addSubview(WroteLBL)
        
        let DateFormatter = NSDateFormatter()
        DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let DateObj = DateFormatter.dateFromString(HistoryAry.objectAtIndex(indexPath.row).valueForKey("commCreateTs") as! String)
        DateFormatter.dateFormat = "dd/MM HH:mm a"
        //LastSyncDateLBL.text=NSString(format: "Last Sync:%@", LastSyncDateFormatter.stringFromDate(LastSyncDateObj!)) as String
        let DateLBL=UILabel(frame: CGRectMake(SubBackView.frame.size.width-90, 5, 75, 15))
        DateLBL.text=DateFormatter.stringFromDate(DateObj!)
        DateLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        DateLBL.font=UIFont.systemFontOfSize(9)
        SubBackView.addSubview(DateLBL)
        
        let DateICon=UIImageView(frame: CGRectMake(DateLBL.frame.origin.x-20, 5, 15, 15))
        SubBackView.addSubview(DateICon)
        
        let CommentLBL=UILabel(frame: CGRectMake(20, TitleIcon.frame.size.height+TitleIcon.frame.origin.y+5, cell.frame.size.width-50, 20))
        CommentLBL.text=HistoryAry.objectAtIndex(indexPath.row).valueForKey("commText") as? String
        CommentLBL.font=UIFont.systemFontOfSize(12)
        CommentLBL.textColor=UIColor.whiteColor()
        CommentLBL.numberOfLines=0
        CommentLBL.sizeToFit()
        SubBackView.addSubview(CommentLBL)
        
        let LineIMG=UIImageView(frame: CGRectMake(0, 0, 10, CommentLBL.frame.size.height+CommentLBL.frame.origin.y+5))
        LineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(LineIMG)
        
        SubBackView.frame=CGRectMake(10, 10, cell.frame.size.width-20, CommentLBL.frame.size.height+CommentLBL.frame.origin.y+5)
        
        let SmallLineIMG=UIImageView(frame: CGRectMake(0, SubBackView.frame.size.height-1, SubBackView.frame.size.width, 1))
        SmallLineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(SmallLineIMG)
        
        let LastIMG=UIImageView(frame: CGRectMake(SubBackView.frame.size.width-20, 5, 20, 20))
        LastIMG.image=UIImage(named: "ic_general_more_details.png")
        SubBackView.addSubview(LastIMG)
        cell.addSubview(SubBackView)
        cell.backgroundColor=UIColor.blackColor()
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        
        HistoryTBL.rowHeight = SubBackView.frame.size.height+20
        
        return cell
    }
    
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            if let CheckDict=Dict.valueForKey("response")!.valueForKey("result") as? NSDictionary
            {
                HistoryAry.addObject(Dict.valueForKey("response")!.valueForKey("result")!)
            }
            else if let CheckAry=Dict.valueForKey("response")!.valueForKey("result") as? NSArray
            {
                HistoryAry=NSMutableArray(array: CheckAry)
            }
            HistoryTBL.reloadData()
            HUD.hide(true)
        }
    }
    
}
