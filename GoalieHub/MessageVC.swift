//
//  MessageVC.swift
//  GoalieHub
//
//  Created by Vivek on 18/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class MessageVC: UIViewController,MBProgressHUDDelegate,UITextFieldDelegate,UITextViewDelegate {
    @IBOutlet var BarBTN: UIBarButtonItem!
    @IBOutlet var TeamNameLBL: UILabel!
    @IBOutlet var TeamIMG: UIImageView!
    @IBOutlet var BackScroll: UIScrollView!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!
    var TeamDetailDict=NSMutableDictionary()
    var MemberAry=NSMutableArray()
    var LimitLBL:UILabel!
    var CountLBL:UILabel!
    var SubjectTXT:UITextField!
    var MessageText:UITextView!
    var animateDistance: CGFloat = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        HUD.show(true)
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        TeamIMG.clipsToBounds=true
        TeamIMG.layer.cornerRadius=TeamIMG.frame.size.width/2
        let decodedData = NSData(base64EncodedString: TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
        TeamIMG.image=UIImage(data:decodedData!)
        TeamNameLBL.text=TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
        
        let TeamID=TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("teamId") as! Int
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        let IdentiDict=NSDictionary(objectsAndKeys: TeamID,"teamId")
        let ParamsDict=NSDictionary(objectsAndKeys:IdentiDict,"Identifier")
        let RequestDict=NSDictionary(objectsAndKeys: "getTeamMembers","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Team",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse1(response.3, Dict: response.2 as? NSDictionary)
        }
        // Do any additional setup after loading the view.
    }
    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func HandleAPIResponse1(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            if let checkDict=Dict.valueForKey("response")!.valueForKey("result") as? NSDictionary
            {
                MemberAry.addObject(checkDict)
            }
            else if let checkAry=Dict.valueForKey("response")!.valueForKey("result") as? NSArray
            {
                MemberAry=NSMutableArray(array: checkAry)
            }
            var width:CGFloat=20.0
            var Height:CGFloat=30.0
            for var i=0;i<MemberAry.count;i++
            {
                let decodedData = NSData(base64EncodedString: MemberAry.objectAtIndex(i).valueForKey("vUserSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
                let UserIMG=UIImageView(frame: CGRectMake(width, Height, 30, 30))
                UserIMG.image=UIImage(data: decodedData!)
                UserIMG.clipsToBounds=true
                UserIMG.layer.cornerRadius=15
                BackScroll.addSubview(UserIMG)
                width=width+35
                
                let UserNameLBL=UILabel(frame: CGRectMake(width, Height+7, 300, 15))
                UserNameLBL.text=MemberAry.objectAtIndex(i).valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
                UserNameLBL.font=UIFont.boldSystemFontOfSize(12)
                UserNameLBL.textColor=UIColor.whiteColor()
                UserNameLBL.numberOfLines=0
                UserNameLBL.sizeToFit()
                BackScroll.addSubview(UserNameLBL)
                
                width=width+UserNameLBL.frame.size.width+5
                
                let NextLBL=UILabel(frame: CGRectMake(0, 0, 500, 15))
                if i != (MemberAry.count-1)
                {
                    NextLBL.text=MemberAry.objectAtIndex(i+1).valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
                }
                NextLBL.font=UIFont.systemFontOfSize(12)
                NextLBL.textColor=UIColor.whiteColor()
                NextLBL.numberOfLines=0
                NextLBL.sizeToFit()
                
                let FinalWidth=NextLBL.frame.size.width+width+40
                if FinalWidth > BackScroll.frame.size.width
                {
                    Height = Height + 40
                    width=20.0
                }
            }
            
            SubjectTXT=UITextField(frame: CGRectMake(15, Height+60, BackScroll.frame.size.width-65, 20))
            SubjectTXT.textColor=UIColor.lightGrayColor()
            SubjectTXT.textAlignment=NSTextAlignment.Center
            SubjectTXT.font=UIFont.systemFontOfSize(14)
            SubjectTXT.delegate=self
            SubjectTXT.returnKeyType=UIReturnKeyType.Done
            SubjectTXT.attributedPlaceholder = NSAttributedString(string:"Subject",attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
            BackScroll.addSubview(SubjectTXT)
            
            LimitLBL=UILabel(frame: CGRectMake(SubjectTXT.frame.size.width+15, Height+60, 35, 20))
            LimitLBL.text="0/25"
            LimitLBL.font=UIFont.systemFontOfSize(12)
            LimitLBL.textColor=UIColor.lightGrayColor()
            BackScroll.addSubview(LimitLBL)
            
            let LineImage=UIImageView(frame: CGRectMake(15, SubjectTXT.frame.size.height+SubjectTXT.frame.origin.y, BackScroll.frame.size.width-30, 7))
            LineImage.image=UIImage(named: "bg_text_filed_rounded_corner.png")
            BackScroll.addSubview(LineImage)
            
            let BackView=UIView(frame: CGRectMake(15, LineImage.frame.size.height+LineImage.frame.origin.y+20, BackScroll.frame.size.width-30, 100))
            BackView.clipsToBounds=true
            BackView.layer.cornerRadius=5.0
            BackView.layer.borderWidth=1.0
            BackView.layer.borderColor=UIColor.whiteColor().CGColor
            MessageText=UITextView(frame: CGRectMake(5, 5, BackView.frame.size.width-10, BackView.frame.size.height-20))
            MessageText.text="Comments"
            MessageText.backgroundColor=UIColor.clearColor()
            MessageText.font=UIFont.systemFontOfSize(12)
            MessageText.textColor=UIColor.grayColor()
            MessageText.delegate=self
            MessageText.returnKeyType=UIReturnKeyType.Done
            BackView.addSubview(MessageText)
            
            CountLBL=UILabel(frame: CGRectMake(BackView.frame.size.width-70, BackView.frame.size.height-20, 65, 20))
            CountLBL.text="0/250"
            CountLBL.textAlignment=NSTextAlignment.Right
            CountLBL.font=UIFont.systemFontOfSize(12)
            CountLBL.textColor=UIColor.lightGrayColor()
            BackView.addSubview(CountLBL)
            BackScroll.addSubview(BackView)
            
            BackScroll.contentSize=CGSizeMake(0, BackView.frame.size.height+BackView.frame.origin.y+10)
        }
        HUD.hide(true)
    }
    func HandleAPIResponse2(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
        }
        HUD.hide(true)
    }
    @IBAction func SendBTNClick(sender: UIButton) {
        
        if SubjectTXT.text.isEmpty==true
        {
            let  Warningalert = UIAlertView(title: nil,
                message:"Please enter subject",
                delegate: self,
                cancelButtonTitle:"OK")
            Warningalert.show()
        }
        else if MessageText.text == "Comments"
        {
            let  Warningalert = UIAlertView(title: nil,
                message:"Please enter comments",
                delegate: self,
                cancelButtonTitle:"OK")
            Warningalert.show()
        }
        else
        {
            MessageText.resignFirstResponder()
            SubjectTXT.resignFirstResponder()
            HUD.show(true)
            let TeamID=TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("teamId") as! Int
            ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
            KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
            let ValueDict=NSDictionary(objectsAndKeys: SubjectTXT.text,"subject",MessageText.text,"message")
            let IdentiDict=NSDictionary(objectsAndKeys: TeamID,"teamId")
            let ParamsDict=NSDictionary(objectsAndKeys:IdentiDict,"Identifier",ValueDict,"Value")
            let RequestDict=NSDictionary(objectsAndKeys: "sendTeamEmail","method","1","id",ParamsDict,"params")
            print(RequestDict)
            let param = [
                "id"    : ID,
                "key"    : KEY,
                "callBack"    : "myCallBackMethod",
                "serviceName"    : "Team",
                "request" : RequestDict
            ]
            println(param)
            Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
                .responseJSON {response in
                    self.HandleAPIResponse2(response.3, Dict: response.2 as? NSDictionary)
            }
        }
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        let textFieldRect : CGRect = self.view.window!.convertRect(textField.bounds, fromView: textField)
        let viewRect : CGRect = self.view.window!.convertRect(self.view.bounds, fromView: self.view)
        let midline : CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
        let numerator : CGFloat = midline - viewRect.origin.y - MoveKeyboard.MINIMUM_SCROLL_FRACTION * viewRect.size.height
        let denominator : CGFloat = (MoveKeyboard.MAXIMUM_SCROLL_FRACTION - MoveKeyboard.MINIMUM_SCROLL_FRACTION) * viewRect.size.height
        var heightFraction : CGFloat = numerator / denominator
        if heightFraction < 0.0 {
            heightFraction = 0.0
        } else if heightFraction > 1.0 {
            heightFraction = 1.0
        }
        let orientation : UIInterfaceOrientation = UIApplication.sharedApplication().statusBarOrientation
        if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown) {
            animateDistance = floor(MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
        } else {
            animateDistance = floor(MoveKeyboard.LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
        }
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y -= animateDistance
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        self.view.frame = viewFrame
        UIView.commitAnimations()
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y += animateDistance
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        self.view.frame = viewFrame
        UIView.commitAnimations()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        //print(string)
        
        let newLength = count(textField.text.utf16) + count(string.utf16) - range.length
        print(newLength)
        if newLength == 26
        {
            return false
        }
        else
        {
            LimitLBL.text=NSString(format: "%i/25", newLength) as String
            return true
        }
    }
    
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"
        {
            textView.resignFirstResponder()
            return false
        }
        else
        {
            let newLength = count(textView.text.utf16) + count(text.utf16) - range.length
            if newLength == 251
            {
                return false
            }
            else
            {
                CountLBL.text=NSString(format: "%i/250", newLength) as String
                return true
            }
        }
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        
        if textView.text=="Comments"
        {
            textView.text=""
        }
        let textFieldRect : CGRect = self.view.window!.convertRect(textView.bounds, fromView: textView)
        let viewRect : CGRect = self.view.window!.convertRect(self.view.bounds, fromView: self.view)
        let midline : CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
        let numerator : CGFloat = midline - viewRect.origin.y - MoveKeyboard.MINIMUM_SCROLL_FRACTION * viewRect.size.height
        let denominator : CGFloat = (MoveKeyboard.MAXIMUM_SCROLL_FRACTION - MoveKeyboard.MINIMUM_SCROLL_FRACTION) * viewRect.size.height
        var heightFraction : CGFloat = numerator / denominator
        if heightFraction < 0.0 {
            heightFraction = 0.0
        } else if heightFraction > 1.0 {
            heightFraction = 1.0
        }
        let orientation : UIInterfaceOrientation = UIApplication.sharedApplication().statusBarOrientation
        if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown) {
            animateDistance = floor(MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
        } else {
            animateDistance = floor(MoveKeyboard.LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
        }
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y -= animateDistance
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        self.view.frame = viewFrame
        UIView.commitAnimations()
    }
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty==true
        {
            textView.text="Comments"
        }
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y += animateDistance
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        self.view.frame = viewFrame
        UIView.commitAnimations()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }
}
