//
//  MemberVC.swift
//  GoalieHub
//
//  Created by Vivek on 18/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class MemberVC: UIViewController,MBProgressHUDDelegate {
    let cellIdentifier = "cellIdentifier"
    @IBOutlet var BarBTN: UIBarButtonItem!
    @IBOutlet var MemberTBL: UITableView!
    @IBOutlet var TeamNameLBL: UILabel!
    @IBOutlet var TeamIMG: UIImageView!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!
    var MemberAry=NSMutableArray()
    var TeamDetailDict=NSMutableDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        HUD.show(true)
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        self.MemberTBL?.registerClass(UITableViewCell.self, forCellReuseIdentifier: self.cellIdentifier)
        MemberTBL.tableFooterView = UIView(frame: CGRectZero)
        MemberTBL.separatorStyle=UITableViewCellSeparatorStyle.None
        TeamIMG.clipsToBounds=true
        TeamIMG.layer.cornerRadius=TeamIMG.frame.size.width/2
        let decodedData = NSData(base64EncodedString: TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
        TeamIMG.image=UIImage(data:decodedData!)
        TeamNameLBL.text=TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
        
        let TeamID=TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("teamId") as! Int
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        let IdentiDict=NSDictionary(objectsAndKeys: TeamID,"teamId")
        let ParamsDict=NSDictionary(objectsAndKeys:IdentiDict,"Identifier")
        let RequestDict=NSDictionary(objectsAndKeys: "getTeamMembers","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Team",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                print(response)
                self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func AddMemberBTNClick(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("AddMemberVC") as! AddMemberVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
        initialViewController.TeamDetailDict=TeamDetailDict as NSMutableDictionary
    }
    @IBAction func AddMsgBTNClick(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("MessageVC") as! MessageVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
        initialViewController.TeamDetailDict=TeamDetailDict as NSMutableDictionary
    }
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            
            if let checkDict=Dict.valueForKey("response")!.valueForKey("result") as? NSDictionary
            {
                MemberAry.addObject(checkDict)
            }
            else if let checkAry=Dict.valueForKey("response")!.valueForKey("result") as? NSArray
            {
                MemberAry=NSMutableArray(array: checkAry)
            }
            MemberTBL.reloadData()
        }
        HUD.hide(true)
    }
    
    
    /********************************************
    MARK:TableView Delegate
    ********************************************/
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MemberAry.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as! UITableViewCell
        cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: self.cellIdentifier)
        
        cell.frame=CGRectMake(0, 0, self.view.frame.size.width, 40)
        let SubBackView=UIView(frame: CGRectMake(10, 10, cell.frame.size.width-20, 50))
        
        let LineIMG=UIImageView(frame: CGRectMake(0, 0, 10, 50))
        LineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(LineIMG)
        
        let decodedData = NSData(base64EncodedString: MemberAry.objectAtIndex(indexPath.row).valueForKey("vUserSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
        let TitleIcon=UIImageView(frame: CGRectMake(LineIMG.frame.size.width+LineIMG.frame.origin.x+5, 5, 40, 40))
        TitleIcon.image=UIImage(data: decodedData!)
        TitleIcon.clipsToBounds=true
        TitleIcon.layer.cornerRadius=TitleIcon.frame.size.width/2
        SubBackView.addSubview(TitleIcon)
        
        let TItleLBL=UILabel(frame: CGRectMake(TitleIcon.frame.size.width+TitleIcon.frame.origin.x+5, 5, SubBackView.frame.size.width-TitleIcon.frame.origin.x-20, 40))
        TItleLBL.text=MemberAry.objectAtIndex(indexPath.row).valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
        TItleLBL.font=UIFont.boldSystemFontOfSize(12)
        TItleLBL.textColor=UIColor.whiteColor()
        SubBackView.addSubview(TItleLBL)
        
        
        let SmallLineIMG=UIImageView(frame: CGRectMake(0, SubBackView.frame.size.height-1, SubBackView.frame.size.width, 1))
        SmallLineIMG.backgroundColor=UIColor.whiteColor()
        SubBackView.addSubview(SmallLineIMG)
        
        let LastIMG=UIImageView(frame: CGRectMake(SubBackView.frame.size.width-20, 5, 20, 20))
        LastIMG.image=UIImage(named: "ic_general_more_details.png")
        SubBackView.addSubview(LastIMG)
        cell.addSubview(SubBackView)
        cell.backgroundColor=UIColor.blackColor()
        cell.selectionStyle=UITableViewCellSelectionStyle.None
        MemberTBL.rowHeight = SubBackView.frame.size.height+20
        
        return cell
    }
    override func viewWillAppear(animated: Bool) {
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }
}
