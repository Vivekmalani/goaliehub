//
//  CancelEventVC.swift
//  GoalieHub
//
//  Created by Vivek on 04/05/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class CancelEventVC: UIViewController,MBProgressHUDDelegate {
    @IBOutlet var BarBTN:UIBarButtonItem!
    @IBOutlet var IDLBL:UILabel!
    @IBOutlet var UserName:UILabel!
    @IBOutlet var UserIMG:UIImageView!
    @IBOutlet var DateLBL:UILabel!
    @IBOutlet var EventNameLBL:UILabel!
    @IBOutlet var LocationLBL: UILabel!
    @IBOutlet var CreateDateLBL: UILabel!
    var CancelDict=NSMutableDictionary()
    var ID:NSNumber!,KEY:NSNumber!
    var HUD:MBProgressHUD!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        // Do any setup after loading the view.
        
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        EventNameLBL.text=CancelDict.valueForKey("response")!.valueForKey("result")!.valueForKey("eventName") as? String
        IDLBL.text=NSString(format: "Event : %@", CancelDict.valueForKey("response")!.valueForKey("result")!.valueForKey("eventId") as! NSNumber) as String
        
        let DateFormatter = NSDateFormatter()
        DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let DateObj = DateFormatter.dateFromString(CancelDict.valueForKey("response")!.valueForKey("result")!.valueForKey("eventTs") as! String)
        DateFormatter.dateFormat = "EE,MMM dd,yyyy"
        DateLBL.text = DateFormatter.stringFromDate(DateObj!)
        
        let CreateDate=CancelDict.valueForKey("response")!.valueForKey("result")!.valueForKey("eventCreateTs") as! String
        let CreateDateAry=CreateDate.componentsSeparatedByString(" ")
        CreateDateLBL.text=CreateDateAry[0]
        
        let ImageData = NSData(base64EncodedString: CancelDict.valueForKey("response")!.valueForKey("result")!.valueForKey("vUserSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
        UserIMG.image=UIImage(data: ImageData!)
        UserIMG.clipsToBounds=true
        UserIMG.layer.cornerRadius=UserIMG.frame.size.width/2
        
        UserName.text=CancelDict.valueForKey("response")!.valueForKey("result")!.valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
        let Width=UserName.intrinsicContentSize().width
        UserName.frame=CGRectMake(self.view.frame.size.width-Width-5, UserName.frame.origin.y, Width, UserName.frame.size.height)
        UserIMG.frame=CGRectMake(UserName.frame.origin.x-UserIMG.frame.size.width-5, UserIMG.frame.origin.y, UserIMG.frame.size.width, UserIMG.frame.size.height)
        LocationLBL.text=CancelDict.valueForKey("response")!.valueForKey("result")!.valueForKey("eventLocationnote") as? String
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func CancelBTNClick(sender: UIButton) {
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        let IdentDict=NSDictionary(objectsAndKeys: CancelDict.valueForKey("response")!.valueForKey("result")!.valueForKey("eventId") as! Int,"eventId")
        let ParamsDict=NSDictionary(objectsAndKeys:IdentDict,"Identifier")
        let RequestDict=NSDictionary(objectsAndKeys: "cancelEvent","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Event",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters:param, encoding:.JSON).responseJSON {response in
            self.HandleAPIResponse(response.3, Dict:response.2 as? NSDictionary)
        }
    }
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
        }
        HUD.hide(true)
    }
}
