//
//  SaveLocationVC.swift
//  GoalieHub
//
//  Created by Vivek on 07/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class SaveLocationVC: UIViewController ,MBProgressHUDDelegate{
    var registerUDIDConnectionLocation=NSURLConnection()
    var registerUDIDDataLocation : NSMutableData!
    var registerUDIDConnectionAddLocation=NSURLConnection()
    var registerUDIDDataAddLocation : NSMutableData!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!,LocID:Int!,Distance:Int!
    var SearchAry=NSMutableArray()
    @IBOutlet var BackScroll: UIScrollView!
    @IBOutlet var BackView: UIView!
    @IBOutlet var PopupView: UIView!
    @IBOutlet var MapBTN: UIButton!
    @IBOutlet var AddLocationBTN: UIButton!
    
    var CheckAddLocationAry=NSMutableArray()
    var AgentLocationAry=NSMutableArray()
    var CityNameStr:String!
    override func viewDidLoad() {
        super.viewDidLoad()

//        print(CityNameStr)
//        print(Distance)
        
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        
        MapBTN.clipsToBounds=true
        MapBTN.layer.cornerRadius=5
        MapBTN.layer.borderWidth=1.0
        MapBTN.layer.borderColor=UIColor.whiteColor().CGColor
        
        AddLocationBTN.clipsToBounds=true
        AddLocationBTN.layer.cornerRadius=5
        AddLocationBTN.layer.borderWidth=1.0
        AddLocationBTN.layer.borderColor=UIColor.whiteColor().CGColor
        
        let gesture = UITapGestureRecognizer(target: self, action: "someAction:")
        BackView.addGestureRecognizer(gesture)
        
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        var ParamsDict=NSDictionary()
        var RequestDict=NSDictionary()
        if CityNameStr != nil
        {
            HUD.show(true)
            ParamsDict=NSDictionary(objectsAndKeys: CityNameStr,"city",25,"limitResult",1,"pageNumber","Y","showDistance")
            RequestDict=NSDictionary(objectsAndKeys: "getLocationsByNameCriteria","method","1","id",ParamsDict,"params")
            print(RequestDict)
            let param = [
                "id"    : ID,
                "key"    : KEY,
                "callBack"    : "myCallBackMethod",
                "serviceName"    : "Location",
                "request" : RequestDict
            ]
            println(param)
            Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
                .responseJSON {response in
                    self.HandleAPIResponse1(response.3, Dict: response.2 as? NSDictionary)
            }
        }
        else if Distance != nil
        {
            HUD.show(true)
            ParamsDict=NSDictionary(objectsAndKeys: Distance,"maxDistance","locname","sortBy","asc","sortDirection",25,"limitResult",1,"pageNumber")
            RequestDict=NSDictionary(objectsAndKeys: "getLocationsByDistance","method","1","id",ParamsDict,"params")
            print(RequestDict)
            let param = [
                "id"    : ID,
                "key"    : KEY,
                "callBack"    : "myCallBackMethod",
                "serviceName"    : "Location",
                "request" : RequestDict
            ]
            println(param)
            Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
                .responseJSON {response in
                    self.HandleAPIResponse1(response.3, Dict: response.2 as? NSDictionary)
            }
        }
        
        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func someAction(sender:UITapGestureRecognizer){
        BackView.hidden=true
        PopupView.hidden=true
    }
    
    @IBAction func AddLocationBTNClick(sender: UIButton) {
        
        BackView.hidden=true
        PopupView.hidden=true
        if CheckAddLocationAry.objectAtIndex(LocID).isEqualToString("0")
        {
            CheckAddLocationAry.replaceObjectAtIndex(LocID, withObject: "1")
            AgentLocationAry.addObject(SearchAry.objectAtIndex(LocID).valueForKey("locId") as! Int)
            
        }
        else
        {
            CheckAddLocationAry.replaceObjectAtIndex(LocID, withObject: "0")
            for var i=0;i<AgentLocationAry.count;i++
            {
                
                if AgentLocationAry.objectAtIndex(i).integerValue == SearchAry.objectAtIndex(LocID).valueForKey("locId") as! Int
                {
                    AgentLocationAry.removeObjectAtIndex(i)
                }
            }
        }
        LoadScrollView()
        
    }

    @IBAction func DoneBTNClick(sender: UIButton) {
        HUD.show(true)
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        let IdentifierDict=NSDictionary(objectsAndKeys: 102,"agentId")
        let ValueDict=NSDictionary(objectsAndKeys: AgentLocationAry,"AgentLocation")
        let ParamsDict=NSDictionary(objectsAndKeys: IdentifierDict,"Identifier",ValueDict,"Value")
        let RequestDict=NSDictionary(objectsAndKeys: "addAgentLocations","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Agent",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse2(response.3, Dict: response.2 as? NSDictionary)
        }
    }
    
    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func HandleAPIResponse1(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            
            if let Check = Dict.valueForKey("response")!.valueForKey("result") as? NSDictionary
            {
                SearchAry.addObject(Dict.valueForKey("response")!.valueForKey("result")!)
            }
            else
            {
                SearchAry=NSMutableArray(array: Dict.valueForKey("response")!.valueForKey("result") as! NSArray)
            }
            
            for var i=0;i<SearchAry.count;i++
            {
                CheckAddLocationAry.addObject("0")
            }
            LoadScrollView()
            
            HUD.hidden=true
        }
    }
    func HandleAPIResponse2(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as! [UIViewController];
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true);
            HUD.hide(true)
        }
    }
    func BackBTNClick1(sender:UIButton!)
    {
        if CheckAddLocationAry.objectAtIndex(sender.tag).isEqualToString("1")
        {
            AddLocationBTN.setTitle("Remove Location", forState: UIControlState.Normal)
        }
        else
        {
            AddLocationBTN.setTitle("Add Location", forState: UIControlState.Normal)
        }
        LocID=sender.tag
        BackView.hidden=false
        PopupView.hidden=false
    }
    
    func LoadScrollView()
    {
        for view in BackScroll.subviews {
            view.removeFromSuperview()
        }
        var Height:CGFloat=10.0
        for var i=0;i<SearchAry.count;i++
        {
            let BackView=UIView(frame: CGRectMake(10, Height, BackScroll.frame.size.width-20, 50))
            //BackView.backgroundColor=UIColor.whiteColor()
            BackView.tag=i+100
            let LineIMG=UIImageView(frame: CGRectMake(0, 0, 10, 50))
            LineIMG.backgroundColor=UIColor.whiteColor()
            BackView.addSubview(LineIMG)
            
            //print(SearchAry.objectAtIndex(i).valueForKey("distanceToLocation"))
            let tempString = SearchAry.objectAtIndex(i).valueForKey("distanceToLocation") as! NSNumber
            let DistanceLBL=UILabel(frame: CGRectMake(BackView.frame.size.width-20, 5, 200, 20))
            DistanceLBL.numberOfLines=0
            DistanceLBL.text="(\(tempString))"
            DistanceLBL.sizeToFit()
            DistanceLBL.frame=CGRectMake(BackView.frame.size.width-DistanceLBL.frame.size.width-20, 5, DistanceLBL.frame.size.width, 20)
            DistanceLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            DistanceLBL.font=UIFont.boldSystemFontOfSize(13)
            DistanceLBL.textAlignment=NSTextAlignment.Right
            //                DistanceLBL.layer.borderWidth=1.0
            //                DistanceLBL.layer.borderColor=UIColor.whiteColor().CGColor
            //DistanceLBL.backgroundColor=UIColor.whiteColor()
            BackView.addSubview(DistanceLBL)
            
            let TitleLBL=UILabel(frame: CGRectMake(33, 5, BackView.frame.size.width-DistanceLBL.frame.size.width-40, 20))
            TitleLBL.text=SearchAry.objectAtIndex(i).valueForKey("locName") as? String
            TitleLBL.textColor=UIColor.lightGrayColor()
            TitleLBL.font=UIFont.systemFontOfSize(13)
            TitleLBL.numberOfLines=0
            TitleLBL.sizeToFit()
            BackView.addSubview(TitleLBL)
            var Width:CGFloat=0.0
            if CheckAddLocationAry.objectAtIndex(i).isEqualToString("1")
            {
                let AddLBL=UILabel(frame: CGRectMake(BackView.frame.size.width-30, TitleLBL.frame.size.height+TitleLBL.frame.origin.y+10, 30, 10))
                AddLBL.text="Add"
                AddLBL.textColor=UIColor(red: 1.0/255.0, green: 203.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                AddLBL.font=UIFont.boldSystemFontOfSize(12)
                BackView.addSubview(AddLBL)
                Width=10
            }
            
            let addressLBL=UILabel(frame: CGRectMake(33, TitleLBL.frame.size.height+TitleLBL.frame.origin.y+5, BackView.frame.size.width-40-Width, 20))
            addressLBL.text=NSString(format: "%@,%@,%@", SearchAry.objectAtIndex(i).valueForKey("Address")!.valueForKey("addrStreetaddress") as! String,SearchAry.objectAtIndex(i).valueForKey("Address")!.valueForKey("addrCity") as! String,SearchAry.objectAtIndex(i).valueForKey("Address")!.valueForKey("addrCountry") as! String) as String
            addressLBL.textColor=UIColor.lightGrayColor()
            addressLBL.font=UIFont.systemFontOfSize(13)
            addressLBL.numberOfLines=0
            addressLBL.sizeToFit()
//            addressLBL.layer.borderWidth=1.0
//            addressLBL.layer.borderColor=UIColor.whiteColor().CGColor
            BackView.addSubview(addressLBL)
            
            BackView.frame=CGRectMake(10, Height, BackScroll.frame.size.width-20, addressLBL.frame.size.height+addressLBL.frame.origin.y+10)
            LineIMG.frame=CGRectMake(0, 0, 10, BackView.frame.size.height)
            
            let LocationIcon=UIImageView(frame: CGRectMake(LineIMG.frame.size.width+LineIMG.frame.origin.x+5, (BackView.frame.size.height/2)-10, 13, 20))
            LocationIcon.image=UIImage(named: "ic_home_location_small.png")
            BackView.addSubview(LocationIcon)
            
            let SmallLineIMG=UIImageView(frame: CGRectMake(0, BackView.frame.size.height-1, BackView.frame.size.width, 1))
            SmallLineIMG.backgroundColor=UIColor.whiteColor()
            BackView.addSubview(SmallLineIMG)
            
            let LastIMG=UIImageView(frame: CGRectMake(BackView.frame.size.width-15, 5, 20, 20))
            LastIMG.image=UIImage(named: "ic_general_more_details.png")
            BackView.addSubview(LastIMG)
            
            BackScroll.addSubview(BackView)
            
            let BackBTN=UIButton(frame: CGRectMake(BackView.frame.origin.x, Height, BackView.frame.size.width, BackView.frame.size.height))
            BackBTN.addTarget(self, action: "BackBTNClick1:", forControlEvents: UIControlEvents.TouchUpInside)
            BackBTN.tag=i
            BackScroll.addSubview(BackBTN)
            
            Height = Height + BackView.frame.size.height + 10
            
        }
        BackScroll.contentSize=CGSizeMake(0, Height+10)
    }
    override func viewWillAppear(animated: Bool) {
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }
}
