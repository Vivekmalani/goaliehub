//
//  NotificationHomeVC.swift
//  GoalieHub
//
//  Created by Vivek on 14/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit

class NotificationHomeVC: UIViewController {
    @IBOutlet var MenuView: UIView!
    var carbonTabSwipeNavigation:CarbonTabSwipeNavigation!
    @IBOutlet var BarBTN: UIBarButtonItem!
    @IBOutlet var LogoIMG: UIImageView!
    @IBOutlet var LastSyncDateLBL: UILabel!
    @IBOutlet var WorkingLBL: UILabel!
    @IBOutlet var StatusLBL: UILabel!
    @IBOutlet var StatusIMG: UIImageView!
    @IBOutlet var StatusView: UIView!
    var itemAry=NSArray()
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        itemAry=NSArray(array: ["NOTIFICATION","REQUESTS"])
        carbonTabSwipeNavigation=CarbonTabSwipeNavigation(items: itemAry as [AnyObject], delegate: self)
        carbonTabSwipeNavigation.insertIntoRootViewController(self, andTargetView: MenuView)
        Style()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func Style()
    {
        let color=UIColor.whiteColor()
        self.navigationController?.navigationBar.translucent=false
        carbonTabSwipeNavigation.setTabBarHeight(40)
        carbonTabSwipeNavigation.toolbar.translucent=false
        carbonTabSwipeNavigation.setIndicatorColor(color)
        carbonTabSwipeNavigation.setSelectedColor(UIColor.whiteColor())
        carbonTabSwipeNavigation.setNormalColor(UIColor.whiteColor())
        carbonTabSwipeNavigation.toolbar.barTintColor=UIColor.blackColor()
        //carbonTabSwipeNavigation.view.backgroundColor
        carbonTabSwipeNavigation.setTabExtraWidth(30)
        carbonTabSwipeNavigation.carbonSegmentedControl.setWidth(self.view.frame.size.width/2, forSegmentAtIndex: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl.setWidth(self.view.frame.size.width/2, forSegmentAtIndex: 1)
    }
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        switch index
        {
        case 0:
            return self.storyboard!.instantiateViewControllerWithIdentifier("NotificationVC") as! NotificationVC
        default:
            return self.storyboard!.instantiateViewControllerWithIdentifier("RequestVC") as! RequestVC
        }
    }
    override func viewWillAppear(animated: Bool) {
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }
}
