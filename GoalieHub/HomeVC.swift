//
//  HomeVC.swift
//  GoalieHub
//
//  Created by Vivek on 02/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
extension UIView{
    func setHeader(LastSync:String!)
    {
//        let BackIMG=UIImageView(frame: CGRectMake(0, 0, self.frame.size.width, 30))
//        BackIMG.backgroundColor=UIColor.lightGrayColor()
//        BackIMG.userInteractionEnabled=true
//        self.addSubview(BackIMG)
        
        let SyncLBL=UILabel(frame: CGRectMake(self.frame.size.width-165, 5, 160, 10))
        SyncLBL.text=LastSync
        SyncLBL.textColor=UIColor.whiteColor()
        SyncLBL.font=UIFont.boldSystemFontOfSize(9)
        SyncLBL.textAlignment=NSTextAlignment.Right
        self.addSubview(SyncLBL)
        
        let WorkingLBL=UILabel(frame: CGRectMake(self.frame.size.width-140, SyncLBL.frame.size.height+9, 85, 10))
        WorkingLBL.text="Currently Working"
        WorkingLBL.textColor=UIColor.whiteColor()
        WorkingLBL.font=UIFont.boldSystemFontOfSize(9)
        self.addSubview(WorkingLBL)
        
        let GreenIMG=UIImageView(frame: CGRectMake(WorkingLBL.frame.size.width+WorkingLBL.frame.origin.x+5, SyncLBL.frame.size.height+9, 10, 10))
        GreenIMG.image=UIImage(named: "ic_on.png")
        self.addSubview(GreenIMG)
        
        let StatusLBL=UILabel(frame: CGRectMake(GreenIMG.frame.size.width+GreenIMG.frame.origin.x+5, SyncLBL.frame.size.height+9, 35, 10))
        StatusLBL.text="Online"
        StatusLBL.textColor=UIColor.whiteColor()
        StatusLBL.font=UIFont.boldSystemFontOfSize(9)
        self.addSubview(StatusLBL)
    }
    
}

class HomeVC: UIViewController,MBProgressHUDDelegate,CarbonTabSwipeNavigationDelegate{
    var carbonTabSwipeNavigation:CarbonTabSwipeNavigation!
    @IBOutlet var BackView: UIView!
    @IBOutlet var BarBTN: UIBarButtonItem!
    @IBOutlet var DateLBL: UILabel!
    @IBOutlet var UserNameLBL: UILabel!
    @IBOutlet var UserIMG: UIImageView!
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!
    var itemAry=NSArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
//        if NSUserDefaults.standardUserDefaults().valueForKey("CheckNav")!.isEqualToString("1")
//        {
//            itemAry=NSArray(array: ["RECORDS","FEEDBACK"])
//        }
//        else
//        {
            itemAry=NSArray(array: ["MY RECORDS","FEEDBACK","LEAVE FEEDBACK"])
        //}
        
        carbonTabSwipeNavigation=CarbonTabSwipeNavigation(items: itemAry as [AnyObject], delegate: self)
        carbonTabSwipeNavigation.insertIntoRootViewController(self, andTargetView: BackView)
        Style()
        
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        
        let ParamsDict=NSDictionary()
        let RequestDict=NSDictionary(objectsAndKeys: "getMyUserDetails","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "User",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
            HUD.hide(true)
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict!.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict!.valueForKey("key"), forKey: "KEY")
            let decodedData = NSData(base64EncodedString: Dict!.valueForKey("response")!.valueForKey("result")!.valueForKey("vUserSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
            UserIMG.image=UIImage(data: decodedData!)
            
            UserNameLBL.text=Dict!.valueForKey("response")!.valueForKey("result")!.valueForKey("vUserSmall")!.valueForKey("userdetFullname") as? String
            DateLBL.text=Dict!.valueForKey("response")!.valueForKey("result")!.valueForKey("vUserSmall")!.valueForKey("userName") as? String
            
            let LastSyncDateFormatter = NSDateFormatter()
            LastSyncDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let LastSyncDateObj = LastSyncDateFormatter.dateFromString(Dict!.valueForKey("response")!.valueForKey("result")!.valueForKey("vUserLastseen")!.valueForKey("lastseenTs") as! String)
            LastSyncDateFormatter.dateFormat = "dd/MM/yyyy HH:mm a"
            
            self.view.setHeader(NSString(format: "Last Sync:%@", LastSyncDateFormatter.stringFromDate(LastSyncDateObj!)) as String)
            NSUserDefaults.standardUserDefaults().setValue(NSString(format: "Last Sync:%@", LastSyncDateFormatter.stringFromDate(LastSyncDateObj!)), forKey: "LastSync")
            
            //LastSyncDateLBL.text=NSString(format: "Last Sync:%@", LastSyncDateFormatter.stringFromDate(LastSyncDateObj!)) as String
        }
    }
    
    func Style()
    {
        let color=UIColor.whiteColor()
        self.navigationController?.navigationBar.translucent=false
        carbonTabSwipeNavigation.setTabBarHeight(40)
        carbonTabSwipeNavigation.toolbar.translucent=false
        carbonTabSwipeNavigation.setIndicatorColor(color)
        carbonTabSwipeNavigation.setSelectedColor(UIColor.whiteColor())
        carbonTabSwipeNavigation.setNormalColor(UIColor.whiteColor())
        carbonTabSwipeNavigation.toolbar.barTintColor=UIColor.blackColor()
        //carbonTabSwipeNavigation.view.backgroundColor
        carbonTabSwipeNavigation.setTabExtraWidth(30)
        
//        carbonTabSwipeNavigation.toolbar.layer.borderWidth=1.0
//        carbonTabSwipeNavigation.toolbar.layer.borderColor=UIColor.whiteColor().CGColor
//        carbonTabSwipeNavigation.toolbar.clipsToBounds=true
        
        carbonTabSwipeNavigation.carbonSegmentedControl.setWidth(self.view.frame.size.width/2, forSegmentAtIndex: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl.setWidth(self.view.frame.size.width/2, forSegmentAtIndex: 1)
        carbonTabSwipeNavigation.carbonSegmentedControl.setWidth(self.view.frame.size.width/2, forSegmentAtIndex: 2)
    }
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        switch index
        {
        case 0:
            return self.storyboard!.instantiateViewControllerWithIdentifier("MyrecordVC") as! MyrecordVC
        case 1:
            return self.storyboard!.instantiateViewControllerWithIdentifier("FeedbackVC") as! FeedbackVC
        default:
            return self.storyboard!.instantiateViewControllerWithIdentifier("LeaveFeedbackVC") as! LeaveFeedbackVC
        }
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAtIndex index: UInt) {
        if index == 0
        {
            NSNotificationCenter.defaultCenter().postNotificationName("MyRecords", object: nil)
        }
        else if index==1
        {
            NSNotificationCenter.defaultCenter().postNotificationName("Feedback", object: nil)
        }
        else if index==2
        {
            NSNotificationCenter.defaultCenter().postNotificationName("LeaveFeedback", object: nil)
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
//        let BackIMG=UIImageView(frame: CGRectMake(0,65,self.view.frame.size.width,30))
//        BackIMG.backgroundColor=UIColor.lightGrayColor()
//        self.navigationController?.view.addSubview(BackIMG)
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
        
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
