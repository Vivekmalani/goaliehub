//
//  TeamOptionVC.swift
//  GoalieHub
//
//  Created by Vivek on 18/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit

class TeamOptionVC: UIViewController {
    @IBOutlet var BarBTN: UIBarButtonItem!
    @IBOutlet var TeamNameLBL: UILabel!
    @IBOutlet var TeamImage: UIImageView!
    var TeamDetailDict=NSMutableDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        print(TeamDetailDict)
        TeamImage.clipsToBounds=true
        TeamImage.layer.cornerRadius=TeamImage.frame.size.width/2
        let decodedData = NSData(base64EncodedString: TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
        TeamImage.image=UIImage(data:decodedData!)
        TeamNameLBL.text=TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func MemberBTNClick(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("MemberVC") as! MemberVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
        initialViewController.TeamDetailDict=TeamDetailDict as NSMutableDictionary
    }

    @IBAction func SettingBTNClick(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("TeamSettingVC") as! TeamSettingVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
        initialViewController.TeamDetailDict=TeamDetailDict as NSMutableDictionary
    }
    
    @IBAction func EventBTNClick(sender: UIButton) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var initialViewController = storyboard.instantiateViewControllerWithIdentifier("TeamEventVC") as! TeamEventVC
        self.navigationController?.pushViewController(initialViewController, animated: true)
        initialViewController.TeamDetailDict=TeamDetailDict as NSMutableDictionary
    }
    override func viewWillAppear(animated: Bool) {
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }

}
