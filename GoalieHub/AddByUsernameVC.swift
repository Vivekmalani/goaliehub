//
//  AddByUsernameVC.swift
//  GoalieHub
//
//  Created by Vivek on 21/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
class AddByUsernameVC: UIViewController,MBProgressHUDDelegate {
    @IBOutlet var BarBTN: UIBarButtonItem!
    @IBOutlet var TeamNameLBL: UILabel!
    @IBOutlet var TeamIMG: UIImageView!
    var TeamDetailDict=NSMutableDictionary()
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!
    var animateDistance: CGFloat = 0.0
    @IBOutlet var UserTBL: UITableView!
    @IBOutlet var TXTUsername: UITextField!
    let cellIdentifier = "cellIdentifier"
    var UserAry=NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()

        HUD=MBProgressHUD(view: self.view)
        self.view.addSubview(HUD)
        self.view.bringSubviewToFront(HUD)
        HUD.delegate=self
        
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        self.UserTBL?.registerClass(UITableViewCell.self, forCellReuseIdentifier: self.cellIdentifier)
        UserTBL.tableFooterView = UIView(frame: CGRectZero)
        UserTBL.separatorStyle=UITableViewCellSeparatorStyle.None
        
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        TeamIMG.clipsToBounds=true
        TeamIMG.layer.cornerRadius=TeamIMG.frame.size.width/2
        let decodedData = NSData(base64EncodedString: TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("imgMicro") as! String, options: NSDataBase64DecodingOptions(rawValue: 0))
        TeamIMG.image=UIImage(data:decodedData!)
        TeamNameLBL.text=TeamDetailDict.valueForKey("vTeamSmall")!.valueForKey("teamName") as? String
        
        TXTUsername.attributedPlaceholder = NSAttributedString(string:"Username",attributes:[NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        
        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackBTNClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func FindUserBTNClick(sender: UIButton) {
        
        if TXTUsername.text.isEmpty==true
        {
            let  Warningalert = UIAlertView(title: nil,
                message:"Please enter username",
                delegate: self,
                cancelButtonTitle:"OK")
            Warningalert.show()
        }
        else
        {
            HUD.show(true)
            ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
            KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
            let ValueDict=NSDictionary(objectsAndKeys: TXTUsername.text,"userName")
            let ParamsDict=NSDictionary(objectsAndKeys:ValueDict,"Value")
            let RequestDict=NSDictionary(objectsAndKeys: "searchForUsername","method","1","id",ParamsDict,"params")
            print(RequestDict)
            let param = [
                "id"    : ID,
                "key"    : KEY,
                "callBack"    : "myCallBackMethod",
                "serviceName"    : "User",
                "request" : RequestDict
            ]
            println(param)
            Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
                .responseJSON {response in
                    self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
            }
        }
    }

    @IBAction func DoneBTnClick(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    /********************************************
    MARK:TableView Delegate
    ********************************************/
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserAry.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as! UITableViewCell
        cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: self.cellIdentifier)
        return cell
    }

    func textFieldDidBeginEditing(textField: UITextField) {
        let textFieldRect : CGRect = self.view.window!.convertRect(textField.bounds, fromView: textField)
        let viewRect : CGRect = self.view.window!.convertRect(self.view.bounds, fromView: self.view)
        let midline : CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
        let numerator : CGFloat = midline - viewRect.origin.y - MoveKeyboard.MINIMUM_SCROLL_FRACTION * viewRect.size.height
        let denominator : CGFloat = (MoveKeyboard.MAXIMUM_SCROLL_FRACTION - MoveKeyboard.MINIMUM_SCROLL_FRACTION) * viewRect.size.height
        var heightFraction : CGFloat = numerator / denominator
        if heightFraction < 0.0 {
            heightFraction = 0.0
        } else if heightFraction > 1.0 {
            heightFraction = 1.0
        }
        let orientation : UIInterfaceOrientation = UIApplication.sharedApplication().statusBarOrientation
        if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown) {
            animateDistance = floor(MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
        } else {
            animateDistance = floor(MoveKeyboard.LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
        }
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y -= animateDistance
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        self.view.frame = viewFrame
        UIView.commitAnimations()
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y += animateDistance
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))
        self.view.frame = viewFrame
        UIView.commitAnimations()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        var nextTag: Int = textField.tag + 1
        let nextResponder = textField.superview?.viewWithTag(nextTag) as UIResponder!
        if (nextResponder != nil)  {
            nextResponder.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            UIAlertView(title: nil,message:"Please check your internet connection",delegate: self,cancelButtonTitle:"OK").show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
        }
        HUD.hide(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }
}
