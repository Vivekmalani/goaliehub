//
//  PlayerSearchVC.swift
//  GoalieHub
//
//  Created by Vivek on 22/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit

class PlayerSearchVC: UIViewController {
    let cellIdentifier = "cellIdentifier"
    @IBOutlet var PlayerTBL: UITableView!
    var PlayerAry=NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.PlayerTBL?.registerClass(UITableViewCell.self, forCellReuseIdentifier: self.cellIdentifier)
        PlayerTBL.tableFooterView = UIView(frame: CGRectZero)
        PlayerTBL.separatorStyle=UITableViewCellSeparatorStyle.None
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /********************************************
    MARK:TableView Delegate
    ********************************************/
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PlayerAry.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as! UITableViewCell
        cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: self.cellIdentifier)
        cell.frame=CGRectMake(0, 0, self.view.frame.size.width, 40)
        return cell
    }
    

}
