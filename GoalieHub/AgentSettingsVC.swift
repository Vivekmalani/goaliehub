//
//  AgentSettingsVC.swift
//  GoalieHub
//
//  Created by Vivek on 02/04/16.
//  Copyright (c) 2016 Vivek. All rights reserved.
//

import UIKit
import Alamofire
var AgentID:Int=0,LocationID:Int=0
class AgentSettingsVC: UIViewController,CarbonTabSwipeNavigationDelegate,MBProgressHUDDelegate {
    @IBOutlet var MenuView: UIView!
    var carbonTabSwipeNavigation:CarbonTabSwipeNavigation!
    @IBOutlet var BarBTN: UIBarButtonItem!
    @IBOutlet var AgentStatus: UISwitch!
    @IBOutlet var UserIMG: UIImageView!
    var itemAry=NSArray()
    var HUD:MBProgressHUD!
    var ID:NSNumber!,KEY:NSNumber!,LocID:Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.revealViewController() != nil
        {
            BarBTN.target = self.revealViewController()
            BarBTN.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.view.setHeader(NSUserDefaults.standardUserDefaults().valueForKey("LastSync") as! String)
        
        self.tabBarController?.tabBar.hidden=true
        UserIMG.clipsToBounds=true
        UserIMG.layer.cornerRadius=UserIMG.frame.size.width/2
        itemAry=NSArray(array: ["GENERAL","LOCATION","AVAILABILITY"])
        carbonTabSwipeNavigation=CarbonTabSwipeNavigation(items: itemAry as [AnyObject], delegate: self)
        carbonTabSwipeNavigation.insertIntoRootViewController(self, andTargetView: MenuView)
        Style()
        
        ID=NSUserDefaults.standardUserDefaults().valueForKey("ID")!.integerValue as NSNumber
        KEY=NSUserDefaults.standardUserDefaults().valueForKey("KEY")!.integerValue as NSNumber
        
        let ParamsDict=NSDictionary()
        let RequestDict=NSDictionary(objectsAndKeys: "getMyAgents","method","1","id",ParamsDict,"params")
        print(RequestDict)
        let param = [
            "id"    : ID,
            "key"    : KEY,
            "callBack"    : "myCallBackMethod",
            "serviceName"    : "Agent",
            "request" : RequestDict
        ]
        println(param)
        Alamofire.request(.POST, API_STR, parameters: param, encoding: .JSON)
            .responseJSON {response in
                self.HandleAPIResponse(response.3, Dict: response.2 as? NSDictionary)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func AgentStatusChange(sender: UISwitch) {
        if sender.on == true
        {
            NSUserDefaults.standardUserDefaults().setValue("Y", forKey: "Status")
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue("N", forKey: "Status")
        }
    }
    
    
    func Style()
    {
        let color=UIColor.whiteColor()
        self.navigationController?.navigationBar.translucent=false
        carbonTabSwipeNavigation.setTabBarHeight(40)
        carbonTabSwipeNavigation.toolbar.translucent=false
        carbonTabSwipeNavigation.setIndicatorColor(color)
        carbonTabSwipeNavigation.setSelectedColor(UIColor.whiteColor())
        carbonTabSwipeNavigation.setNormalColor(UIColor.whiteColor())
        carbonTabSwipeNavigation.toolbar.barTintColor=UIColor.blackColor()
        //carbonTabSwipeNavigation.view.backgroundColor
        carbonTabSwipeNavigation.setTabExtraWidth(30)
//        carbonTabSwipeNavigation.toolbar.layer.borderWidth=1.0
//        carbonTabSwipeNavigation.toolbar.layer.borderColor=UIColor.whiteColor().CGColor
        carbonTabSwipeNavigation.carbonSegmentedControl.setWidth(self.view.frame.size.width/2, forSegmentAtIndex: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl.setWidth(self.view.frame.size.width/2, forSegmentAtIndex: 1)
        carbonTabSwipeNavigation.carbonSegmentedControl.setWidth(self.view.frame.size.width/2, forSegmentAtIndex: 2)
    }
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
        switch index
        {
        case 0:
            return self.storyboard!.instantiateViewControllerWithIdentifier("GeneralVC") as! GeneralVC
        case 1:
            return self.storyboard!.instantiateViewControllerWithIdentifier("LocationVC") as! LocationVC
        default:
            return self.storyboard!.instantiateViewControllerWithIdentifier("AvailableVC") as! AvailableVC
        }
    }
    
    func carbonTabSwipeNavigation(carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAtIndex index: UInt) {
        if index == 0
        {
            NSNotificationCenter.defaultCenter().postNotificationName("General", object: nil)
        }
        else if index==1
        {
            NSNotificationCenter.defaultCenter().postNotificationName("Location", object: nil)
        }
        else if index==2
        {
            NSNotificationCenter.defaultCenter().postNotificationName("Available", object: nil)
        }
    }
    
    func HandleAPIResponse(error:NSError!,Dict:NSDictionary!)
    {
        print(error)
        print(Dict)
        
        if error != nil
        {
            let  Connalert = UIAlertView(title: nil,
                message:"Please check your internet connection",
                delegate: self,
                cancelButtonTitle:"OK")
            Connalert.show()
        }
        else
        {
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("id"), forKey: "ID")
            NSUserDefaults.standardUserDefaults().setValue(Dict.valueForKey("key"), forKey: "KEY")
            AgentID=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("agentId") as! Int
            LocationID=Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("AgentSettings")!.valueForKey("locId") as! Int
            
            if Dict.valueForKey("response")!.valueForKey("result")!.valueForKey("agentActive")!.isEqualToString("Y")
            {
                AgentStatus.on=true
                NSUserDefaults.standardUserDefaults().setValue("Y", forKey: "Status")
            }
            else
            {
                AgentStatus.on=false
                NSUserDefaults.standardUserDefaults().setValue("N", forKey: "Status")
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        
        if carbonTabSwipeNavigation.carbonSegmentedControl.selectedSegmentIndex == 1
        {
            NSNotificationCenter.defaultCenter().postNotificationName("Location", object: nil)
        }
        else if carbonTabSwipeNavigation.carbonSegmentedControl.selectedSegmentIndex == 2
        {
            NSNotificationCenter.defaultCenter().postNotificationName("Available", object: nil)
        }
        let LogoIMG=UIImageView(frame: CGRectMake(60, 33, 130, 50))
        LogoIMG.image=UIImage(named: "ic_header_general_logo.png")
        self.navigationController?.view.addSubview(LogoIMG)
        
        let LocationBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        LocationBtn.setImage(UIImage(named: "ic_header_notification.png"), forState: UIControlState.Normal)
        //        LocationBtn.addTarget(self, action: "LocationBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        LocationBtn.frame = CGRectMake(0, 0, 40, 40)
        let LocationBarBtn = UIBarButtonItem(customView: LocationBtn)
        
        let ShareBtn: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
        ShareBtn.setImage(UIImage(named: "unnamed.png"), forState: UIControlState.Normal)
        ShareBtn.clipsToBounds=true
        ShareBtn.layer.cornerRadius=15
        //        ShareBtn.addTarget(self, action: "ShareBTNClick", forControlEvents: UIControlEvents.TouchUpInside)
        ShareBtn.frame = CGRectMake(0, 0, 30, 30)
        let ShareBarBtn = UIBarButtonItem(customView: ShareBtn)
        self.navigationItem.setRightBarButtonItems([ShareBarBtn, LocationBarBtn], animated: false)
    }
}
