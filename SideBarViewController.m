//
//  SideBarViewController.m
//  acino
//
//  Created by dipen  narola on 07/02/15.
//  Copyright (c) 2015 dipen. All rights reserved.
//

#import "SideBarViewController.h"
#import "SWRevealViewController.h"

#import <Social/Social.h>
#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>

@interface SideBarViewController ()
{
    NSArray *menuItems;
    
    NSArray *user_info;
    
    int x;
}
@end

@implementation SideBarViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
   NSString *imageupdate=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"imagechanged"]];
    if([imageupdate isEqualToString:@"1"])
    {
        [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"imagechanged"];
        [self.tableView reloadData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    menuItems = @[@"null",@"Home",@"Profilepicture", @"Password", @"Email",@"Zipcode", @"Blocked",@"Appfeedback",@"Logout",@"Aboutapp",@"null1"];
    
     x=0;
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell1"];
    
    self.tableView.backgroundColor=[UIColor colorWithRed:49.0/255.0 green:49.0/255.0 blue:49.0/255.0 alpha:1.0];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
  //        UIAlertView *loginsuccess = [[UIAlertView alloc]initWithTitle:nil message:@"1" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        
//        [loginsuccess show];
//    }
//    else
//    {
//        UIAlertView *loginsuccess = [[UIAlertView alloc]initWithTitle:nil message:@"not 1" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        
//        [loginsuccess show];
//    }

    
    NSString *value=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"Push"]];
    
    if([value isEqualToString:@"1"])
    {
        [self performSegueWithIdentifier:@"Notification" sender:nil];
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     return 8;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   //NSString *CellIdentifier = [menuItems objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell1" forIndexPath:indexPath];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
   
    
    if(indexPath.row==0)
    {
        UIImageView *BackImage=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
        BackImage.image=[UIImage imageNamed:@"bg_side_menu_header_logo.png"];
        [cell addSubview:BackImage];
        
        UIImageView *LogoIMG=[[UIImageView alloc] initWithFrame:CGRectMake(60, 25, 150, 50)];
        LogoIMG.image=[UIImage imageNamed:@"ic_side_menu_header_logo.png"];
        [cell addSubview:LogoIMG];
    }
    
    else if (indexPath.row==1)
    {
//        UIImageView *ImageL=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 24, 24)];
//        ImageL.image=[UIImage imageNamed:@"ic_side_menu_home.png"];
//        ImageL.userInteractionEnabled=YES;
//        [cell addSubview:ImageL];
        
        UILabel *lbl_home=[[UILabel alloc]initWithFrame:CGRectMake(15, 2, self.view.frame.size.width-30, 40)];
        lbl_home.text=@"Home";
        lbl_home.textColor=[UIColor whiteColor];
        lbl_home.font=[UIFont boldSystemFontOfSize:14.0];
        lbl_home.textAlignment=NSTextAlignmentLeft;
        [cell addSubview:lbl_home];
    }
    
    else if (indexPath.row==2)
    {
//        UIImageView *ImageL=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 24, 24)];
//        ImageL.image=[UIImage imageNamed:@"ic_side_menu_home.png"];
//        ImageL.userInteractionEnabled=YES;
//        [cell addSubview:ImageL];
        
        UILabel *lbl_home=[[UILabel alloc]initWithFrame:CGRectMake(15, 2, self.view.frame.size.width-30, 40)];
        lbl_home.text=@"Agent Settings";
        lbl_home.textColor=[UIColor whiteColor];
        lbl_home.font=[UIFont boldSystemFontOfSize:14.0];
        lbl_home.textAlignment=NSTextAlignmentLeft;
        [cell addSubview:lbl_home];
    }
    
    else if (indexPath.row==3)
    {
        
//        UIImageView *ImageL=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 24, 24)];
//        ImageL.image=[UIImage imageNamed:@"ic_side_menu_profile.png"];
//        ImageL.userInteractionEnabled=YES;
//        [cell addSubview:ImageL];
        
        UILabel *lbl_Profile=[[UILabel alloc]initWithFrame:CGRectMake(15, 2, self.view.frame.size.width-30, 40)];
        lbl_Profile.text=@"Help Menus";
        lbl_Profile.textColor=[UIColor whiteColor];
        lbl_Profile.font=[UIFont boldSystemFontOfSize:14.0];
        lbl_Profile.textAlignment=NSTextAlignmentLeft;
        [cell addSubview:lbl_Profile];
    }
    else if (indexPath.row==4)
    {
        
//        UIImageView *ImageL=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 24, 24)];
//        ImageL.image=[UIImage imageNamed:@"ic_side_menu_notification.png"];
//        ImageL.userInteractionEnabled=YES;
//        [cell addSubview:ImageL];
        
        UILabel *lbl_Profile=[[UILabel alloc]initWithFrame:CGRectMake(15, 2, self.view.frame.size.width-30, 40)];
        lbl_Profile.text=@"Favorite/Blocked Users";
        lbl_Profile.textColor=[UIColor whiteColor];
        lbl_Profile.font=[UIFont boldSystemFontOfSize:14.0];
        lbl_Profile.textAlignment=NSTextAlignmentLeft;
        [cell addSubview:lbl_Profile];
    }

    else if (indexPath.row==5)
    {
        
//        UIImageView *ImageL=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 24, 24)];
//        ImageL.image=[UIImage imageNamed:@"ic_side_menu_share.png"];
//        ImageL.userInteractionEnabled=YES;
//        [cell addSubview:ImageL];
        
        UILabel *lbl_Notification=[[UILabel alloc]initWithFrame:CGRectMake(15, 2, self.view.frame.size.width-30, 40)];
        lbl_Notification.text=@"App Feedback";
        lbl_Notification.textColor=[UIColor whiteColor];
        lbl_Notification.font=[UIFont boldSystemFontOfSize:14.0];
        lbl_Notification.textAlignment=NSTextAlignmentLeft;
        [cell addSubview:lbl_Notification];
    }
    else if (indexPath.row==6)
    {
//        UIImageView *ImageL=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 24, 24)];
//        ImageL.image=[UIImage imageNamed:@"ic_side_menu_about_us.png"];
//        ImageL.userInteractionEnabled=YES;
//        [cell addSubview:ImageL];
        
        UILabel *fav=[[UILabel alloc]initWithFrame:CGRectMake(15, 2, self.view.frame.size.width-30, 40)];
        fav.text=@"About PlayerBin.com";
        fav.textColor=[UIColor whiteColor];
        fav.font=[UIFont boldSystemFontOfSize:14.0];
        fav.textAlignment=NSTextAlignmentLeft;
        [cell addSubview:fav];
    }
    else if (indexPath.row==7)
    {
        
//        UIImageView *ImageL=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 24, 24)];
//        ImageL.image=[UIImage imageNamed:@"ic_side_menu_sign_out.png"];
//        ImageL.userInteractionEnabled=YES;
//        [cell addSubview:ImageL];
        
        
        UILabel *lbl_logout=[[UILabel alloc]initWithFrame:CGRectMake(15, 2, self.view.frame.size.width-30, 40)];
        lbl_logout.text=@"Logout";
        lbl_logout.textColor=[UIColor whiteColor];
        lbl_logout.font=[UIFont boldSystemFontOfSize:14.0];
        lbl_logout.textAlignment=NSTextAlignmentLeft;
        [cell addSubview:lbl_logout];
    }

    cell.backgroundColor=[UIColor clearColor];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        return 80;
    }
    else
    {
        
        if(indexPath.row==11)
        {
            if(self.view.frame.size.height>((44*8)+250))
            {
                return self.view.bounds.size.height-((44*7)+250);
            }
            else
            {
                return 0;//self.view.bounds.size.height-((44*7)+250);
            }
        }
        else
        {
            return 44;
        }
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"CheckNav"];
        [self performSegueWithIdentifier:@"Home" sender:nil];
    }
    else if (indexPath.row == 2)
    {
        [self performSegueWithIdentifier:@"AgentSettings" sender:nil];
        //self.tabBarController.tabBar.hidden=YES;
    }
    else if (indexPath.row == 3)
    {
        
    }
    else if (indexPath.row == 4)
    {
        [self performSegueWithIdentifier:@"Faviourites" sender:nil];
    }
    else if (indexPath.row == 5)
    {
        [self performSegueWithIdentifier:@"Feedback" sender:nil];
    }
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
//    
//    if ([[segue identifier] isEqualToString:@"Sign Out"])
//    {
//        if(x==0)
//        {
//            UIAlertView *logout = [[UIAlertView alloc]initWithTitle:@"Are You sure?" message:@"Do you Want to Logout?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:nil];
//            //locationAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//            logout.tag=512;
//            [logout addButtonWithTitle:@"YES"];
//            
//            [logout show];
//        }
//        else
//        {
//            
//        
//        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
//        UINavigationController *destViewController = (UINavigationController*)segue.destinationViewController;
//        
//        destViewController.title = [[menuItems objectAtIndex:indexPath.row] capitalizedString];
//        
//        NSUserDefaults *fetchDefaultslogin = [NSUserDefaults standardUserDefaults];
//        [fetchDefaultslogin setBool:NO forKey:@"is_login"];
//        
//        if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] )
//        {
//            SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
//            
//            swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
//                
//                UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
//                [navController setViewControllers: @[dvc] animated: NO ];
//                [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
//            };
//            
//        }
//    }
//
//    }
//    else if ([[segue identifier] isEqualToString:@"Share"])
//    {
//        UIAlertView *share_alert = [[UIAlertView alloc]
//                                    initWithTitle:@"Share Application With"
//                                    message:nil
//                                    delegate:self
//                                    cancelButtonTitle:@"Cancel"
//                                    otherButtonTitles:@"Twitter",@"Facebook",@"Email",@"Text Message",nil];
//        share_alert.tag=45;
//        [share_alert show];
//    }
//    else
//    {
    
    
  //  [self hidesBottomBarWhenPushed];
    
    // Set the title of navigation bar by using the menu items
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    UINavigationController *destViewController = (UINavigationController*)segue.destinationViewController;

    destViewController.title = [[menuItems objectAtIndex:indexPath.row] capitalizedString];
    
    
    //    home=[[HomeView alloc]init];
    //    [home.player1 play];
    
    // Set the photo if it navigates to the PhotoView
    
    if ([[segue identifier] isEqualToString:@"Home"])
    {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"Hidetabbar"];
   
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
        vc.view.backgroundColor = [UIColor clearColor];
        //  [vc setTransitioningDelegate:transitionController];
        vc.modalPresentationStyle= UIModalPresentationCustom;
        [self presentViewController:vc animated:YES completion:nil];
    
//    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] )
//    {
//        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
//        
//        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
//            
//            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
//            navController.hidesBottomBarWhenPushed = YES;
//            [navController setViewControllers: @[dvc] animated: NO ];
//            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
//            
//            
//        };
//        
//        
//    }
    }

    else
    {
        if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] )
        {
            SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        
            swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
                 navController.hidesBottomBarWhenPushed = YES;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
        
     }
 }
   // }
}


//- (BOOL)hidesBottomBarWhenPushed {
//    return YES;
//}


/*

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==512)
    {
    if (buttonIndex == 1)
    {
        
        [self callLOgout];
//        x=1;
//        [self performSegueWithIdentifier:@"Sign Out" sender:nil];
    }
    else
    {
        x=0;
    }
    }
    
    if (alertView.tag == 45)
    {
        
        //        NSString *string = [alertView buttonTitleAtIndex:buttonIndex];
        
        NSInteger index = buttonIndex;
        if (index==1)
        {
            [self twittershare];
        }
        else if (index==2)
        {
            [self facebookshare];
        }
        else if(index ==3)
        {
            [self emailshare];
        }
        else if (index==4)
        {
            [self sendMessage];
        }
        
    }

}
-(void)twittershare
{
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        //    NSString *tweet_str=hederlbl.text;
        //    if(tweet_str.length>89)
        //    {
        //        tweet_str=[tweet_str substringToIndex:3];
        //        tweet_str=[tweet_str stringByAppendingString:@" Read more: http://www.elementmag.asia/magazine.php"];
        //    }
        //    else
        //    {
        //        tweet_str=[tweet_str stringByAppendingString:@" Read more: http://www.elementmag.asia/magazine.php"];
        //    }
        
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:@"Download amazing application Win Or Loss from app store : https://itunes.apple.com/us/app/win-or-loss/id985170419?ls=1&mt=8"];
        [tweetSheet addImage:[UIImage imageNamed:@"app_icon_image.png"]];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
    
    
}
-(void)facebookshare
{
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [controller setInitialText:@"Download amazing application Win Or Loss from app store : https://itunes.apple.com/us/app/win-or-loss/id985170419?ls=1&mt=8"];
        [controller addImage:[UIImage imageNamed:@"app_icon_image.png"]];//[UIImage imageNamed:@"socialsharing-facebook-image.jpg"]];
        [self presentViewController:controller animated:YES completion:Nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a post right now, make sure your device has an internet connection and you have at least one facebook account setup in setting"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
    
    
}
*/
//
//-(void)emailshare
//{
//    NSString * subject = @"Win Or Loss application Share";
//    //email body
//    // NSString * body = @"How did you find the Android-IOS-Tutorials Website ?";
//    //recipient(s)
//    // NSArray * recipients = [NSArray arrayWithObjects:@"info@vasundhravision.com", nil];
//    MFMailComposeViewController * composer = [[MFMailComposeViewController alloc] init];
//    
//    if ([MFMailComposeViewController canSendMail]) {
//        composer.mailComposeDelegate = self;
//        [composer setSubject:subject];
//        //[composer setMessageBody:@"here is link for Love Frame application" isHTML:NO];
//        //[composer setMessageBody:body isHTML:YES]; //if you want to send an HTML message
//        // [composer setToRecipients:recipients];
//        
//        [composer setMessageBody:@"Download amazing application Win Or Loss from app store : https://itunes.apple.com/us/app/win-or-loss/id985170419?ls=1&mt=8" isHTML:NO];
//        
//        //get the filepath from resources
//        //NSString *filePath = [[NSBundle mainBundle] pathForResource:@"logo" ofType:@"png"];
//        
//        //read the file using NSData
//        // NSData * fileData = [NSData dataWithContentsOfFile:filePath];
//        // Set the MIME type
//        /*you can use :
//         - @"application/msword" for MS Word
//         - @"application/vnd.ms-powerpoint" for PowerPoint
//         - @"text/html" for HTML file
//         - @"application/pdf" for PDF document
//         - @"image/jpeg" for JPEG/JPG images
//         */
//        //NSString *mimeType = @"image/png";
//        
//        //add attachement
//        //[composer addAttachmentData:fileData mimeType:mimeType fileName:filePath];
//        
//        //present it on the screen
//        [self presentViewController:composer animated:YES completion:NULL];
//    }
//    
//    
//    
//    
//    
//}

/*
//**************************************************
#pragma mark - Mail composer Controll
//**************************************************
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled"); break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved"); break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent"); break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]); break;
        default:
            break;
    }
    
    // close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


 
//************************************************
#pragma mark - Send Message
//************************************************
-(void)sendMessage
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        NSString *share_str=[NSString stringWithFormat:@"Download amazing application Win Or Loss from app store : https://itunes.apple.com/us/app/win-or-loss/id985170419?ls=1&mt=8"];
        
        controller.body = share_str;
        //controller.recipients = [NSArray arrayWithObjects:@"1(234)567-8910", nil];
        controller.messageComposeDelegate = self;
        [self presentModalViewController:controller animated:YES];
    }
    
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"Cancelled");
            break;
        case MessageComposeResultFailed:
            
            break;
        case MessageComposeResultSent:
            
            break;
        default:
            break;
    }
    
    [self dismissModalViewControllerAnimated:YES];
}

*/



/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
 
    // Configure the cell...
 
    return cell;
}
*/
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
